<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'user_id','user_name','user_email_id','user_profile_image','image','image_id','comment','markup',
    ];
}
