<?php

namespace App\Http\Controllers;

use App\Favourite;
use Illuminate\Http\Request;
use App\User;
use App\Image;

class FavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function show(Favourite $favourite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favourite $favourite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favourite $favourite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favourite $favourite)
    {
        //
    }

    public function like($fromlike, $tolike)
    {
        $likecountdata = Favourite::where('user_id',$fromlike)->where('image_id',$tolike)->get();
         $likedcount=count($likecountdata);
       
        if($likedcount==0){
        $id = $fromlike;
        $userdata = User::find($id);
        // return $fromdata;
        $data = new Favourite;
        $data->user_id = $fromlike;
        $data->image_id = $tolike;

        $imagedata = Image::find($tolike);
        $imagedata->favourite_count=$imagedata->favourite_count+1;
        $imagedata->save();
        
        $data->image = $imagedata->image;
        $data->user_name = $userdata->name;
        $data->user_email_id = $userdata->email;
        $data->user_profile_image = $userdata->image;
        
        $data->save();

        // return back();
        return 1;
    }
    }

    public function unlike($fromlike, $tolike)
    {
        $likecountdata = Favourite::where('user_id',$fromlike)->where('image_id',$tolike)->get();
         $likedcount=count($likecountdata);
       
        if($likedcount==1){
        $data = Favourite::where([
            ['user_id', '=', $fromlike],
            ['image_id', '=', $tolike],]);
        // return $data;
        $data->delete();
        $imagedata = Image::find($tolike);
        $imagedata->favourite_count=$imagedata->favourite_count-1;
        $imagedata->save();
        // return back();
        return 1;

    }
    }
}
