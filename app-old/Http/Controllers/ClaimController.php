<?php

namespace App\Http\Controllers;

use App\Claim;
use Illuminate\Http\Request;
use App\Image;
use Auth;
use Mail;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;
            $signedinname = Auth::user()->name;
            $signedinemail = Auth::user()->email;
        }else{
            $signedinid = "notsi";
            $signedinname = "notsi";
            $signedinemail = "notsi";
        }

        $imageid = $request->image_id;
        $image = Image::find($imageid);
        // return $image;
        $claim = new Claim();
        $claim->claimed = $image->claimed;
        $claim->owners_name = $image->owners_name;
        $claim->owners_email = $image->owners_email;
        $claim->claimers_id = $signedinid;
        $claim->claimers_name = $signedinname;
        $claim->claimers_email = $signedinemail;
        $claim->image_id = $request->image_id;
        $claim->image = $image->image;
        $claim->message = $request->message;
        
        if($request->hasfile('document'))
        {
            // return "doc hai";
            $file = $request->file('document');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename ='file.'.time().'.'.$extension;
            $file->move('supportingdocs', $filename);
            $claim->supporting_document =$filename;
        }
            // return "doc nahi hai";
        $claim->save();


        $emails = ['akil.ideamagix@gmail.com','support@atmovfx.com'];
        $data1 = array('content'=>"There's a new image claim request on atmovfx.com, please visit atmovfx.com/admin to check complete details");
        Mail::send('mails.claimreq',$data1, function($message) use($emails){
        $message->to($emails, 'ATMO-VFX')->subject
        ('Claim Request');
        $message->from('contact@atmovfx.com','ATMO');
        });



        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function show(Claim $claim)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function edit(Claim $claim)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Claim $claim)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Claim  $claim
     * @return \Illuminate\Http\Response
     */
    public function destroy(Claim $claim)
    {
        //
    }
}
