<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Edit;
use App\Claim;
use App\Favourite;
use Validator;
use Hash;
use Redirect;
use App\User;
use App\Comment;
use Imagei;
use Storage;
use File;
use App\tag;
use App\Advertise;
use Mail;


class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Image::all()->random();
        // $data = Image::inRandomOrder()->where([['status','approved'],['deleted','no']])->paginate(30);
        $data = Image::inRandomOrder()->where([['status','approved'],['deleted','no']])->orderby('id','desc')->paginate(100);
        $link = Advertise::find(1);
        // return $data;
        $datatype = 'all';
        $users = User::all();
        return view('index',compact('data','datatype','link','users'));
    }
    
    
    
    

    public function page($imagecategory)
    {
        // return 1;
        $data = Image::inRandomOrder()->where('category','like','%'.$imagecategory.'%')->where([['status','approved'],['deleted','no']])->inRandomOrder()->paginate(100);
        $datatype = $imagecategory;
        $link = Advertise::find(1);
        // return $data;
        $users = User::all();
        return view('index',compact('data','datatype','link','users'));
    }

    public function useruploads()
    {   
        // return 1;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }

        $data = Image::where([['user_id',$signedinid],['deleted','no']])->orderby('id','desc')->get();
        // return $data;
        $datatype = 'useruploads';
        return view('userpanel.myuploads',compact('data','datatype'));
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;


        if(Auth::check())
        {
            $signedinid = Auth::user()->id;
            $signedinname = Auth::user()->name;
            $signedinemail = Auth::user()->email;
            $signedinimage = Auth::user()->image;
            
            $signedinartisttype = Auth::user()->artist_type;
            $signedinartisttypeverified = Auth::user()->artist_type_verified;
        }else{
            $signedinname = "notsi";
            $signedinemail = "notsi";
            $signedinimage = "notsi";
            $signedinid = 0;
            $signedinartisttype = "notsi";
            $signedinartisttypeverified = "notsi";
        }

        if ($request->has('admin')) {
            $image = new Image();
            $image->artist_name = $request->artist_name;
            $image->uploaders_name = "admin";
            $image->uploaders_email = "admin";
            $image->owners_email = "admin";

            if($request->movie_name != null){
                $image->movie_name = $request->movie_name;
            }
            if($request->category != null){
                $catstring = implode(',', $request->category);
                // return $catstring;
                $image->category = $catstring;
            }
            if($request->description != null){
                $image->description = $request->description;
            }
            if($request->tags != null){
                $stringtag = implode(',', $request->tags);
                $image->tags = $stringtag;
            }

            if($request->hasfile('image'))
            {
               $file = $request->file('image');
               $filesize = round($file->getSize()/1024);
               $data = getimagesize($file);
               $width = $data[0]/3;
               $height = $data[1]/3;

               $extension = $file->getClientOriginalExtension(); // getting image extension
               $filename ='image.'.time().'.'.$extension;
               $image_resize = Imagei::make($file->getRealPath());
               $file->move('images', $filename);
               $image_resize->resize(500, null, function ($constraint) {
                   $constraint->aspectRatio();
               });
               $image_resize->save('images/compressed/'.$filename,100);
               $image->image =$filename;
            }
            $image->owners_name = "admin";

            $image->status = "approved";
            $image->user_id = 0;
            $image->save();
            return back();
        }
        // return $request;


        $image = new Image();
        
        $image->uploaders_name = $signedinname;
        $image->uploaders_email = $signedinemail;
        $image->uploaders_id =$signedinid;
        

        if($request->artistnamedropdown == 'unknown'){
            $image->artist_name = 'Unknown';
            $image->claimed = 'no';
            $image->owners_name = $signedinname;
            $image->owners_email = $signedinemail;
            $image->user_id =$signedinid;
        }elseif($request->artistnamedropdown == 'other'){
            $image->artist_name = $request->artist_name;
            $image->claimed = 'no';
            $image->owners_name = $signedinname;
            $image->owners_email = $signedinemail;
            $image->user_id =$signedinid;
        }else{
            $dropdownuserdata = User::find($request->artistnamedropdown);
            $image->artist_name = $dropdownuserdata->name;
            $image->claimed = 'yes';
            $image->claimed_by = $dropdownuserdata->name;
            $image->owners_name = $dropdownuserdata->name;
            $image->owners_email = $dropdownuserdata->email;
            $image->profile_image = $dropdownuserdata->image;
            $image->user_id =$request->artistnamedropdown;
        }



        // if ($request->has('i_am_artist')) {
        //     $image->claimed = 'yes';
        //     $image->claimed_by = $request->artist_name;
        //     $image->owners_name = $request->artist_name;
        //     $image->profile_image = $signedinimage;
        //     $image->user_id =$signedinid;
        // }else{
        //     $image->claimed = 'no';
        //     $image->owners_name = $signedinname;
        //     $image->user_id =$signedinid;
        // }

        if($request->movie_name != null){
            $image->movie_name = $request->movie_name;
        }
        if($request->category != null){
            $catstring = implode(',', $request->category);
            // return $catstring;
            $image->category = $catstring;
        }
        if($request->description != null){
            $image->description = $request->description;
        }
        if($request->tags != null){
            $stringtag = implode(',', $request->tags);
            $image->tags = $stringtag;
        }
       
        if($request->hasfile('image'))
        {
            
            $file = $request->file('image');
            $filesize = round($file->getSize()/1024);
            // return $filesize;
            // return 1;
            $data = getimagesize($file);
            $width = $data[0]/3;
            $height = $data[1]/3;
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename ='image.'.time().'.'.$extension;
            $image_resize = Imagei::make($file->getRealPath());
            $file->move('images', $filename);
            // $image_resize = Imagei::make($request->file('image')->getRealPath());
            // $image_resize = Imagei::make($file->getRealPath());
            // $image_resize->resize($width,$height);
            $image_resize->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save('images/compressed/'.$filename,100);
            // $image_resize->save('images/compressed/'.$filename);
            $image->image =$filename;


            // return "saved";
        }

        if ($signedinemail == 'moderator@atmo.com') {
            $image->status = "approved";
        }elseif($signedinartisttype == 'senior' && $signedinartisttypeverified == 'yes'){
            $image->status = "approved";
        }
        // return $image;
        
        $image->save();
        return back();
        
        
        $emails = ['akil.ideamagix@gmail.com','support@atmovfx.com'];
        $data1 = array('content'=>"There's a new image uploaded on atmovfx.com, please visit atmovfx.com/admin to check complete details");
        Mail::send('mails.newupload',$data1, function($message) use($emails){
        $message->to($emails, 'ATMO-VFX')->subject
        ('Edit Request');
        $message->from('contact@atmovfx.com','ATMO');
        });
        
        
        
    }

    

    public function search(Request $request)
    {
        $search = $request->search_field;
        // return $search;
        $users = User::all();

        // $terms = explode(",",$search);
        // return $terms;

        // $data = Image::query()
        //     ->where([['status','approved'],['deleted','no']])
        //     ->where(function ($query) use ($terms) {
        //         foreach ($terms as $term) {
        //             // Loop over the terms and do a search for each.
        //             $query->where('artist_name', 'like', '%' . $term . '%');
        //         }
        //     })
        //     ->orWhere(function ($query) use ($terms) {
        //         foreach ($terms as $term) {
        //             $query->where('description', 'like', '%' . $term . '%');
        //         }
        //     })
        //     ->orWhere(function ($query) use ($terms) {
        //         foreach ($terms as $term) {
        //             $query->where('tags', 'like', '%' . $term . '%');
        //         }
        //     })
        //     ->orWhere(function ($query) use ($terms) {
        //         foreach ($terms as $term) {
        //             $query->where('movie_name', 'like', '%' . $term . '%');
        //         }
        //     })
        //     ->inRandomOrder()
        //     ->paginate(100);








        $search_fields = ['artist_name', 'description', 'tags', 'movie_name'];
        $search_terms = explode(',',$search);

        $query = Image::query();
        foreach ($search_terms as $term) {
            $query->orWhere(function ($query) use ($search_fields, $term) {

                foreach ($search_fields as $field) {
                    $query->orWhere($field, 'LIKE', '%' . $term . '%');
                }
            });
        }
        $data = $query->inRandomOrder()->paginate(100);
        $search_terms = implode(",",$search_terms);
        $data->appends(['search_field' => $search_terms]);
        // return $data;










        // $data = DB::table('images')
        //     ->where([['status','approved'],['deleted','no']])
        //     ->where('artist_name','like','%'.$search.'%')
        //     ->orWhere('description', 'like', '%'.$search.'%')
        //     ->orWhere('tags', 'like', '%'.$search.'%')
        //     ->orWhere('movie_name','like','%'.$search.'%')
        //     ->inRandomOrder()
        //     ->paginate(100);
        // return $data;



        
        $link = Advertise::find(1);
        $datatype = 'search';
        return view('index',compact('data','datatype','search','link','users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        // return 1;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;
            $claimdata = Claim::where('claimers_id',$signedinid)->get();
            $likeddata = Favourite::where('user_id',$signedinid)->get();
            // return $claimdata;
        }else{
            $claimdata = '0';
            $likeddata = '0';
        }
        $comments = Comment::where('image_id',$image->id)->orderby('id','desc')->get();
        
        $userdetail = User::where('id',$image->user_id)->first();

        $datatype = "favourites";
        $alltags = tag::orderby('tag','asc')->get();
        // return $image;
        $users = User::all();
        return view('imagedetail',compact('image','claimdata','comments','likeddata','userdetail','datatype','alltags','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        if(Auth::check())
        {   
            $signedinname = Auth::user()->name;
            $signedinemail = Auth::user()->email;
            $signedinid = Auth::user()->id;
            
            $signedinartisttype = Auth::user()->artist_type;
            $signedinartisttypeverified = Auth::user()->artist_type_verified;
        }else{
            $signedinname = "notsi";
            $signedinemail = "notsi";
            $signedinid = "notsi";
        }

        $edit = new Edit();
        $edit->editors_id = $signedinid;
        $edit->editors_name = $signedinname;
        $edit->editors_email = $signedinemail;
        $edit->uploaders_name = $image->owners_name;
        $edit->uploaders_email = $image->owners_email;
        $edit->claimed = $image->claimed;
        $edit->image_id = $image->id;
        $edit->image = $image->image;
        $edit->old_artist_name = $image->artist_name;
        $edit->new_artist_name = $request->artist_name;
        $edit->old_movie_name = $image->movie_name;
        $edit->new_movie_name = $request->movie_name;
        $edit->old_category = $image->category;
        if($request->category != ''){
            $newcatstring = implode(',', $request->category);
        }else{
            $newcatstring = '';
        }
        $edit->new_category = $newcatstring;
        $edit->old_tags = $image->tags;
        if($request->tags){
            $stringtag = implode(',', $request->tags);
            $edit->new_tags = $stringtag;
        }else{
            $edit->new_tags = $request->tags;
        }

        // $edit->new_tags = $request->tags;
        $edit->old_description = $image->description;
        $edit->new_description = $request->description;
        $edit->status = 'pending';
        // return $edit;
        
        
        
        if($signedinartisttype == 'senior' && $signedinartisttypeverified == 'yes'){
            $edit->status = 'approved';
            
            $imageid = $edit->image_id;
            $imagedata = Image::find($imageid);
            // return $imagedata;
            $imagedata->artist_name = $edit->new_artist_name;
            $imagedata->movie_name = $edit->new_movie_name;
            $imagedata->category = $edit->new_category;
            $imagedata->tags = $edit->new_tags;
            $imagedata->description = $edit->new_description;
            $imagedata->save();
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        $edit->save();
        
        
        
        
        
        
        

        $emails = ['akil.ideamagix@gmail.com','support@atmovfx.com'];
        $data1 = array('content'=>"There's a new image edit request on atmovfx.com, please visit atmovfx.com/admin to check complete details");
        Mail::send('mails.editreq',$data1, function($message) use($emails){
        $message->to($emails, 'ATMO-VFX')->subject
        ('Edit Request');
        $message->from('support@atmovfx.com','ATMO');
        });



        return back();





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }

    public function uploadpage()
    {
        $tags = tag::orderby('tag','asc')->get();
        $users = User::where('id', '!=', auth()->id())->get();
        // return $tags;
        return view('userpanel.uploadimage',compact('tags','users'));
    }

    public function userwishlist()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Favourite::where('user_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'favourites';
        return view('userpanel.mywishlist',compact('data','datatype'));
    }

    public function userdetail()
    {
        if(Auth::check())
        {
            $data = Auth::user();  
        }else{
            $data = 0;
        }
        $datatype = 'userdetails';
        return view('userpanel.userdetail',compact('data','datatype'));
    }

    public function changeuserpassword()
    {   
        // return view('auth.passwords.reset',compact('token'));
        $datatype = 'changepassword';
        return view('userpanel.changepassword',compact('datatype'));
    }

    public function admin_credential_rules(array $data)
    {
      $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
      ];

      $validator = Validator::make($data, [
        'current-password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',     
      ], $messages);

      return $validator;
    }


    public function updatepassword(Request $request)
    {
        // return 112;
      if(Auth::Check())
      {
        $request_data = $request->All();
        $validator = $this->admin_credential_rules($request_data);
        if($validator->fails())
        {
          $error = response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
          return Redirect::back()->withErrors(['New Passwords Dont Match','New Passwords Dont Match']);
        }
        else
        {  
          $current_password = Auth::User()->password;           
          if(Hash::check($request_data['current-password'], $current_password))
          {           
            $user_id = Auth::User()->id;                       
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($request_data['password']);;
            $obj_user->save(); 
            return Redirect::back()->withErrors(['Password Changed', 'Password Changed']);
          }
          else
          {           
            return Redirect::back()->withErrors(['Please enter correct current password', 'Please enter correct current password']);
          }
        }        
      }
      else
      {
        return redirect()->to('/');
      }    
    }

    public function useredits()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Edit::where('editors_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'useredits';
        return view('userpanel.myedits',compact('data','datatype'));
    }

    public function userclaims()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Claim::where('claimers_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'userclaims';
        return view('userpanel.myclaims',compact('data','datatype'));   
    }



    public function deleteimage($id)
    {
        // return $id;
        $image = Image::find($id);
        $image->deleted = 'yes';
        $image->save();
        return back();
    }


    public function deletedimages()
    {
        $data = Image::where('deleted','yes')->orderby('id','desc')->get();
        $datatype = 'deletedimages';
        return view('userpanel.deletedimages',compact('data','datatype'));
    }


    public function markupcomment($id)
    {
        $image = Image::find($id);
        return view('userpanel.commentmarkup',compact('image'));
        return $data;
    }

    public function getalldata(Request $request)
    {
        // return $request->offset;
        $alldata = DB::table('images')->select('*')->where([['status', 'approved'],['deleted','no']])->take($request->limit)->skip($request->offset)->orderby('id','desc')->get();
        $res = json_decode($alldata, true);
        // return $res;

        


        foreach ($alldata as $alldatanew) {

            $id = $alldatanew->id;
            $image = $alldatanew->image;
            $artist_name = $alldatanew->artist_name;
            $movie_name = $alldatanew->movie_name;
            $dataone='

                <div class="box" data-aos="fade-up">
                  <a href="/image/'.$id.'" title="" class="d-block">
                    <img src="/images/'.$image.'" alt="">
                    <div class="home-page-profile-container">
                      <div class="user-detail d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center" style="flex-basis: 74%;">
                          <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                    ';
                          if($alldatanew->artist_name != ''){
                $datatwo = '

                    <p class="mb-0">'.$artist_name.'</p>

                ';
                }
                            
                          elseif($alldatanew->movie_name != ''){
                $datatwo = ' 

                    <p class="mb-0">'.$movie_name.'</p>

                ';

                }
                          else{

                $datatwo='
                            <p class="mb-0">Unknown</p>

                ';
                
                }           

                $datathree = '
                        </div>
                        <div class="" style="flex-basis: 70px;">
                          <div class="likes-wrap d-flex justify-content-between">
                            <div class="counts-container d-flex align-items-center mr-2">
                              <i class="fas fa-share"></i>
                            </div>
                            <div class="counts-container d-flex align-items-center">
                              <i class="far fa-comment"></i>
                              <span class="count">999</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>

            ';


            $finaldata = $dataone.$datatwo.$datathree;
            echo $finaldata;
        }
    }


    public function getimagedata(Request $request)
    {
        // return $request->offset;

        if ($request->type == 'all') {
            $alldata = DB::table('images')->select('*')->where([['status', 'approved'],['deleted','no']])->take($request->limit)->skip($request->offset)->orderby('id','desc')->get();
            $res = json_decode($alldata, true);    
        }else{
            $alldata = DB::table('images')->select('*')->where([['category','like','%'.$request->type.'%'],['status', 'approved'],['deleted','no']])->take($request->limit)->skip($request->offset)->orderby('id','desc')->get();
            $res = json_decode($alldata, true);   
        }
        
        
        // return $res;

        // if(Auth::check())
        // {
        //     $user = Auth::user(); 
        //     $userimg = Auth::user()->image; 
        // }else{
        //     $user = 0;
        // }

        foreach ($alldata as $alldatanew) {

            $id = $alldatanew->id;
            $image = $alldatanew->image;
            $artist_name = $alldatanew->artist_name;
            $movie_name = $alldatanew->movie_name;
            $favourite_count = $alldatanew->favourite_count;
            if ($alldatanew->claimed == 'yes') {
                $userid = $alldatanew->user_id;
                $userdata = User::find($userid);
                $user_image = $userdata->image;
                // $user_image = $userimg;
            }else{
                $user_image ='1.jpg';
            }
            // return $user_image;
            $dataone='

                <div class="box" data-aos="fade-up">
                  <a href="/image/'.$id.'" title="" class="d-block">
                    <img src="/images/compressed/'.$image.'" alt="">
                    <div class="home-page-profile-container">
                      <div class="user-detail d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center" style="flex-basis: 74%;">
                          <img src="/assets/images/'.$user_image.'" alt="" class="rounded-circle">
                    ';
                          if($alldatanew->artist_name != ''){
                $datatwo = '

                    <p class="mb-0">'.$artist_name.'</p>

                ';
                }
                            
                          elseif($alldatanew->movie_name != ''){
                $datatwo = ' 

                    <p class="mb-0">'.$movie_name.'</p>

                ';

                }
                          else{

                $datatwo='
                            <p class="mb-0">Unknown</p>

                ';
                
                }           

                $datathree = '
                        </div>
                        <div class="" style="flex-basis: 70px;">
                          <div class="likes-wrap d-flex justify-content-between">
                            <div class="counts-container d-flex align-items-center mr-2">
                              <i class="fas fa-share"></i>
                            </div>
                            <div class="counts-container d-flex align-items-center">
                              <i class="far fa-comment"></i>
                              <span class="count">'.$favourite_count.'</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>

            ';


            $finaldata = $dataone.$datatwo.$datathree;
            echo $finaldata;
        }
    }




    public function savemarkup(Request $request)
    {
        $img = $request['imgBase64'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $filename ='/images/markup.'.time().'.png';
        // $success = file_put_contents($filename, $data);
        // File::put($filename,$data);
        Storage::disk('markup_uploads')->put($filename,$data);
        return "success";
        
    }


    public function notfound()
    {
        // return 1;
        return view('errors.404');
    }
    
    
    
    
    
    
    public function savemarkupscreenshot(Request $request)
    {
        // return 1;
        $img = $request['imgBase64'];
        // $img = str_replace('data:image/png;base64,', '', $img);
        // $img = str_replace(' ', '+', $img);
        // $data = base64_decode($img);
        // $filename ='image.'.time().'.jpeg';
        // return $data;
        $width = "500"; 
        $height = "500"; 
        $name = time().'.'.explode('/',explode(':',substr($img,0,strpos($img,';')))[1])[1];
        // Imagei::make($img)->resize($width,$height)->save(public_path('images/test/').$name);
        $newimg = Imagei::make($img);
        // $newimg->resize($width,$height);
        $newimg->save(public_path('images/test/').$name,70);
        // $image_resize = Imagei::make($data);
        // $image_resize->resize($width,$height);
        // return $data;
        // $image_resize->save('/images/compressed/'.$filename,100);

        // $success = file_put_contents($filename, $data);
        // File::put($filename,$data);
        // Storage::disk('markup_uploads')->put($filename,$data);
        return $name;



        // $file = $request->file('image');
        // $filesize = round($file->getSize()/1024);
        // // return $filesize;
        // // return 1;
        // $data = getimagesize($file);
        // $width = $data[0]/3;
        // $height = $data[1]/3;
        // $extension = $file->getClientOriginalExtension(); // getting image extension
        // $filename ='image.'.time().'.'.$extension;
        // $image_resize = Imagei::make($file->getRealPath());
        // $file->move('images', $filename);
        // // $image_resize = Imagei::make($request->file('image')->getRealPath());
        // // $image_resize = Imagei::make($file->getRealPath());
        // // $image_resize->resize($width,$height);
        // $image_resize->resize(500, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // });
        // $image_resize->save('images/compressed/'.$filename,100);
        // // $image_resize->save('images/compressed/'.$filename);
        // $image->image =$filename;
        
    }


    public function allmarkups($id)
    {
        // return $id;
        // $data = Comment::where('id',$id)->get();
        $data = Comment::where([['image_id',$id],['markup','!=','null']])->paginate(20);
        // return $data;
        $users = User::all();
        return view('allmarkups',compact('data','users'));

        
    }
    

    public function tagimages($tag)
    {
        $tag = str_replace('-', ' ', $tag);
        $data = Image::where('tags', 'like', '%'.$tag.'%')->paginate(50);
        $type = 'Tag';
        $value = $tag;
        $users = User::all();
        return view('otherimages',compact('data','type','value','users'));
        // return $data;
    }

    public function titleimages($title)
    {
        $title = str_replace('-', ' ', $title);
        $data = Image::where('movie_name', 'like', '%'.$title.'%')->paginate(50);
        $type = 'Title';
        $value = $title;
        $users = User::all();
        return view('otherimages',compact('data','type','value','users'));
        // return $data;
    }

    public function artistimages($artist)
    {
        // return $artist;
        if ($artist == 'unknown') {
            $artist = ''; 
            $data = Image::where('artist_name','')->paginate(50);   
        }else{
            $artist = str_replace('-', ' ', $artist);    
            $data = Image::where('artist_name', 'like', '%'.$artist.'%')->paginate(50);
        }
        
        $type = 'Artist';
        $value = $artist;
        if ($artist == '') {
            $value = 'Unknown';
        };
        $users = User::all();
        return view('otherimages',compact('data','type','value','users'));
        return $data;
    }
    
    
    
}