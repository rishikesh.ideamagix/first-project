<?php

    Route::GET('/home', 'AdminController@index')->name('admin.home');
    Route::GET('/dashboard', 'AdminController@index')->name('admin.home');
    Route::GET('/tags', 'AdminController@alltags')->name('admin.alltags');
    // Login and Logout
    Route::GET('/', 'LoginController@showLoginForm')->name('admin.login');
    Route::POST('/', 'LoginController@login');
    Route::POST('/logout', 'LoginController@logout')->name('admin.logout');

    // Password Resets
    Route::POST('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::GET('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::POST('/password/reset', 'ResetPasswordController@reset');
    Route::GET('/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::GET('/password/change', 'AdminController@showChangePasswordForm')->name('admin.password.change');
    Route::POST('/password/change', 'AdminController@changePassword');

    // Register Admins
    Route::get('/register', 'RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('/register', 'RegisterController@register');
    Route::get('/{admin}/edit', 'RegisterController@edit')->name('admin.edit');
    Route::delete('/{admin}', 'RegisterController@destroy')->name('admin.delete');
    Route::patch('/{admin}', 'RegisterController@update')->name('admin.update');

    // Admin Lists
    Route::get('/show', 'AdminController@show')->name('admin.show');

    // Admin Roles
    Route::post('/{admin}/role/{role}', 'AdminRoleController@attach')->name('admin.attach.roles');
    Route::delete('/{admin}/role/{role}', 'AdminRoleController@detach');

    // Roles
    Route::get('/roles', 'RoleController@index')->name('admin.roles');
    Route::get('/role/create', 'RoleController@create')->name('admin.role.create');
    Route::post('/role/store', 'RoleController@store')->name('admin.role.store');
    Route::delete('/role/{role}', 'RoleController@destroy')->name('admin.role.delete');
    Route::get('/role/{role}/edit', 'RoleController@edit')->name('admin.role.edit');
    Route::patch('/role/{role}', 'RoleController@update')->name('admin.role.update');

    Route::fallback(function () {
        return abort(404);
    });




    Route::GET('/editrequests', 'AdminController@editrequests')->name('admin.editrequests');
    Route::GET('/editdetail/{id}', 'AdminController@editdetail')->name('admin.editdetail');
    Route::GET('/approveedit/{id}', 'AdminController@approveedit')->name('admin.approveedit');
    Route::GET('/rejectedit/{id}', 'AdminController@rejectedit')->name('admin.rejectedit');

    Route::GET('/images', 'AdminController@images')->name('admin.allimages');
    Route::GET('/imagestwo', 'AdminController@imagestwo')->name('admin.allimagestwo');

    Route::GET('/image/detail/{id}', 'AdminController@imagedetail')->name('admin.imagedetail');

    Route::GET('/deletedimages', 'AdminController@deletedimages')->name('admin.deletedimages');
    Route::POST('/changestatus/{id}/{status}', 'AdminController@changestatus')->name('admin.changestatus');
    Route::POST('/changedeletestatus/{id}/{deletestatus}', 'AdminController@changedeletestatus')->name('admin.changedeletestatus');


    Route::GET('/claimrequests', 'AdminController@claimrequests')->name('admin.claimrequests');
    Route::GET('/claimdetail/{id}', 'AdminController@claimdetail')->name('admin.claimdetail');
    Route::GET('/approveclaim/{id}', 'AdminController@approveclaim')->name('admin.approveclaim');
    Route::GET('/rejectclaim/{id}', 'AdminController@rejectclaim')->name('admin.rejectclaim');
    Route::GET('/deleteclaim/{id}', 'AdminController@deleteclaim')->name('admin.deleteclaim');

    Route::GET('/users', 'AdminController@users')->name('admin.users');
    Route::POST('/changeuserstatus/{id}/{status}', 'AdminController@changeuserstatus')->name('admin.changeuserstatus');
    Route::POST('/changeapprovalstatus/{id}/{status}', 'AdminController@changeapprovalstatus')->name('admin.changeapprovalstatus');
    
    Route::POST('/updatead', 'AdminController@updatead')->name('admin.updatead');
    
    Route::POST('/updatetags/{id}', 'AdminController@updatetags')->name('admin.updatetags');    
    
    
    Route::post('/add/tag', 'AdminController@addtag');
    
    Route::any('/deletetag/{id}', 'AdminController@deletetag');

    Route::any('/imagesdata', 'AdminController@imagesdata');


    Route::post('/updateimgdet/{id}', 'AdminController@updateimagedet');
    
    Route::any('/deleteper/{id}', 'AdminController@deleteper');
    

    Route::any('/allusermail', 'AdminController@allusermail');




