<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  @include('layouts.styles')
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <!-- home page search form start -->
      <!-- home page search form end -->
      <!-- result grid start -->
      @if(count($data) < 1)
        <div class="container d-flex flex-wrap justify-content-center align-items-center text-center" style="height:80vh;">
            <div style="color:#5CD9D3;">
                <h1>No Markups Yet</h1>
            </div>
        </div>
      @endif
      <section class="result-grid py-4 px-2 px-lg-0" style="min-height:80vh;">
        <div class="container-fluid px-2">
          <div id="mecy" class="image-container">
            <!-- Automatic data comes here -->
            @foreach($data as $datanew)
              <div class="box" data-aos="fade-up">
                <a href="javascript:void(0)" onclick="showmarkupimagenew(this)" data-image="{{$datanew->markup}}" data-comment="{{$datanew->comment}}" data-toggle="modal" data-target="#myModal" title="" class="d-block">
                  <img src="/images/test/{{$datanew->markup}}" alt="" class="lazy">
                  <div class="home-page-profile-container">
                    <div class="user-detail d-flex align-items-center justify-content-between">
                      <div class="d-flex align-items-center">
                        <!-- <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle"> -->
                        <img id="currentPhoto" src="SomeImage.jpg" class="rounded-circle" onerror="this.onerror=null; this.src='/assets/images/1.jpg'" >
                        @php
                          $comment = substr($datanew->comment, 0, 30);
                          if(strlen($comment) > 29 ){
                            $dot = "...";
                          }else{
                            $dot = '';
                          }
                        @endphp
                        <p class="mb-0">{{$datanew->user_name}} : {{$comment}} {{$dot}}</p>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            @endforeach
          </div>
          <div class="mt-5">
            {{$data->links()}}
          </div>
        </div>
      </section>
      <!-- result grid end -->
    </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->

    @include('layouts.sidenav')
    <!-- sidenav end -->


    <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h6 class="modal-title" id="markedupcomment"></h6>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            
            <img class="w-100" src="" id="markedupimage" alt="gagag">
          </div>

          <!-- Modal footer -->
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div> -->

        </div>
      </div>
    </div>


  </div>
  <!-- wrapper end -->
  <!-- javascript files start -->
  @include('layouts.js.jquery')
  @include('layouts.js.macy')
  @include('layouts.js.script')
  <script src="/assets/vendor/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>

  <!-- javascript files end -->

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
  <script type="text/javascript">
    function showmarkupimagenew(xyz){
        var comment = $(xyz).attr('data-comment');
        console.log(comment);
        var image = $(xyz).attr('data-image');
        var source = '/images/test/'+image;
        $('#markedupimage').attr('src',source);
        $('#markedupcomment').html(comment);
    }

    $(".category-button").click(function(){
      var link = $(this).attr('data-link');
      window.location.href = link;
    });
    $(function() {
        $('.lazy').lazy();
    });
  </script>
</body>

</html>