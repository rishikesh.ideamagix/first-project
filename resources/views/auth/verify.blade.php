<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  @include('layouts.styles')
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-12">
                  <div class="text-center email-verification">
                      <h3 class="">{{ __('Verify Your Email Address') }}</h3>
                      <div class="">
                          @if (session('resent'))
                              <div class="alert alert-success" role="alert">
                                  {{ __('A fresh verification link has been sent to your email address.') }}
                              </div>
                          @endif
                          {{ __('Before proceeding, please check your email for a verification link.') }}
                          {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}" style="color:#5CD9D3;">{{ __('click here to request another') }}</a>.
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </main>
    <!-- main end -->
    <!-- footer start -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
            <ul class="m-0 p-0">
              <li>
                <a href="#" title="About Us">About Us</a>
              </li>
              <li>
                <a href="#" title="Contact">Contact</a>
              </li>
              <li>
                <a href="#" title="Terms & Conditions">Terms & Conditions</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-2 col-md-6 mb-5 mb-lg-0">
            <ul class="m-0 p-0">
              <li>
                <a href="#" title="Facebook"><span class="mr-2 fab fa-facebook-f"></span> Facebook</a>
              </li>
              <li>
                <a href="#" title="Twitter"><span class="mr-2 fab fa-twitter"></span> Twitter</a>
              </li>
              <li>
                <a href="#" title="Instagram"><span class="mr-2 fab fa-instagram"></span> Instagram</a>
              </li>
            </ul>
          </div>
          <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
            <p>Sunscribe to our newsletter</p>
            <form action="" method="" class="w-100">
              <div class="input-wrap">
                <input type="text" placeholder="Email Address" class="w-100 h-100 py-2 pl-3 border-0">
                <button class="d-flex justify-content-center align-items-center px-3 border-0">
                  OK
                </button>
              </div>
            </form>
          </div>
          <div class="col-lg-3 col-md-6">
            <p class="m-0">497 Evergreen Rd. Roseville, CA 95673</p>
            <a href="tel:+44345678903" title="+44 345 678 903" class="d-block">+44 345 678 903</a>
            <a href="mailto:adobexd@mail.com" title="adobexd@mail.com" class="d-block">adobexd@mail.com</a>
          </div>
        </div>
      </div>
    </footer>
    <!-- footer end -->
    <!-- sidenav start -->

    <div class="sidenav">
      <!-- <div class="sidenav-close d-flex justify-content-center align-items-center">
        <span class="fas fa-angle-left"></span>
      </div> -->
      <div class="sidenav-wrap">
        <a href="index.html" title="ATMO" class="d-block sidenav-logo py-4">
          <!-- <img src="/assets/images/logo-highresolution.png" alt="Pocket HRMS" class="d-block mx-auto"> -->
          ATMO
        </a>
        <ul class="sidenav-main-menu p-0 m-0">
          <!-- Links will be generated automatically using the main nav bar -->
        </ul>
        <div class="signup-container py-4 d-flex justify-content-around">
          <!-- Right side Links will be generated automatically using the main right  nav bar -->
        </div>
      </div>
    </div>
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  <!-- javascript files start -->
  @include('layouts.scripts')
  <!-- javascript files end -->
</body>

</html>