<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:image" content="/images/{{$image->image}}">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="" sizes="32x32" type="image/png">
    <title> Image | ATMO </title>

    <!-- css files start -->
  <style>
      .hoverchnage:hover{
          color:#5FD9D3 !important;
      }

      .greyimage{
        -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
      }
      
      .nice-select{
          min-width:200px;
          color:#5CD9D3 !important;
          background:black !important;
      }
      
      .nice-select .list{
         color:#5CD9D3 !important;
      }
  </style>
    @include('layouts.styles')
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e29762e82ddf0001251a4f6&product=inline-share-buttons' async='async'></script>
    <!-- bootstrap.tagsinput -->
    <link rel="stylesheet" href="/assets/vendor/select2/select2.min.css">
    <link rel="stylesheet" href="/assets/vendor/fancybox/jquery.fancybox.min.css">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.css">
    
    <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
    
    <link rel="stylesheet" href="/assets/css/style.css">

    <style type="text/css">
        .nice-select {
            line-height: 24px;
        }
        
        .nice-select .option:hover,
        .nice-select .option.focus,
        .nice-select .option.selected.focus {
            background: #181F27;
        }
        
        .select2-container--default .select2-selection--multiple {
            background: transparent;
            border: 2px solid #5cd9d3;
            border-radius: 0;
            min-height: 40px;
        }
        
        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 2px solid #5cd9d3;
        }
        
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background: #5cd9d3;
            border: 0;
        }


        .bootstrap-tagsinput{
            background: transparent;
            border:2px solid #5cd9d3;
            min-height: 40px;
            border-radius: 0;
            width: 100%;
        }
        .sharethis-inline-share-buttons{
            z-index: 1 !important;
        }

        .modal-lg {
            max-width: 85% !important;
        }

        .select2-container{
          width: 100% !important;
        }
        
        .select2-container--default .select2-search--inline .select2-search__field{
            color: #fff;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>
    @php
      if(Auth::check())
      {
          $user = Auth::user();
          $signedinid = Auth::user()->id;
          $signedinname = Auth::user()->name;
          $signedinemail = Auth::user()->email;
          $signedinimage = Auth::user()->image;
          $emailverify = Auth::user()->email_verified_at;
          $artisttype = Auth::user()->artist_type;
          $artisttypeverified = Auth::user()->artist_type_verified;
      }else{
          $signedinname = "notsi";
          $signedinemail = "notsi";
          $signedinid = 0;
          $signedinimage = 0;
          $emailverify = "";
          $artisttype = "";
          $artisttypeverified="0";
      }
    @endphp
    <!--{{$emailverify}}-->
    <!-- wrapper start -->
    <div class="wrapper">
        <!-- header start -->
        @include('layouts.header')
        <!-- header end -->
        <!-- main start -->
        <main>

            <!-- home page search form start -->
            <!--<div class="home-page-search-form">-->
            <!--    <div class="container">-->
            <!--        <div class="row justify-content-center">-->
            <!--            <nav aria-label="breadcrumb " class="pb-2">-->
            <!--                <h2 class="pt-5 text-center text-white">Profile</h2>-->
            <!--                <ol class="breadcrumb justify-content-center">-->
            <!--                    <li class="breadcrumb-item"><a href="/">Home</a></li>-->
            <!--                    <li class="breadcrumb-item active" aria-current="page">Profile</li>-->
            <!--                </ol>-->
            <!--            </nav>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->
            <!-- home page search form end -->
            <div class="container" id="divtoshow">
                <div class="row" >
                    <div class="container">
                        <div class="d-flex flex-wrap" >
                            <div class="col-md-12 mx-auto my-5 coverform px-0 pb-3">
                                <div class=" coverall ">
                                    <div class="row">
                                        @php
                                            $currimageid = $image->id;
                                        @endphp
                                        <div class="col-sm-12 mt-2 ml-2"  >
                                            <div class="profile-image d-flex">
                                                <figure class="image-cover">
                                                    @if($image->claimed == 'yes')
                                                        <img src="/assets/images/{{$userdetail->image}}" class="rounded-circle d-block w-100" alt="Profile Image">
                                                    @else
                                                        <img src="/assets/images/1.jpg" class="rounded-circle d-block w-100" alt="Profile Image">
                                                    @endif
                                                </figure>
                                                <p class="text-white  profile-name">
                                                    @if($image->artist_name != '')
                                                        @php $artistlink = preg_replace('/\s+/', '-', $image->artist_name); @endphp
                                                        <a href="/images/artist/{{$artistlink}}" style="color: #5CD9D3">{{$image->artist_name}}</a>
                                                        
                                                        
                                                        @if($image->claimed == 'yes')
                                                            @foreach($users as $user)
                                                              @if($image->user_id == $user->id)
                                                                @if($user->artist_type == 'senior'  && $user->artist_type_verified == 'yes' )
                                                                  <img src="/verify.png" style="width: 20px !important">
                                                                @endif
                                                              @endif
                                                            @endforeach
                                                        @endif
                                                        
                                                        
                                                      @else
                                                        <a href="/images/artist/unknown" style="color: #5CD9D3">Unknown</a>
                                                      @endif
                                                    <!--@if($image->artist_name != '') {{$image->artist_name}} @elseif($image->movie_name != '') {{$image->movie_name}} @else Unknown @endif-->
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-12 m-0">
                                            <div class="profile-uploaded-image flex-column">
                                                <figure class="mb-0 originalfigure">
                                                    <button class="btn btn-sm showorignalbutton d-none" onclick="showorignalimage(this)">View Orignal Image</button>
                                                    <a class="fancy-img-part" data-fancybox="gallery" href="/images/{{$image->image}}">
                                                        <img src="/images/{{$image->image}}" class="w-100 profileimage zoom-in imagecolor" alt="" id="imagetoshow">
                                                    </a>
                                                </figure>
                                                @if($image->deleted == 'no')
                                                <div class="px-4 px-md-3 py-4 d-flex justify-content-between sharelinks mt-auto flex-wrap">
                                                    <div class="sharethis-inline-share-buttons"></div>
                                                    @php $liked = 0 @endphp
                                                    @if (Auth::check())
                                                    @foreach($likeddata as $likeddatanew)
                                                        @if($image->id == $likeddatanew->image_id)
                                                        @php $liked = 1 @endphp
                                                        @endif
                                                    @endforeach
                                                    
                                    @php
                                                                    $matchedgal = 'none';
                                                                @endphp
                                                                @foreach($refgalleries as $gallery)
                                                                    @php
                                                                        $finalimglistarr = explode(',', $gallery->images);
                                                                        if(in_array($currimageid, $finalimglistarr)){
                                                                            $matchedgal = $gallery->id;
                                                                        }
                                                                    @endphp
                                                                @endforeach


                                                                @if(Auth::check())
                                                                    <select name="torefgal" id="torefgal" class="form-control ml-md-auto mt-md-0 mt-2 nice-select mr-3 nice-select" style="min-width:200px !important;" >
                                                                        <option disabled selected value style="color:white;">Add to reference gallery</option>
                                                                        <option value="none">Remove</option>
                                                                        @foreach($refgalleries as $gallery)
                                                                            <option value="{{$gallery->id}}" @if($matchedgal == $gallery->id) selected @endif>{{$gallery->name}}</option>
                                                                        @endforeach
                                                                        <option value="addnew">Add New Gallery</option>
                                                                    </select>
                                                                @endif                
                                        
                                                    
                                                
                                    @php if ($liked == 1) {
                                    @endphp
                                    <button style="background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none; " class="unlikebutton" onClick="unlike(this)" data-link='/unlike/@php echo"$signedinid"; @endphp/{{ $image->id }}'>
                                        <i style="color: red" class="fa fa-heart"></i>
                                    </button>
                                    <button style="background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none; " class="likebutton d-none" onClick="like(this)" data-link='/like/@php echo"$signedinid"; @endphp/{{ $image->id }}'>
                                        <i style="color: white" class="far fa-heart"></i>
                                    </button>

                                    @php }else{  @endphp
                                    <button style="background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none; " class="likebutton" onClick="like(this)" data-link='/like/@php echo"$signedinid"; @endphp/{{ $image->id }}'>
                                        <i style="color: white" class="far fa-heart"></i>
                                    </button>
                                    <button style="background-color: Transparent; background-repeat:no-repeat; border: none; cursor:pointer; overflow: hidden; outline:none; " class="unlikebutton d-none" onClick="unlike(this)" data-link='/unlike/@php echo"$signedinid"; @endphp/{{ $image->id }}'>
                                        <i style="color: red" class="fa fa-heart"></i>
                                    </button>
                                    @php } @endphp
                                                    <!-- <a href="javascript:void(0);" class="d-block sharecommt">Favourite</a> -->
                                                    @endif
                                                </div>
                                                @endif
                                            </div>
                                        </div>

                                        
                                        <div class="col-12 profile-rightcontent d-flex flex-wrap px-4 mt-2" id="">
                                            <div class="col-md-6 blockone">
                                                <div class="text-left mb-1 mb-3">
                                                    <span class="text-white text-left  d-inline-block"> Artist : </span>
                                                    <span class="brandcolor justify-content-center d-inline-block">
                                                      @if($image->artist_name != '')
                                                        @php $artistlink = preg_replace('/\s+/', '-', $image->artist_name); @endphp
                                                        <a href="/images/artist/{{$artistlink}}" style="color: #5CD9D3">{{$image->artist_name}}</a>
                                                      @else
                                                        <a href="/images/artist/unknown" style="color: #5CD9D3">Unknown</a>
                                                      @endif
                                                    </span>
                                                </div> 
                                                <div class="text-left mb-1 mb-3">
                                                    <span class="text-white text-left  d-inline-block"> Movie/TV Title : </span>
                                                    <span class="brandcolor d-inline-block">
                                                    @if($image->movie_name != '')
                                                        @php $titlelink = preg_replace('/\s+/', '-', $image->movie_name); @endphp
                                                        <a href="/images/title/{{$titlelink}}" style="color: #5CD9D3">{{$image->movie_name}}</a>
                                                    @else
                                                        Unknown
                                                    @endif
                                                    </span>
                                                </div>   
                                            </div>
                                            <div class="col-md-6 blockone">
                                                <div class="  text-left d-sm-flex mb-3">
                                                    <span class="text-white text-left  d-inline-block pr-2" style="white-space: nowrap;">
                                                        <i class="fas fa-tags "></i> Tags :
                                                    </span>
                                                    <div class="d-flex flex-wrap ">
                                                        @if($image->tags != '') @php $tagarray = explode(",",$image->tags); $tagcount = count($tagarray); @endphp 
                                                            @foreach($tagarray as $tags)
                                                                @php $tagslink = preg_replace('/\s+/', '-', $tags); @endphp
                                                                <a href="/images/tag/{{$tagslink}}" class="taglabel m-1 px-1" style="color: #000;background:#5CD9D3;border: 1px solid #5CD9D3;cursor: pointer;">{{$tags}}</a> 
                                                            @endforeach 
                                                        @else
                                                            <span class="taglabel m-1" style="color: #5CD9D3">No Tags Found</span> 
                                                        @endif
                                                    </div>
                                                </div> 
                                                <div class="form-group  text-left mb-1 ">
                                                    <label class="text-white text-left">Description : </label>
                                                    <span id="dis" class="brandcolor">
                                                      @if($image->description != '')
                                                        {{ucfirst($image->description)}}
                                                      @else
                                                        No Description Yet.
                                                      @endif
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 blocktwo d-none">
                                                <form action="/image/{{$image->id}}" method="post" id="editdetailsform" class="d-flex flex-column h-100 mx-0 mt-0">
                                                    @csrf @method('put')
                                                    <div class="form-group  text-lg-left mb-1">
                                                        <label for="artist" class="text-white text-left  d-block"> Artist : </label>
                                                        <input type="text" id="artist" value="{{$image->artist_name}}" class="mytextfield brandcolor" name="artist_name" readonly="">
                                                    </div>
                                                    <div class="form-group  text-lg-left mb-1">
                                                        <label for="title" class="text-white text-left  d-block"> Movie/TV Title : </label>
                                                        <input type="text" id="artist" value="{{$image->movie_name}}" class="mytextfield brandcolor" name="movie_name">
                                                    </div>
                                                    @php $mpse = 'false'; $cp = 'false'; $pc = 'false'; $mo = 'false'; if (strpos($image->category, 'matte-painting-set-extension') !== false) { $mpse = 'true'; } if(strpos($image->category, 'traditional-painting') !== false){ $cp = 'true'; } if(strpos($image->category, 'photography-composition') !== false){ $pc = 'true'; } if(strpos($image->category, 'movies') !== false){ $mo = 'true'; } @endphp
                                                    <div class="form-group  text-lg-left mb-1">
                                                        <label for="myselect" class="text-white text-left  d-block"> Category: </label>
                                                        <select class="js-example-basic-multiple" placeholder="Select Category" name="category[]" multiple="multiple" style="width: 100%;background: transparent;color: white">
                                                            <option value="matte-painting-set-extension" @if($mpse=='true' ) selected @endif>Matte Painting Set Extension</option>
                                                            <option value="traditional-painting" @if($cp=='true' ) selected @endif>Traditional Painting</option>
                                                            <option value="photography-composition" @if($pc=='true' ) selected @endif>Photography Composition</option>
                                                            <option value="movies" @if($mo=='true' ) selected @endif>Movies</option>
                                                        </select>
                                                    </div>
                                            </div>
                                            <div class="col-md-6 blocktwo d-none">
                                                    <div class="form-group  text-lg-left">
                                                        <label for="taginput" class="text-white text-left  d-block"> Tags : </label>
                                                        <select class="js-example-basic-multiple" name="tags[]" multiple="multiple" style="color: #fff">
                                                          @foreach($alltags as $tag)
                                                            <option value="{{$tag->tag}}" @if(strpos($image->tags,$tag->tag) !== false) selected @endif >{{ucfirst($tag->tag)}}</option>
                                                          @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group  text-lg-left mb-1 ">
                                                        <label class="text-white text-left  d-block">Description : </label>
                                                        <textarea name="description" id="dis" cols="50" rows="3" class="adddiscription" placeholder="Add Description">{{$image->description}}</textarea>
                                                    </div>
                                                </form>
                                            </div>
                                            @if (Auth::check())
                                                @if($image->deleted == 'no')
                                                    @if($emailverify != null)
                                                        <div class="col-12 buttonone mt-2">
                                                            <div class=" profilesubmit  mt-auto mb-4 ml-auto ">
                                                                @php $alreadyclaimed = 'no' @endphp @foreach($claimdata as $claimdatanew) @if($claimdatanew->image_id == $image->id) @php $alreadyclaimed = 'yes'; @endphp @endif @endforeach @if($alreadyclaimed == 'yes')
                                                                <button class="uploadButton ml-1 submitbtn claimbnt allbnt border-0" style="color:#000">Already Claimed</button>
                                                                @else
                                                                <button class="uploadButton ml-1 claimbnt allbnt border-0" data-toggle="modal" data-target="#claimmodal" style="color: #000">Claim</button>
                                                                @endif
                                                                <button class="uploadButton ml-1 claimbnt allbnt border-0" onclick="myFunction1()" style="color: #000">Edit</button>
                                                                

                                                                
                                                                
                                                                

                                                                <span style="color: #5CD9D3" id="approvalmessage"></span>
                                                                @if($artisttype == 'senior' && $artisttype='yes' &&  $image->status == 'pending')
                                                                    <button class="uploadButton ml-1 claimbnt allbnt border-0 approvebutton" style="color: #000">Approve Image</button>
                                                                    <!-- <div> -->
                                                                        <button class="uploadButton ml-1 claimbnt allbnt border-0 confimbuttons d-none" data-imgid='{{$image->id}}' data-userid='{{$signedinid}}' onclick="approvefunction(this)" style="color: #000; background: #0f0">Confirm Approval</button>
                                                                        <button class="uploadButton ml-1 claimbnt allbnt border-0 confimbuttons d-none cancelapprovebutton" style="color: #000; background: #f00">Cancel</button>
                                                                    <!-- </div> -->
                                                                @endif
                                                            </div>  
                                                        </div>
                                                    @endif
                                                @endif
                                                <div class="col-12 buttontwo d-none mt-2">
                                                    <div class=" profilesubmit  mt-auto mb-4 ml-auto ">
                                                        <button form="editdetailsform" class="uploadButton ml-1 submitbtn claimbnt allbnt border-0">Submit</button>
                                                        <button class="ploadButton ml-1 claimbnt allbnt border-0" onclick="myFunction2()">Cancel</button>
                                                    </div>  
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-5 profile-rightcontent   flex-column pl-5 pl-md-0 pr-5" id="edit-submit">
                                            <form action="/image/{{$image->id}}" method="post" id="dropFileForm" class="d-flex flex-column h-100 mx-0 mt-0">
                                                @csrf @method('put')
                                                <div class="form-group  text-lg-left mb-1">
                                                    <label for="artist" class="text-white text-left  d-block"> Artist : </label>
                                                    <input type="text" id="artist" value="{{$image->artist_name}}" class="mytextfield brandcolor " name="artist_name">
                                                </div>
                                                <div class="form-group  text-lg-left mb-1">
                                                    <label for="title" class="text-white text-left  d-block"> Movie/TV Title : </label>
                                                    <input type="text" id="artist" value="{{$image->movie_name}}" class="mytextfield brandcolor" name="movie_name">
                                                </div>
                                                @php 
                                                    $mpse = 'false'; $cp = 'false'; $pc = 'false'; $mo = 'false'; 
                                                    if (strpos($image->category, 'matte-painting-set-extension') !== false) { $mpse = 'true'; } if(strpos($image->category, 'traditional-painting') !== false){ $cp = 'true'; } if(strpos($image->category, 'photography-composition') !== false){ $pc = 'true'; } if(strpos($image->category, 'movies') !== false){ $mo = 'true'; } @endphp
                                                <div class="form-group  text-lg-left mb-1">
                                                    <label for="myselect" class="text-white text-left  d-block"> Category : </label>
                                                    <select class="js-example-basic-multiple" placeholder="Select Category" name="category[]" multiple="multiple" style="width: 100%;background: transparent;">
                                                        <option value="matte-painting-set-extension" @if($mpse=='true' ) selected @endif>Matte Painting Set Extension</option>
                                                        <option value="traditional-painting" @if($cp=='true' ) selected @endif>Traditional Painting</option>
                                                        <option value="photography-composition" @if($pc=='true' ) selected @endif>Photography Composition</option>
                                                        <option value="movies" @if($mo=='true' ) selected @endif>Movies</option>
                                                    </select>
                                                </div>
                                                <div class="form-group  text-lg-left">
                                                    <label for="taginput" class="text-white text-left  d-block"> Tags : </label>
                                                    <input type="text" value="{{$image->tags}}" name="tags" id="taginput" class="mytags text-left" data-role="tagsinput" placeholder="Add Tags" />
                                                    <span id="tagerror" class="brandcolor font-weight-bold  d-block text-left"></span>
                                                    <span id="common" class="brandcolor font-weight-bold  d-block text-left"></span>
                                                </div>
                                                <div class="form-group  text-lg-left mb-1 ">
                                                    <label class="text-white text-left  d-block">Description : </label>
                                                    <textarea name="description" id="dis" cols="50" rows="3" class="adddiscription" placeholder="Add Description">{{$image->description}}</textarea>
                                                </div>
                                                <div class=" profilesubmit  mt-auto mb-2 ml-auto text-right">
                                                    <input type="submit" value="UPDATE" name="submit" class="uploadButton mr-1 submitbtn allbnt text-right">
                                                    <a href="javascript:void(0)" class="uploadButton ml-1 cancelbtn allbnt text-right" onclick="myFunction2()">CANCEL</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mx-auto mb-5 pt-5 coverform px-0 comment-section">
                                <div class=" coverall ">
                                    <form action="/comment" method="post" class="m-0">
                                        @csrf
                                        <div class="row">
                                            <div class="col-12 d-block d-sm-flex commentfield pb-4">
                                                <div class="profile-image p-3">
                                                    <figure class="image-cover">
                                                        @php
                                                          if(Auth::check())
                                                          {
                                                              $signedinimage = Auth::user()->image;
                                                        @endphp
                                                              <img src="/assets/images/{{$signedinimage}}" class="rounded-circle d-block w-100" alt="Profile Image">
                                                        @php
                                                          }else{
                                                        @endphp
                                                              <img src="/assets/images/1.jpg" class="rounded-circle d-block w-100" alt="Profile Image">
                                                        @php
                                                          }
                                                        @endphp
                                                        
                                                    </figure>
                                                </div>
                                                @if(Auth::check()){
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <input type="hidden" name="user_name" value="{{$user->name}}">
                                                    <input type="hidden" name="user_email_id" value="{{$user->email}}">
                                                    <input type="hidden" name="image" value="{{$user->image}}">
                                                    <input type="hidden" name="image_id" value="{{$image->id}}">
                                                    <input type="hidden" name="image" value="{{$image->image}}">
                                                    <input type="hidden" name="user_profile_image" value="{{$user->image}}">
                                                    <div class="form-group  text-lg-left mb-1  col-sm-10 mr-sm-auto ">
                                                        
                                                        @if($emailverify != null)
                                                            <textarea id="dis" cols="50" rows="6" class="adddiscription" name="comment" placeholder="Add Comment" required=""></textarea>
                                                            <div class="d-flex flex-wrap align-items-start profilesubmit  mt-auto mb-2 ml-auto text-right">
                                                                <!-- <input type="submit" value="Add marks" name="submit" class="uploadButton mr-1 submitbtn  text-right"> -->
                                                                <!-- <a href="javascript:void(0)" class="uploadButton ml-1 cancelbtn  text-right">Post</a> -->
                                                                <button class="btn mb-1 mr-2" style="background: #5CD9D3;border-radius: 0">Post Comment</button>
                                                                <a href="/markupcomment/{{$image->id}}" class="btn btn-success " style="background: #5CD9D3;border-radius: 0;color: #000">Post Comment With Markup</a>
                                                            </div>
                                                        @else
                                                            <textarea id="dis" cols="50" rows="6" class="adddiscription" name="comment" placeholder="Verify Email to comment" required="" disabled></textarea>
                                                            <div class=" profilesubmit  mt-auto mb-2 ml-auto text-right">
                                                                <!-- <input type="submit" value="Add marks" name="submit" class="uploadButton mr-1 submitbtn  text-right"> -->
                                                                <!-- <a href="javascript:void(0)" class="uploadButton ml-1 cancelbtn  text-right">Post</a> -->
                                                                <!-- <button class="btn" style="background: #5CD9D3;border-radius: 0">Post Comment</button> -->
                                                                <a href="javascript:void(0)" class="btn btn-success" style="background: #5CD9D3;border-radius: 0;color: #000">Verify your email address to comment</a><br>
                                                                <small style="color: white">Click <a href="">here</a> to resend verification email</small>
                                                            </div>
                                                        @endif
                                                        
                                                        
                                                        
                                                        <!--<div class=" profilesubmit  mt-auto mb-2 ml-auto text-right">-->
                                                            <!-- <input type="submit" value="Add marks" name="submit" class="uploadButton mr-1 submitbtn  text-right"> -->
                                                            <!-- <a href="javascript:void(0)" class="uploadButton ml-1 cancelbtn  text-right">Post</a> -->
                                                        <!--    <button class="btn" style="background: #5CD9D3;border-radius: 0">Post Comment</button>-->
                                                        <!--    <a href="/markupcomment/{{$image->id}}" class="btn btn-success" style="background: #5CD9D3;border-radius: 0;color: #000">Post Comment With Markup</a>-->
                                                        <!--</div>-->
                                                    </div>
                                                @else
                                                    <div class="form-group  text-lg-left mb-1  col-sm-10 mr-sm-auto ">
                                                        <textarea id="dis" cols="50" rows="6" class="adddiscription" name="comment" placeholder="Login To Comment" disabled=""></textarea>
                                                        <div class=" profilesubmit  mt-auto mb-2 ml-auto text-right">
                                                            <!-- <input type="submit" value="Add marks" name="submit" class="uploadButton mr-1 submitbtn  text-right"> -->
                                                            <!-- <a href="javascript:void(0)" class="uploadButton ml-1 cancelbtn  text-right">Post</a> -->
                                                            <a href="/login" class="btn" style="background: #5CD9D3;border-radius: 0;color: rgba(0,0,0,0.85);">Login To Comment</a>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col-12">
                                                <div class="savecomment py-4">
                                                    @if(count($comments) == 0)
                                                        <span class="d-block text-center" style="color: #5CD9D3">No Comments !!!</span>
                                                    @endif
                                                    <div class="px-3 d-flex justify-content-between align-items-center pb-3">
                                                        @if(count($comments) > 0)   
                                                            <h3 style="color: #5CD9D3">Comments</h3>
                                                            <a href="/allmarkups/{{$image->id}}" class="btn btn-success" style="background: #5CD9D3;color: #000">View All Markups</a>
                                                        @endif    
                                                    </div>
                                                    @foreach($comments as $commentsdata)
                                                        <div class="profile-image d-flex">
                                                            <div class="px-3">
                                                                <figure class="image-cover  ">
                                                                    <img src="/assets/images/{{$commentsdata->user_profile_image}}" class="rounded-circle d-block w-100" alt="Profile Image">
                                                                </figure>
                                                            </div>
                                                            <div class="comment-user">
                                                                <span class="profile-name">{{$commentsdata->user_name}}</span>
                                                                <span class="comment-history">{{$commentsdata->created_at->diffForHumans()}}</span>
                                                                <br>
                                                                @if($commentsdata->markup != null)
                                                                    <span>
                                                                        <!-- <i class="fas fa-images" style="color: #5CD9D3" onclick="showmarkupimage(this)" data-image="{{$commentsdata->markup}}"></i> -->
                                                                        <!--<img src="/images/test/{{$commentsdata->markup}}" height="50px" width="50px">-->
                                                                        <!--<i class="fas fa-images" style="cursor: pointer;color: #5CD9D3" onclick="showmarkupimagenew(this)" data-image="{{$commentsdata->markup}}" data-comment="{{$commentsdata->comment}}" data-toggle="modal" data-target="#myModal"></i>-->
                                                                        <img class="mr-1" src="/images/test/{{$commentsdata->markup}}" style="max-width: 400px;cursor: pointer;color: #5CD9D3" onclick="showmarkupimagenew(this)" data-image="{{$commentsdata->markup}}" data-comment="{{$commentsdata->comment}}" data-toggle="modal" data-target="#myModal">

                                                                    </span>
                                                                @endif
                                                                <p class="text-white usercmnt text-left">{{$commentsdata->comment}}</p>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        
        <a id="bwbutton" class="d-flex align-items-center justify-content-center" style="background:#fff;">View Images in B/W <i class="far fa-images ml-2"></i></a>
        <a id="colbutton" class="d-flex align-items-center justify-content-center">View Images in Color  <i class="fas fa-images ml-2"></i> </a>
        <!-- main end -->
        <!-- footer start -->
        @include('layouts.footer')
        <!-- footer end -->
        <!-- sidenav start -->
        @include('layouts.sidenav')
        <!-- sidenav end -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="claimmodal" tabindex="-1" role="dialog" aria-labelledby="claimmodalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="background:#181F27">
                <div class="modal-header" style="color: white">
                    <h5 class="modal-title" id="exampleModalLabel">Claim Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color: white">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/claim" method="post" id="claimform" class="d-flex flex-column h-100 mx-0 mt-0" enctype="multipart/form-data">
                        @csrf

                        <input type="text" id="imageid" class="mytextfield brandcolor d-none" name="image_id" value="{{$image->id}}">
                        <div class="form-group  text-lg-left mb-1 ">
                            <label class="text-white text-left  d-block">Message : </label>
                            <textarea name="message" id="dis" cols="50" rows="3" class="adddiscription" placeholder="Add Message"></textarea>
                        </div>
                        <div class="form-group  text-lg-left mb-1">
                            <label class="text-white text-left  d-block">Supporting Document : </label>
                            <input type="file" id="artist" class="mytextfield brandcolor" name="document">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="border-radius: 0">Close</button>
                    <button type="submit" form="claimform" class="btn btn-primary" style="background:#5CD9D3;border-color:#5CD9D3;border-radius: 0">Upload</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header">
            <h6 class="modal-title" id="markedupcomment"></h6>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
            
            <img class="w-100" src="" id="markedupimage" alt="gagag">
          </div>

          <!-- Modal footer -->
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div> -->

        </div>
      </div>
    </div>


    <div class="modal" id="myModaladd">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Add New Reference Gallery</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
              <form action="/addnewrefgall" id="newgalform" method="POST">
                @csrf
                <input type="hidden" name="imgid" class="w-100" value="{{$currimageid}}" required="">
                <input type="text" name="name" class="w-100" placeholder="Gallery Name" required="">
              </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" form="newgalform" class="btn btn-success">Add</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

          </div>
        </div>
    </div>
    <!-- wrapper end -->
    <!-- javascript files start -->

    <!-- jquery 3.4.1 -->
    @include('layouts.js.jquery')
    @include('layouts.js.select2')
    @include('layouts.js.fancybox')
    @include('layouts.js.script')
    @include('layouts.js.bootstraptagsinput')
    @include('layouts.js.likeunlikeajax')
    @include('layouts.js.upload')
    @include('layouts.js.niceselect')
    <script src="/assets/vendor/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
    

    <script>

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $('#torefgal').on('change', function() {
            
            var galid = this.value;
            if (galid == 'addnew') {
                console.log("aax");
                $('#myModaladd').modal('toggle');
            }else{
                var imageid = '<?php echo $currimageid; ?>';
                // console.log(imageid);
                var url = '/addtorefgal/'+imageid+'/'+galid;
                console.log(url);
                $.ajax({
                    url: url,
                    type: "post",
                    success: function(data){
                        console.log(data);
                    }
                });
            }
            
        });

        $('figure').on('contextmenu', 'img', function(e){ 
          return false; 
        });
        $('.fancybox-content img').bind('contextmenu', function(e) {
            return false;
        }); 
        var orignalimage;
        function myFunction1() {
            $('.blockone').addClass('d-none');
            $('.buttonone').addClass('d-none');
            $('.blocktwo').removeClass('d-none');
            $('.buttontwo').removeClass('d-none');
        }

        function myFunction2() {
            $('.blockone').removeClass('d-none');
            $('.blocktwo').addClass('d-none');
            $('.buttonone').removeClass('d-none');
            $('.buttontwo').addClass('d-none');
        }

        function showmarkupimage(xyz){
            // console.log($(xyz).attr('data-image'));
            var image = $(xyz).attr('data-image');
            var source = '/images/test/'+image;
            $('#imagetoshow').attr('src',source);
            orignalimage = $('.fancy-img-part').attr('href');
            $('.fancy-img-part').attr('href',source);
            $('.showorignalbutton').removeClass('d-none');

            $('html, body').animate({
                scrollTop: $("#divtoshow").offset().top
            }, 2000);

        }

        function showmarkupimagenew(xyz){
            var comment = $(xyz).attr('data-comment');
            console.log(comment);
            var image = $(xyz).attr('data-image');
            var source = '/images/test/'+image;
            $('#markedupimage').attr('src',source);
            $('#markedupcomment').html(comment);
            // orignalimage = $('.fancy-img-part').attr('href');
            // $('.fancy-img-part').attr('href',source);
            // $('.showorignalbutton').removeClass('d-none');

            // $('html, body').animate({
                // scrollTop: $("#divtoshow").offset().top
            // }, 2000);

        }


        function showorignalimage(xyz){
            console.log(orignalimage);
            $('#imagetoshow').attr('src',orignalimage);
            $('.fancy-img-part').attr('href',orignalimage);
            $('.showorignalbutton').addClass('d-none');
        }

        function approvefunction(xyz){
            // console.log(orignalimage);
            var imageid = $(xyz).attr('data-imgid');
            var userid = $(xyz).attr('data-userid');
            var url = '/senior/image/approval/'+imageid+'/'+userid;
            $.ajax({
                url: url,
                type: "post",
                success: function(data){
                    // $("#employees").html(data);
                    // console.log(data)
                    $('.approvebutton').addClass('d-none');
                    $('.confimbuttons').addClass('d-none');
                    $('#approvalmessage').html(" Image Approved!");
                    setTimeout(function() {
                        $("#approvalmessage").html('');
                    }, 5000);
                }
            });


        }


        $(".approvebutton").click(function(){
            $('.confimbuttons').removeClass('d-none');
        });

        $(".cancelapprovebutton").click(function(){
            $('.confimbuttons').addClass('d-none');
        });

var btnnew = $('#bwbutton');
    var btnnewcol = $('#colbutton');


    btnnew.addClass('show');
    btnnew.on('click', function(e) {
      e.preventDefault();
      
        $('.fancybox-image').addClass('greyimage');
        $('.imagecolor').addClass('greyimage');
        btnnew.removeClass('show');
        btnnewcol.addClass('show');
    });


    
    
    btnnewcol.on('click', function(e) {
      e.preventDefault();
      
        $('.fancybox-image').removeClass('greyimage');
        $('.imagecolor').removeClass('greyimage');
        btnnew.addClass('show');
        btnnewcol.removeClass('show');
    });

    </script>

</body>

</html>