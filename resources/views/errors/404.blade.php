<!DOCTYPE html>
 <html lang="en">

 <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <meta property="og:url" content="">
   <meta property="og:title" content="">
   <meta property="og:image" content="">
   <meta property="og:site_name" content="">
   <meta property="og:description" content="">
   <meta name="author" content="">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <link rel="icon" href="" sizes="32x32" type="image/png">
   <title>ATMO | Page Not Found</title>

   <!-- css files start -->
   <!-- bootstrap 4.0.0 -->
   <link rel="stylesheet" href="/assets/vendor/bootstrap-4.0.0/dist/css/bootstrap.min.css">
   <!-- font awesome 5.11.2 -->
   <link rel="stylesheet" href="/assets/vendor/fontawesome-free-5.11.2-web/css/all.min.css">
   <!-- owl carousel 2.3.4 -->
   <link rel="stylesheet" href="/assets/vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
   <!-- animate.css 3.7.2 -->
   <link rel="stylesheet" href="/assets/vendor/animate.css.3.7.2/animate.3.7.2.css">
   <!-- animate on scroll -->
   <link rel="stylesheet" href="/assets/vendor/aos-master/dist/aos.css">
   <!-- nice select 1.1.0 -->
   <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
   <!-- custom -->
   <link rel="stylesheet" href="/assets/css/style.css">
   <!-- css files end -->
 </head>

 <body >
   <!-- wrapper start -->
   <div class="wrapper" >
     <!-- header start -->
     @include('layouts.header')
     <!-- header end -->
     <!-- main start -->
        <main>
            <div class="container d-flex flex-wrap justify-content-center align-items-center text-center" style="height:80vh;">
                <div style="color:#5CD9D3;">
                    <h1>Sorry !!</h1>
                    <p>The page you are looking can not be found</p>
                </div>
            </div>
       </main>
     <!-- main end -->
     <!-- footer start -->
     @include('layouts.footer')
     <!-- footer end -->
     <!-- sidenav start -->
     @include('layouts.sidenav')
     <!-- sidenav end -->
   </div>
   <!-- wrapper end -->
   @include('layouts.js.jquery')
   @include('layouts.js.script')
 </body>

 </html>