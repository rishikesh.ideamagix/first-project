<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  @include('layouts.styles')
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    
    <main>
          <!-- user profile avatar start -->
          @include('layouts.userheader')
          <!-- user profile avatar end -->
          <!-- result grid start -->
          <section class="result-grid pb-5 px-2 px-lg-0">
            <div class="container-fluid px-2">
              @include('layouts.userdropdown')

              <div style="text-align:center;">
                  <span style="color:#5CD9D3;font-size: 28px;margin-bottom: 15px;" class="mr-2">{{$data->name}}</span>
              </div>
              <div class="d-flex justify-content-end flex-wrap">
                <div>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="background:transparent; color:#5CD9D3;border-color:#5CD9D3;">
                    Edit Gallery
                  </button>
                  <div class="modal" id="myModal">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Edit Reference Gallery</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <form action="/editnewrefgall" id="newgalform" method="POST">
                            @csrf
                            <input type="hidden" name="id" class="w-100" value="{{$data->id}}">
                            <input type="text" name="name" class="w-100" placeholder="Gallery Name" required="" value="{{$data->name}}">
                          </form>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                          <button type="submit" form="newgalform" class="btn btn-success">Update</button>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                      </div>
                    </div>
                  </div>

                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModald" style="background:transparent; color:#5CD9D3;border-color:#5CD9D3;">
                    Delete Gallery
                  </button>
                  <div class="modal" id="myModald">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Delete {{$data->name}}</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                          <form action="/deletegal" id="delgalform" method="POST">
                            @csrf
                            <input type="hidden" name="id" class="w-100" value="{{$data->id}}">
                            <!-- <input type="text" name="name" class="w-100" placeholder="Gallery Name" required="" value="{{$data->name}}"> -->
                            <h5>Are you sure? The gallery cant be recoverd later.</h5>
                          </form>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                          <button type="submit" form="delgalform" class="btn btn-danger">Confirm and delete</button>
                          <!-- <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @if(count($images)>0)  
                <div id="mecy" class="mt-5">
                  @foreach($images as $datanew)
                    <div class="box">
                      <a href="/image/{{$datanew->id}}" title="" class="d-block">
                        <span class="flag-rejected deletebutton" data-imgid='{{$datanew->id}}'>Delete</span>
                        <img src="/images/compressed/{{$datanew->image}}" alt="">
                        <div class="home-page-profile-container">
                          <div class="user-detail d-flex align-items-center justify-content-between">
                            <div class="d-flex align-items-center" style="flex-basis: 74%;">
                              @if($datanew->claimed == 'yes')
                                <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle">
                              @else  
                                <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                              @endif
                              @if($datanew->artist_name != '')
                                <p class="mb-0">{{$datanew->artist_name}}</p>
                              @elseif($datanew->movie_name != '')
                                <p class="mb-0">{{$datanew->movie_name}}</p>
                              @else
                                <p class="mb-0">Unknown</p>
                              @endif
                            </div>
                            <div class="" style="flex-basis: 70px;">
                              <div class="likes-wrap d-flex justify-content-end">
                                <!--<div class="counts-container d-flex align-items-center mr-2">-->
                                <!--  <i class="fas fa-share"></i>-->
                                <!--</div>-->
                                <div class="counts-container d-flex align-items-center">
                                  <i class="far fa-comment"></i>
                                  <span class="count">
                                    {{$datanew->comment_count}}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  @endforeach
                </div>
              @else
                <div class="text-center px-3" style="color: #5CD9D3;padding: 10% 0">
                  <h3>No images in this gallery</h3>
                </div>
              @endif
            </div>
          </section>
          
          <div class="modal" id="myModalimgd">
                <div class="modal-dialog">
                  <div class="modal-content" style="background:#404040 !important; color: #5CD9D3 !important;">

                    <!-- Modal Header -->
                    <div class="modal-header" style="border:0;padding: 0 10px 5px 0;">
                      <!--<h4 class="modal-title">Delete Image from Gallery?</h4>-->
                      <button type="button" class="close" data-dismiss="modal" style="color:#5CD9D3 !important;">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="padding: 0 0 0 11px;">
                      <form action="/deletegal" id="delgalform" method="POST">
                        @csrf
                        <input type="hidden" id="idd" name="id" class="w-100" value="">
                        <h5>Are you sure you want to remove the image from this reference gallery ?</h5>
                      </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer" style="border:0;">
                      <button id="delurlbutton" type="button" form="" class="btn btn-danger" data-delurl="" style="background:#5CD9D3 !important; border-color:#5CD9D3 !important;">Confirm and delete</button>
                    </div>

                  </div>
                </div>
              </div>
          <!-- result grid end -->
        </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  @include('layouts.js.jquery')
  @include('layouts.js.niceselect')
  @include('layouts.js.script')
  @include('layouts.js.macy')
  <script src="/assets/vendor/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    $('#userpages').on('change', function() {
      var link = this.value;
      window.location.href = link;
    });
    
    $("#delurlbutton").click(function(e){
        var finalurl = $(this).attr('data-delurl');
        $.ajax({
            url: finalurl,
            type: "post",
            success: function(data){
                // console.log(data);
                location.reload();
            }
        });
    });
    $(".deletebutton").click(function(e){
        // console.log('aaaa')
        e.preventDefault();
        var imageid = $(this).attr('data-imgid');
        $('#myModalimgd').modal('toggle');
        var url = '/addtorefgal/'+imageid+'/none';
        $('#myModalimgd').find('#delurlbutton').attr('data-delurl',url);
    });
  </script>
</body>

</html>