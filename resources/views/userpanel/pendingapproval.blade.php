<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  @include('layouts.styles')
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    
    <main>
          <!-- user profile avatar start -->
          @include('layouts.userheader')
          <!-- user profile avatar end -->
          <!-- result grid start -->
          <section class="result-grid pb-5 px-2 px-lg-0">
            <div class="container-fluid px-2">
              @include('layouts.userdropdown')
               @if(count($data)>0) 
                <div id="mecy">
                  @foreach($data as $datanew)
                    <div class="box">
                      <a href="/image/{{$datanew->id}}" title="" class="d-block">
                        <img src="/images/compressed/{{$datanew->image}}" alt="">
                        <div class="home-page-profile-container">
                          <div class="user-detail d-flex align-items-center justify-content-between">
                            <div class="d-flex align-items-center" style="flex-basis: 74%;">
                              @if($datanew->claimed == 'yes')
                                <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle">
                              @else  
                                <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                              @endif
                              @if($datanew->artist_name != '')
                                <p class="mb-0">{{$datanew->artist_name}}</p>
                              @elseif($datanew->movie_name != '')
                                <p class="mb-0">{{$datanew->movie_name}}</p>
                              @else
                                <p class="mb-0">Unknown</p>
                              @endif
                            </div>
                            <div class="" style="flex-basis: 70px;">
                              <div class="likes-wrap d-flex justify-content-end">
                                <!--<div class="counts-container d-flex align-items-center mr-2">-->
                                <!--  <i class="fas fa-share"></i>-->
                                <!--</div>-->
                                <div class="counts-container d-flex align-items-center">
                                  <i class="far fa-comment"></i>
                                  <span class="count">
                                    {{$datanew->comment_count}}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </a>
                    </div>
                  @endforeach
                </div>
               @else
                <div class="text-center px-3" style="color: #5CD9D3;padding: 10% 0">
                  <h3>No favourite images found</h3>
                </div>
              @endif
            </div>
          </section>
          <!-- result grid end -->
        </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  @include('layouts.js.jquery')
  @include('layouts.js.niceselect')
  @include('layouts.js.script')
  @include('layouts.js.macy')
  <script type="text/javascript">
    $('#userpages').on('change', function() {
      var link = this.value;
      window.location.href = link;
    });
  </script>
</body>

</html>