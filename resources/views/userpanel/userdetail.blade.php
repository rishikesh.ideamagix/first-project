<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO | Details</title>

  <!-- css files start -->
  <!-- bootstrap 4.0.0 -->
  <link rel="stylesheet" href="/assets/vendor/bootstrap-4.0.0/dist/css/bootstrap.min.css">
  <!-- font awesome 5.11.2 -->
  <link rel="stylesheet" href="/assets/vendor/fontawesome-free-5.11.2-web/css/all.min.css">
  <!-- owl carousel 2.3.4 -->
  <link rel="stylesheet" href="/assets/vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
  <!-- animate.css 3.7.2 -->
  <link rel="stylesheet" href="/assets/vendor/animate.css.3.7.2/animate.3.7.2.css">
  <!-- animate on scroll -->
  <link rel="stylesheet" href="/assets/vendor/aos-master/dist/aos.css">
  <!-- nice select 1.1.0 -->
  <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
  <!-- custom -->
  <link rel="stylesheet" href="/assets/css/style.css">
  <!-- css files end -->
</head>

<body>
  @php
    if(Auth::check())
    {
        $signedinid = Auth::user()->name;
        $signedinname = Auth::user()->name;
        $signedinemail = Auth::user()->email;
    }else{
        $signedinname = "notsi";
        $signedinemail = "notsi";
        $signedinid = 0;
    }
  @endphp
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <!-- user profile avatar start -->
      @include('layouts.userheader')
      <!-- user profile avatar end -->
      <!-- user panel page selector start -->
      <div class="user-panel-page-selector">
        <div class="container-fluid px-2">
          @include('layouts.userdropdown')
        </div>
      </div>
      <!-- user panel page selector end -->
      <!-- change password start -->
      <div class="change-password py-5">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <div class="form-wrap p-3">
                <h4 class="mb-3">User Details</h4>
                <form action="/updateuser" method="post" class="w-100" enctype= multipart/form-data>
                  @csrf
                  <div class="input-wrap mb-4">
                    <input type="text" class="w-100 p-2" placeholder="Enter your name" value="{{$data->name}}" name="name" required="">
                  </div>
                  <div class="input-wrap mb-4">
                    <input type="hidden" class="w-100 p-2" placeholder="Enter your email id" value="{{$data->email}}" name="email" required="" readonly="">
                  </div>
                  <div class="input-wrap mb-4 d-none">
                    <input type="text" class="w-100 p-2" placeholder="Enter your contact number" value="{{$data->number}}" name="number" required="">
                  </div>
                  <div class="input-wrap mb-4" style="color: white">
                    <label>Artist Level</label> <br>
                    <label class="radio-inline mr-2">
                      <input type="radio" name="artisttype"  value="junior" class="mr-1" @if($data->artist_type == 'junior') checked @endif >Junior Level
                    </label>
                    <label class="radio-inline mr-2">
                      <input type="radio" name="artisttype" value="middle" class="mr-1" @if($data->artist_type == 'middle') checked @endif>Mid Level
                    </label>
                    <label class="radio-inline mr-2">
                      <input type="radio" name="artisttype" value="senior" class="mr-1" @if($data->artist_type == 'senior') checked @endif>Senior Level
                    </label>
                  </div>
                  <div class="input-wrap mb-4 seniordiv d-none">
                    <span class="" style="color:white;">“Account would be manually reviewed. It usually takes upto 24 hours or more for manual approval.”</span>
                    <input type="text" class="w-100 p-2 " placeholder="Enter Website/Reel URL" name="artisturl" value="{{$data->artist_url}}">
                  </div>
                  <div class="input-wrap mb-4 seniordiv d-none">
                    <input  type="text" class="w-100 p-2 " placeholder="Enter your Linkedin Profile Link" name="linkedinprofile" value="{{$data->linkedinprofile}}">
                  </div>
                  <div class="input-wrap mb-4 seniordiv d-none">
                    <input  type="number" min="0" class="w-100 p-2  " placeholder="Number of Years in Industry" name="industryexperience" value="{{$data->industryexperience}}">
                  </div>

                  <div class="input-wrap mb-4" style="color: white">
                    <input type="file" class="w-100 p-2" name="image" placeholder="Enter your avatar">
                  </div>
                  <div class="input-wrap">
                    <button class="w-100 p-2">
                      Change Details
                    </button>
                  </div>
                </form>
                <p class="mb-0 mt-1"><a href="/changemypassword" title="Change Password">Change Password</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- change password start -->
    </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  
  @include('layouts.js.jquery')
  @include('layouts.js.niceselect')
  @include('layouts.js.script')
  <script type="text/javascript">
    $('#userpages').on('change', function() {
      var link = this.value;
      window.location.href = link;
    });

    var utype = '<?php echo $data->artist_type; ?>';;
    if (utype == 'senior') {
      $('.seniordiv').removeClass('d-none');
    }else{
      $('.seniordiv').addClass('d-none');
    }

    $('input[type=radio]').change(function() {
        if (this.value == 'senior') {
          $('.seniordiv').removeClass('d-none');
        }else{
          $('.seniordiv').addClass('d-none');
        }
        
    });
  </script>
</body>

</html>