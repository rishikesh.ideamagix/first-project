<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  @include('layouts.styles')

  <style type="text/css">
    .collection-wrap {
          background-color: #F5F5F5;
        }
        .collection-wrap h3 {
          font-size: 20px;
        }
        .collection-wrap span {
          font-weight: 600;
        }
        .collection-wrap .action-group a {
          background-color: #ABABAB;
          width: 25px;
          height: 25px;
          font-size: 13px;
          color: #fff !important;
          text-decoration: none !important;
        }
        .collection-wrap figure img {
          width: 100%;
          height: 100%;
          -o-object-fit: cover;
             object-fit: cover;
          position: absolute;
          top: 50%;
          left: 50%;
          -webkit-transform: translate(-50%, -50%);
              -ms-transform: translate(-50%, -50%);
                  transform: translate(-50%, -50%);
        }

        .collection-wrap figure {
          position: relative;
          height: 150px;
        }

  </style>
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    
    <main>
          <!-- user profile avatar start -->
          @include('layouts.userheader')
          <!-- user profile avatar end -->
          <!-- result grid start -->
          <section class="result-grid pb-5 px-2 px-lg-0">
            <div class="container-fluid px-5">
              @include('layouts.userdropdown')

              
                <div class="d-flex justify-content-end">
                  <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#myModal" style="background:transparent; color:#5CD9D3;border-color:#5CD9D3;">
                    Add New Library
                  </button>
                </div>
              
              <div class="modal" id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h4 class="modal-title">Add New Reference Library</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                      <form action="/addnewrefgall" id="newgalform" method="POST">
                        @csrf
                        <input type="hidden" name="imgid" class="w-100" value="" required="">
                        <input type="text" name="name" class="w-100" placeholder="Library Name" required="">
                      </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" form="newgalform" class="btn btn-success">Add</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                  </div>
                </div>
              </div>



              

              @if(count($data)>0)  
                <div class="row">
                  
                
                @foreach($data as $refgall)
                  @php
                    $finalimglist = $refgall->images;
                    $finalimglistarr = explode(',', $finalimglist);
                    $finalimglistarr = array_filter($finalimglistarr);
                    $finalimglistarr = array_unique($finalimglistarr);
                    $imagecount = count($finalimglistarr);
                    $imagedone = 'no';
                    $imageid = '';
                  @endphp
                  <div class="col-lg-6 col-xl-4 mb-4" >
                    <div class="collection-wrap p-3" style="background: transparent;border: 2px solid #5CD9D3; color:#5CD9D3;">
                      <div class="row">
                        <div class="col-md-4">
                          @foreach($images as $image)
                            @if($imagecount > 0)
                              @if($image->id == $finalimglistarr['0'])
                                @php
                                  $imageid = $image->image;
                                  $imagedone = 'yes';
                                @endphp
                              @endif
                            @else
                              @php
                                $imageid = "noimage.jpg";
                                $imagedone = 'yes';
                              @endphp
                            @endif

                            @php if($imagedone == 'yes') break; @endphp
                          @endforeach
                          <figure class="w-100 mb-md-0" style="position: relative;">
                            <!-- <img src="http://crextal.com/cgresettlement/assets/images/about-us/about-us.png" alt=""> -->
                            <img src="/images/compressed/{{$imageid}}" alt="" >
                          </figure>
                        </div>
                        <div class="col-md-8 d-flex flex-wrap flex-column">
                          <h3><a href="/detailedgallery/{{$refgall->id}}" style="text-decoration: none;color: #5CD9D3">{{$refgall->name}}</a></h3>
                          <div class="mt-auto flex-wrap d-sm-flex justify-content-between align-items-center">
                            <span class="mb-2 mb-sm-0 d-block">{{$imagecount}} Image(s)</span>
                            <div class="action-group d-flex flex-wrap justify-content-lg-center justify-content-xl-between">
                              <a href="" title="Edit" class="edit d-flex align-items-center justify-content-center rounded mr-2" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaledit" onclick="editmodaldet(this)" data-name="{{$refgall->name}}" data-id="{{$refgall->id}}"><i class="fas fa-pencil-alt"></i></a>
                              <a href="" title="Delete" class="delete d-flex align-items-center justify-content-center rounded" data-toggle="modal" data-target="#myModald" onclick="deletemodaldet(this)" data-id="{{$refgall->id}}"><i class="fas fa-trash-alt"></i></a>
                            </div>
                          </div>
                        </div>
                        
                        <!-- <div class="col-md-3 d-flex flex-column justify-content-between align-items-md-end">
                          <span>{{$imagecount}} Image(s)</span>
                          <div class="action-group d-flex flex-wrap justify-content-lg-center justify-content-xl-between mt-3 mt-md-0">
                            <a href="" title="Edit" class="edit d-flex align-items-center justify-content-center rounded mr-2 mr-lg-0 mb-lg-2 mb-xl-0 mr-xl-2" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaledit" onclick="editmodaldet(this)" data-name="{{$refgall->name}}" data-id="{{$refgall->id}}"><i class="fas fa-pencil-alt"></i></a>
                            <a href="" title="Delete" class="delete d-flex align-items-center justify-content-center rounded" data-toggle="modal" data-target="#myModald" onclick="deletemodaldet(this)" data-id="{{$refgall->id}}"><i class="fas fa-trash-alt"></i></a>
                          </div>
                        </div> -->
                      </div>
                    </div>
                  </div>
                @endforeach

              <div class="modal" id="myModaledit">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h4 class="modal-title">Edit Reference Library</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                      <form action="/editnewrefgall" id="editform" method="POST">
                        @csrf
                        <input type="hidden" id="id" name="id" class="w-100" value="">
                        <input type="text" id="name" name="name" class="w-100" placeholder="Library Name" required="" value="">
                      </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" form="editform" class="btn btn-success">Update</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                  </div>
                </div>
              </div>

              <div class="modal" id="myModald">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h4 class="modal-title">Delete Reference Library?</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                      <form action="/deletegal" id="delgalform" method="POST">
                        @csrf
                        <input type="hidden" id="idd" name="id" class="w-100" value="">
                        <h5>Are you sure? The Library cant be recoverd later.</h5>
                      </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" form="delgalform" class="btn btn-danger">Confirm and delete</button>
                    </div>

                  </div>
                </div>
              </div>
              </div>
              @else
                <div class="text-center px-3" style="color: #5CD9D3;padding: 10% 0">
                  <h3>No reference Library found</h3>
                </div>
              @endif
            </div>
          </section>
          <!-- result grid end -->
        </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  @include('layouts.js.jquery')
  @include('layouts.js.niceselect')
  @include('layouts.js.script')
  @include('layouts.js.macy')
  <script type="text/javascript">
    $('#userpages').on('change', function() {
      var link = this.value;
      window.location.href = link;
    });


    function editmodaldet(xyz) {
      var name = $(xyz).attr('data-name');
      var id = $(xyz).attr('data-id');
      
      $('#editform').find('#name').val(name);
      $('#editform').find('#id').val(id);
    }

    function deletemodaldet(xyz) {
      var id = $(xyz).attr('data-id');
      console.log(id);
      $('#delgalform').find('#idd').val(id);
    }
  </script>
</body>

</html>