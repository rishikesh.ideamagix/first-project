<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

  @include('layouts.styles')

  <style type="text/css">
    .collection-wrap {
      background-color: #F5F5F5;
    }
    .collection-wrap h3 {
      font-size: 20px;
    }
    .collection-wrap span {
      font-weight: 600;
    }
    .collection-wrap .action-group a {
      background-color: #ABABAB;
      width: 25px;
      height: 25px;
      font-size: 13px;
      color: #fff !important;
      text-decoration: none !important;
    }
    .collection-wrap figure img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  </style>
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    
    <main>
          <!-- user profile avatar start -->
          @include('layouts.userheader')
          <!-- user profile avatar end -->
          <!-- result grid start -->
          <section class="result-grid pb-5 px-2 px-lg-0">
            <div class="container-fluid px-5">
              @include('layouts.userdropdown')

              <button type="button" class="btn btn-primary mb-2" data-toggle="modal" data-target="#myModal">
                Add New Gallery
              </button>
              <div class="modal" id="myModal">
                <div class="modal-dialog">
                  <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                      <h4 class="modal-title">Add New Reference Gallery</h4>
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                      <form action="/addnewrefgall" id="newgalform" method="POST">
                        @csrf
                        <input type="text" name="name" class="w-100" placeholder="Gallery Name" required="">
                      </form>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <button type="submit" form="newgalform" class="btn btn-success">Add</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                  </div>
                </div>
              </div>



              

              @if(count($data)>0)  
                <div class="row">
                  
                
                @foreach($data as $refgall)
                  <div class="col-lg-6 mb-4">
                    <div class="collection-wrap p-3">
                      <div class="row">
                        <div class="col-md-4">
                          <figure class="w-100 mb-md-0">
                            <img src="http://crextal.com/cgresettlement/assets/images/about-us/about-us.png" alt="">
                          </figure>
                        </div>
                        <div class="col-md-6">
                          <h3>title</h3>
                        </div>
                        <div class="col-md-2 d-flex flex-column justify-content-between align-items-md-end">
                          <span>1 item</span>
                          <div class="action-group d-flex flex-wrap justify-content-lg-center justify-content-xl-between mt-3 mt-md-0">
                            <a href="" title="Edit" class="edit d-flex align-items-center justify-content-center rounded mr-2 mr-lg-0 mb-lg-2 mb-xl-0 mr-xl-2"><i class="fas fa-pencil-alt"></i></a>
                            <a href="" title="Delete" class="delete d-flex align-items-center justify-content-center rounded"><i class="fas fa-trash-alt"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div>
                    <span style="color:#5CD9D3;font-size: 28px">{{$refgall->name}} </span><a href="/detailedgallery/{{$refgall->id}}"> view more</a>
                  </div>
                  @php
                    $finalimglist = $refgall->images;
                    $finalimglistarr = explode(',', $finalimglist);
                    $finalimglistarr = array_filter($finalimglistarr);
                    $finalimglistarr = array_unique($finalimglistarr);
                  @endphp
                  <div id="mecy" class="mecyclass">
                    @php $empty = 'yes'; @endphp
                    @foreach($finalimglistarr as $key=>$finalimg)
                      @foreach($images->where('id', $finalimg) as $datanew)
                        @php $empty = 'no'; @endphp
                        <div class="box">
                          <a href="/image/{{$datanew->id}}" title="" class="d-block">
                            @if($datanew->status == 'approved')
                              <span class="flag-approved">Approved</span>
                            @elseif($datanew->status == 'pending')
                              <span class="flag-pending">Pending</span>
                            @elseif($datanew->status == 'rejected')
                              <span class="flag-rejected">Disapproved</span>
                            @endif
                            <img src="/images/compressed/{{$datanew->image}}" alt="">
                            <div class="home-page-profile-container">
                              <div class="user-detail d-flex align-items-center justify-content-between">
                                <div class="d-flex align-items-center" style="flex-basis: 74%;">
                                  @if($datanew->claimed == 'yes')
                                    <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle">
                                  @else  
                                    <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                                  @endif
                                  @if($datanew->artist_name != '')
                                    <p class="mb-0">{{$datanew->artist_name}}</p>
                                  @elseif($datanew->movie_name != '')
                                    <p class="mb-0">{{$datanew->movie_name}}</p>
                                  @else
                                    <p class="mb-0">Unknown</p>
                                  @endif
                                </div>
                                <div class="" style="flex-basis: 70px;">
                                  <div class="likes-wrap d-flex justify-content-end">
                                    <div class="counts-container d-flex align-items-center">
                                      <i class="far fa-comment"></i>
                                      <span class="count">
                                        {{$datanew->comment_count}}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                        </div>
                      @endforeach
                      @php if ($key == 10)
                        break; 
                      @endphp
                    @endforeach
                    
                    @if($empty == 'yes')
                      <div class="text-center px-3 w-100" style="color: #5CD9D3;padding: 5% 0">
                        <h3>No Images in this gallery</h3>
                      </div>
                    @endif
                  </div>
                @endforeach
              </div>
              @else
                <div class="text-center px-3" style="color: #5CD9D3;padding: 10% 0">
                  <h3>No reference gallery found</h3>
                </div>
              @endif
            </div>
          </section>
          <!-- result grid end -->
        </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  @include('layouts.js.jquery')
  @include('layouts.js.niceselect')
  @include('layouts.js.script')
  @include('layouts.js.macy')
  <script type="text/javascript">
    $('#userpages').on('change', function() {
      var link = this.value;
      window.location.href = link;
    });
  </script>
</body>

</html>