<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="/atmo-favicon.ico" sizes="32x32" type="">
  
  <link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" >
  <title>ATMO VFX</title>
  
  
  <scrip src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/js/all.js"></scrip>
  
  <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
    
    <link rel="stylesheet" href="/assets/css/style.css">
    
    
  <style>
      .hoverchnage:hover{
          color:#5FD9D3 !important;
      }

      .greyimage{
        -webkit-filter: grayscale(100%);
                filter: grayscale(100%);
      }

      select {
        /*width:100px;
        margin-left:5px !important; 
        float:left;
        appearance:none;
        -webkit-appearance:none;
        -moz-appearance:none;
        -ms-appearance:none;
        border:2px solid #000;*/
        /*background:url('http://www.free-icons-download.net/images/small-down-arrow-icon-15593.png');*/
        /*background-repeat:no-repeat;
        background-size:16px 17px;
        background-position:right 0px;*/
        
      }
      
      
      .refgaldropdown {
        /*background: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 4 5'%3e%3cpath fill='%23222' d='M2 0L0 2h4zm0 5L0 3h4z'/%3e%3c/svg%3e") no-repeat right 1rem center/8px 10px;*/
        
      }
      
      select option {
        /*margin: 40px;*/
        /*background: rgba(0, 0, 0, 0.3);*/
        /*color: #fff;*/
        /*text-shadow: 0 1px 0 rgba(0, 0, 0, 0.4);*/
        background: #000;
        color: #5CD9D3;
    }
      
  </style>
@include('layouts.styles')



</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <!-- home page search form start -->
      <div class="home-page-search-form" style="background-image:url('/assets/images/{{$link->search_banner_image}}');">
        <div class="container">
          <div class="row justify-content-center">
            <!-- <h1 class="col-12 text-center mb-1">ATMO</h1> -->
            <img src="/atmologotwo.png" class="mt-5 mb-2">
            <p class="col-12 text-center">Here you will find user-uploaded Junior/Mid/WIP work to view and comment on.</p>
            <form action="/jmsearch" method="post" class="col-md-7">
              @csrf
              <div class="input-wrap w-100" style="font-size:12px;">
                @if($datatype == 'search')
                  <input  type="text" placeholder="Search by artist, description, movie name or tags (Comma seperated with no space around comma's)" class="py-2 py-lg-3 pl-2 pl-sm-3 w-100 border-0 searchbox" name="search_field" value="{{$search}}" autocomplete="off">
                @else
                  <input  type="text" placeholder="Search by artist, description, movie name or tags (Comma seperated with no space around comma's)" class="py-2 py-lg-3 pl-2 pl-sm-3 w-100 border-0 searchbox" name="search_field" autocomplete="off">
                @endif
                <div id="countryList">
                </div>
                <button class="border-0 py-0 h-100 px-4">
                  <span class="fas fa-search"></span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- home page search form end -->
      <!-- showcase section start -->
      <div class="showcase px-2 px-lg-0">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="showcase-banner text-center d-flex align-items-center w-100 justify-content-center">
              {{$link->link}}
            </div>
          </div>
        </div>
      </div>
      <!-- showcase section end -->
      <!-- categories filter start -->
      <div class="categories-filter">
        <div class="container-fluid">
          <div class="row justify-content-between px-2">
            <div class="categories-tab col-12 p-0 mb-4 flex-wrap justify-content-between animated fadeIn">
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'all') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/community">All Results</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'matte-painting-set-extension') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/community/matte-painting-set-extension">Matte Painting Set Extension</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'traditional-painting') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/community/traditional-painting">Traditional Painting</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'photography-composition') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/community/photography-composition">Photography Composition</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'movies') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/community/movies">Movies/TV</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu">
                <button @if($datatype == 'games') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/community/games">Games</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- categories filter end -->
      <!-- result grid start -->
      <section class="result-grid py-5 px-2 px-lg-0 my-5">
        <div class="container-fluid px-2">
          @if(count($data) < 1)
            <div class="mt-5 text-center" style="color: #5CD9D3">
              <h3>No Image Found</h3>
            </div>
          @endif
          <div id="mecy" class="image-container">
            <!-- Automatic data comes here -->
            @foreach($data as $key=>$datanew)
              <div class="box" data-aos="fade-up">
                <a href="/image/{{$datanew->id}}" title="" class="d-block">
                  <!--@if($key <= 10)-->
                  <!--  <img src="/images/compressed/{{$datanew->image}}"  alt="" >-->
                  <!--@else-->
                  <!--  <img data-src="/images/compressed/{{$datanew->image}}" data-srcset="/images/compressed/{{$datanew->image}}"  alt="" class="lazy" src="/image-loading.gif" >-->
                  <!--@endif-->
                  <!--<img data-src="/images/compressed/{{$datanew->image}}" data-srcset="/images/compressed/{{$datanew->image}}"  alt="" class="lazy" src="/verify.png" >-->
                  <img src="/images/compressed/{{$datanew->image}}"  class="imagecolor" alt="" >
                </a>
                @php 
                  $artistlink = preg_replace('/\s+/', '-', $datanew->artist_name);
                  $titlelink = preg_replace('/\s+/', '-', $datanew->movie_name);
                  $blanklink = 'unknown';
                  $newlink = '';

                  if($datanew->artist_name != ''){
                    $newlink = 'artist/'.$artistlink;
                  }else if($datanew->movie_name != ''){
                    $newlink = 'title/'.$titlelink;
                  }else{
                    $newlink = 'artist/unknown';
                  }
                @endphp

                <div class="home-page-profile-container">
                  <div class="user-detail d-flex align-items-center justify-content-between">
                    <a href="/images/{{$newlink}}" class="d-flex align-items-center fl70p hoverchnage ">
                      @if($datanew->claimed == 'yes')
                        <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle">
                      @else  
                        <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                      @endif
                      @if($datanew->artist_name != '')
                        <p class="mb-0">
                          {{$datanew->artist_name}}
                          

                          @if($datanew->claimed == 'yes')
                            @foreach($users as $user)
                              @if($datanew->user_id == $user->id)
                                @if($user->artist_type == 'senior'  && $user->artist_type_verified == 'yes' )
                                  <img src="/verify.png" style="width: 20px !important">
                                @endif
                              @endif
                            @endforeach
                          @endif
                        
                        </p>
                      @elseif($datanew->movie_name != '')
                        <p class="mb-0">{{$datanew->movie_name}}</p>
                      @else
                        <p class="mb-0">Unknown</p>
                      @endif
                    </a>
                    <div class="fl74p" style="">
                      <a href="/image/{{$datanew->id}}" class="likes-wrap d-flex justify-content-end">
                        <!--<div class="counts-container d-flex align-items-center mr-2">-->
                        <!--  <i class="fas fa-share"></i>-->
                        <!--</div>-->
                        <div class="counts-container d-flex align-items-center">
                          @php $matched = 0; @endphp
                          @foreach($favourites as $favourite)
                            @if($favourite->user_id == $datanew->id)
                              @php $matched = 1; @endphp
                            @else
                              @php $matched = 0; @endphp
                            @endif
                          @endforeach

                          @if(Auth::check())
                            @php $signedinid = Auth::user()->id; @endphp
                            @if($matched == 1)
                              
                              <a class="fa fa-heart mr-1 unlikebutton" onClick="unlike(this)" data-link='/unlike/{{$signedinid}}/{{$datanew->id}}' style="color: red; cursor:pointer"></a>
                              <a class="far fa-heart mr-1 likebutton d-none" onClick="like(this)" data-link='/like/{{$signedinid}}/{{$datanew->id}}' style="color: white; cursor:pointer"></a>
                            @else
                              
                              <a class="far fa-heart mr-1 likebutton" onClick="like(this)" data-link='/like/{{$signedinid}}/{{$datanew->id}}' style="color: white;cursor:pointer"></a>
                              <a class="fa fa-heart mr-1 unlikebutton d-none" onClick="unlike(this)" data-link='/unlike/{{$signedinid}}/{{$datanew->id}}' style="color: red; cursor:pointer"></a>
                            @endif
                          @else
                            
                            <i class="far fa-heart mr-1" style="color: white; cursor:pointer"></i>
                          @endif
                          @php $matchedgal = 0; @endphp
                          @foreach($refgalleries as $gallery)
                            @php
                              $finalimglistarr = explode(',', $gallery->images);
                              if(in_array($datanew->id, $finalimglistarr)){
                                  $matchedgal = $gallery->id;
                              }
                            @endphp

                          @endforeach
                          
                          @if(Auth::check())
                            <select  data-imgid="{{$datanew->id}}" class="mr-1 refgaldropdown torefgal " style="max-width: 14px; background: transparent !important; border: 0; color: #5CD9D3; cursor:pointer">
                              <option disabled selected value>Add to reference library</option>
                              <option value="none">Remove</option>
                              @foreach($refgalleries as $refgallery)
                                <option value="{{$refgallery->id}}" @if($matchedgal == $refgallery->id) selected @endif>{{$refgallery->name}}</option>
                              @endforeach
                              <option value="addnew">Add New Gallery</option>
                            </select>
                          @endif

                          
                          <i class="far fa-comment" style="color: white !important; cursor:pointer"></i>
                          <span class="count">
                            {{$datanew->comment_count}}
                          </span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="mt-5">
            {{$data->links()}}
          </div>
          <div id="scrolldiv" class="text-center pt-4 d-none">
            <span class="d-block mb-1" style="color: #5CD9D3">Scroll to load more</span>
            <img src="/scroll3.gif" style="height: 30px">
          </div>
        </div>
      </section>
      <!-- result grid end -->
    </main>


    <div class="modal" id="myModaladd">
        <div class="modal-dialog">
          <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Add New Reference Library</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
              <form action="/addnewrefgall" id="newgalform" method="POST">
                @csrf
                <input type="hidden" name="imgid" id="newgalimgid" class="w-100" value="" required="">
                <input type="text" name="name" class="w-100" placeholder="Gallery Name" required="">
              </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" form="newgalform" class="btn btn-success">Add</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

          </div>
        </div>
    </div>


    <a id="bwbutton" class="d-flex align-items-center justify-content-center" style="background:#fff;">View Images in B/W <i class="far fa-images ml-2"></i></a>
    <a id="colbutton" class="d-flex align-items-center justify-content-center">View Images in Color  <i class="fas fa-images ml-2"></i> </a>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->

    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  <!-- javascript files start -->
  @include('layouts.js.jquery')
  @include('layouts.js.macy')
  @include('layouts.js.script')
  @include('layouts.js.likeunlikeajax')
  @include('layouts.js.niceselect')
  <script src="/assets/vendor/bootstrap-4.0.0/dist/js/bootstrap.min.js"></script>

  <!-- javascript files end -->

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
  <script type="text/javascript">
    var lastsearch = '';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      

        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;




    });

    $('.searchbox').keyup(function(){ 
            // console.log('azazaza')
           var query = $(this).val();
           if(query == ''){
               lastsearch = query;
           }
           console.log('query');
           if(query.indexOf(',') != -1){
               console.log('comma  hai');
               var queryarr = query.split(",");
               var queryarrlen = queryarr.length;
               query = queryarr[queryarrlen-1];
           }else{
               console.log('comma nahi hai');
           }



           console.log(query);
           if(query != '')
           {
            var _token = $('input[name="_token"]').val();
            $.ajax({
             url:"{{ route('jmautocomplete.fetch') }}",
             method:"POST",
             data:{query:query, _token:_token},
             success:function(data){
              $('#countryList').fadeIn();  
              $('#countryList').html(data);
              var str2 = "<li";
              if(data.indexOf(str2) != -1){
                  $('.searchul').removeClass('d-none');
              }else{
                  $('.searchul').addClass('d-none');
              }
             }
            });
           }
       });

       // $(document).on('click', function(){  
       //     // $('#country_name').val($(this).text());  
       //     $('#countryList').fadeOut();  
       // });  

       $(document).on('click', 'li', function(){  
            console.log(lastsearch);
            // $('.searchbox').val($(this).text());
            var res = $(this).text();
            console.log(res);
            if (lastsearch != '') {
              var resnew = lastsearch+res+',';  
            }else{
              var resnew = lastsearch+res+',';
            }
            
            console.log(resnew);
            lastsearch = resnew;
            $('.searchbox').val(resnew);
            $('#countryList').fadeOut();  

        });

       
  

      function searchitemfunc(xyz) {
        var val = $(this).attr('data-value');
        
      }
  
  
    $(".category-button").click(function(){
      var link = $(this).attr('data-link');
      window.location.href = link;
    });
    // $(function() {
    //     $('.lazy').lazy();
    // });
    
    
    document.addEventListener("DOMContentLoaded", function() {
      let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
      let active = false;
    
      const lazyLoad = function() {
        if (active === false) {
          active = true;
    
          setTimeout(function() {
            lazyImages.forEach(function(lazyImage) {
              if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
                lazyImage.src = lazyImage.dataset.src;
                lazyImage.srcset = lazyImage.dataset.srcset;
                lazyImage.classList.remove("lazy");
    
                lazyImages = lazyImages.filter(function(image) {
                  return image !== lazyImage;
                });
    
                if (lazyImages.length === 0) {
                  document.removeEventListener("scroll", lazyLoad);
                  window.removeEventListener("resize", lazyLoad);
                  window.removeEventListener("orientationchange", lazyLoad);
                }
              }
            });
    
            active = false;
          }, 200);
        }
      };
      
      
      
      document.addEventListener("scroll", lazyLoad);
      window.addEventListener("resize", lazyLoad);
      window.addEventListener("orientationchange", lazyLoad);
    });



    var btnnew = $('#bwbutton');
    var btnnewcol = $('#colbutton');


    btnnew.addClass('show');
    btnnew.on('click', function(e) {
      e.preventDefault();
      
        $('.imagecolor').addClass('greyimage');
        btnnew.removeClass('show');
        btnnewcol.addClass('show');
    });


    
    
    btnnewcol.on('click', function(e) {
      e.preventDefault();
      
        $('.imagecolor').removeClass('greyimage');
        btnnew.addClass('show');
        btnnewcol.removeClass('show');
    });





    // $(".refgal").click(function(e){
      

    //   console.log($(e.target).siblings('.refgaldropdown'));

    //   if($(e.target).siblings('.refgaldropdown')) {
    //     // console.log(1);
    //     $(e.target).removeClass('d-none');
    //     var abc = $(e.target).siblings('.refgaldropdown');
    //     console.log(abc[0]);
    //     // $(abc[0]).removeClass('d-none');
    //     // $(abc[0]).click();
    //     $(abc[0]).show().focus().click();

    //   }

    // });

    

    $('.torefgal').on('change', function() {
        
        var galid = this.value;
        var imgid = $(this).attr('data-imgid');
        // console.log(imgid);
        if (galid == 'addnew') {
            $("#myModaladd").find('#newgalimgid').val(imgid);
            $('#myModaladd').modal('toggle');
        }else{
            var imageid = imgid;
            var url = '/addtorefgal/'+imageid+'/'+galid;
            console.log(url);
            $.ajax({
                url: url,
                type: "post",
                success: function(data){
                    console.log(data);
                }
            });
        }
        
    });


    
  </script>
    
    
    
  </script>
</body>

</html>