<!DOCTYPE html>
 <html lang="en">

 <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <meta property="og:url" content="">
   <meta property="og:title" content="">
   <meta property="og:image" content="">
   <meta property="og:site_name" content="">
   <meta property="og:description" content="">
   <meta name="author" content="">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <link rel="icon" href="" sizes="32x32" type="image/png">
   <title>ATMO | Survey</title>

   <!-- css files start -->
   <!-- bootstrap 4.0.0 -->
   <link rel="stylesheet" href="/assets/vendor/bootstrap-4.0.0/dist/css/bootstrap.min.css">
   <!-- font awesome 5.11.2 -->
   <link rel="stylesheet" href="/assets/vendor/fontawesome-free-5.11.2-web/css/all.min.css">
   <!-- owl carousel 2.3.4 -->
   <link rel="stylesheet" href="/assets/vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
   <!-- animate.css 3.7.2 -->
   <link rel="stylesheet" href="/assets/vendor/animate.css.3.7.2/animate.3.7.2.css">
   <!-- animate on scroll -->
   <link rel="stylesheet" href="/assets/vendor/aos-master/dist/aos.css">
   <!-- nice select 1.1.0 -->
   <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
   <!-- custom -->
   <link rel="stylesheet" href="/assets/css/style.css">
   <!-- css files end -->

   <link rel="stylesheet" href="/assets/vendor/select2/select2.min.css">
   
   @include('layouts.styles')
</head>

 <body >
   <!-- wrapper start -->
   <div class="wrapper" >
     <!-- header start -->
     @include('layouts.header')
     <!-- header end -->
     <!-- main start -->
        <main>
            <div style="min-height:80vh; margin: auto;" class="px-5 w-75">
            	<div class="form-wrap p-3 mt-5">
               <form method="POST" action="/add/survey" class="w-100">
                 {{ csrf_field() }}
                 <div class="my-3">
                     <div class="form-group  text-lg-left mb-1" >
                         <label for="artist"  class="text-white text-left  d-block"> Artist Type</label>
                         <select class="form-control" placeholder="Select Artist Type"  name="artisttype"   style="width: 100%;background: transparent; color:#5CD9D3;" required="">
                             <option value="Environment Artist Generalist" >Environment Artist Generalist</option>
                             <option value="2d Matte Painter" >2d Matte Painter</option>
                             <option value="Matte Painter Generalist" >Matte Painter Generalist</option>
                             <option value="Game Environment Artist" >Game Environment Artist</option>
                         </select>
                     </div>
                     <div class="form-group  text-lg-left mb-1" >
                         <label for="artist"  class="text-white text-left  d-block"> Artist Level </label>
                         <select class="form-control" placeholder="Select Artist Level"  name="artistlevel"   style="width: 100%;background: transparent; color:#5CD9D3;" required="">
                             <option value="Supervisor">Supervisor</option>
                             <option value="Lead">Lead</option>
                             <option value="Head of Department">Head of Department</option>
                             <option value="Senior">Senior</option>
                             <option value="Junior">Junior</option>
                             <option value="Mid">Mid</option>
                         </select>
                     </div>
                     <div class="row">
                      <div class="col-md-4">
                        <div class="form-group text-lg-left mb-1">
                          <label for="artist"  class="text-white text-left  d-block"> Salary</label>
                          <input class="form-control" type="number" name="salary" required="" placeholder="" style="background: transparent;color: #5CD9D3">
                        </div>
                      </div>
                      <div class="form-group text-lg-left mb-1 col-md-8">
                          <label for="artist"  class="text-white text-left  d-block"> Per</label>
                          <select class="form-control" placeholder="Select Artist Level"  name="salper"   style="width: 100%;background: transparent; color:#5CD9D3;" required="">
                              <option value="Hour">Hour</option>
                              <option value="Day">Day</option>
                              <option value="Month">Month</option>
                              <option value="Year">Year</option>
                          </select>
                      </div>  
                     </div>
                 </div>
                 <div class="input-wrap">
                   <button class="w-100 p-2">
                     Submit
                   </button>
                 </div>
               </form>
             </div>
            
            <div class="mt-5" style="color:#5CD9D3;">
                <h3>Survey Record</h3>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Artist Type</th>
                      <th scope="col">Artist Level</th>
                      <th scope="col">Salary</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($surveys as $key=>$survey)
                    @php $key++ @endphp
                        <tr>
                          <th scope="row">{{$key}}</th>
                          <td>{{$survey->artist_type}}</td>
                          <td>{{$survey->artist_level}}</td>
                          <td>{{$survey->artist_salary}}</td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $surveys->links() }} 
            </div>
            
            
            
            </div>
            
            
            <!-- <div class="container d-flex flex-wrap justify-content-center align-items-center text-center" style="height:80vh;">
                <div style="color:#5CD9D3;">
                    <h1>Coming Soon</h1>
                </div>
            </div> -->
       </main>
     <!-- main end -->
     <!-- footer start -->
     @include('layouts.footer')
     <!-- footer end -->
     <!-- sidenav start -->
     @include('layouts.sidenav')
     <!-- sidenav end -->
   </div>
   <!-- wrapper end -->
   @include('layouts.js.jquery')
   @include('layouts.js.script')
 </body>

 </html>