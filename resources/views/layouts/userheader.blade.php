@php
  if(Auth::check())
  {
      $signedinid = Auth::user()->name;
      $signedinname = Auth::user()->name;
      $signedinemail = Auth::user()->email;
      $signedinimage = Auth::user()->image;
  }else{
      $signedinname = "notsi";
      $signedinemail = "notsi";
      $signedinid = 0;
      $signedinimage = 0;
  }
@endphp
<div class="user-avatar" style="background-image:url('/assets/images/{{$link->profile_banner_image}}');">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-between px-2">
      <div class="col-12">
        <div class="avatar-wrap d-flex align-items-center flex-column flex-lg-row text-center text-lg-left">
          <figure class="mb-0 mr-4">
            <img src="/assets/images/{{$signedinimage}}" alt="" class="rounded-circle">
          </figure>
          <div class="avatar-details">
            <span>{{$signedinname}}</span>
            <p>Hi, Welcome to your user panel.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>