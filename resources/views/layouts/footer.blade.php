<!-- Back to top button -->
<a id="topbutton" class="d-flex align-items-center justify-content-center"></a>

<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
        <ul class="m-0 p-0">
          <li>
            <a href="#" title="About Us">About Us</a>
          </li>
          <li>
            <a href="#" title="Contact">Contact</a>
          </li>
          <li>
            <a href="#" title="Terms & Conditions">Terms & Conditions</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-2 col-md-6 mb-5 mb-lg-0">
        <ul class="m-0 p-0">
          <li>
            <a href="https://m.facebook.com/ATMOVFX/" title="Facebook" target="_blank"><span class="mr-2 fab fa-facebook-f"></span> Facebook</a>
          </li>
          <li>
            <a href="https://www.instagram.com/ATMOVFX/" title="Instagram" target="_blank"><span class="mr-2 fab fa-instagram"></span> Instagram</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
        <p>Subscribe to our newsletter</p>
        <form action="" method="" class="w-100">
          <div class="input-wrap">
            <input type="text" placeholder="Email Address" class="w-100 h-100 py-2 pl-3 border-0">
            <button class="d-flex justify-content-center align-items-center px-3 border-0">
              OK
            </button>
          </div>
        </form>
      </div>
      <div class="col-lg-3 col-md-6">
        <p class="m-0">Get in touch at:</p>
        <!--<a href="tel:+44345678903" title="+44 345 678 903" class="d-block">+44 345 678 903</a>-->
        <a href="mailto:contact@atmovfx.com" title="contact@atmovfx.com" class="d-block">contact@atmovfx.com</a>
      </div>
    </div>
  </div>
</footer>