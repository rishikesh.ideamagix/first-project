<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
    <link rel="stylesheet" href="/assets/vendor/select2/select2.min.css">

    <style type="text/css">
      .select2-container{
        width: 100% !important;
      }
    </style>
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" style="color: #6b6f80">Detailed Info</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <hr>
                    <div class="card-content collapse show p-2">
                        <div class="card-body card-dashboard row">
                            <img src="/images/{{$data->image}}" style="width: 100%" class="mb-2">
                            <div class="col-lg-6">
                              <form id="updateform" action="/admin/updateimgdet/{{$data->id}}" style="width: 100%" method="post">
                                @csrf
                                <label for="myselect"  class="text-white text-left  d-block" style="color: #6b6f80 !important"> Category : </label>
                                @php 
                                    $mpse = 'false'; $ga = 'false' ;$cp = 'false'; $pc = 'false'; $mo = 'false'; 
                                    if (strpos($data->category, 'matte-painting-set-extension') !== false) { $mpse = 'true'; } if(strpos($data->category, 'traditional-painting') !== false){ $cp = 'true'; } if(strpos($data->category, 'photography-composition') !== false){ $pc = 'true'; } if(strpos($data->category, 'movies') !== false){ $mo = 'true'; } if(strpos($data->category, 'games') !== false){ $ga = 'true'; } 
                                @endphp

                                <select class="js-example-basic-multiple" placeholder="Select Category"  name="category[]" multiple="multiple" style="width: 100%;background: transparent;">
                                  <option value="matte-painting-set-extension" @if($mpse=='true' ) selected @endif>Matte Painting Set Extension</option>
                                  <option value="traditional-painting" @if($cp=='true' ) selected @endif>Traditional Painting</option>
                                  <option value="photography-composition" @if($pc=='true' ) selected @endif>Photography Composition</option>
                                  <option value="movies" @if($mo=='true' ) selected @endif>Movies</option>
                                  <option value="games" @if($ga=='true' ) selected @endif>Games</option>
                                </select>
                                
                                <label for="myselect"  class="text-white text-left  d-block mt-1" style="color: #6b6f80 !important"> Movie/Show Name : </label>
                                <input type="text" id="artist" name="movie_name" placeholder="Enter Movie Name" class="w-100" value="{{$data->movie_name}}">

                                <label for="myselect"  class="text-white text-left  d-block mt-1" style="color: #6b6f80 !important"> Artist Name : </label>
                                <input type="text" id="artist" name="artist_name" placeholder="Enter Artist Name" class="w-100" value="{{$data->artist_name}}">

                                <label class="text-white text-left  d-block mt-1" style="color: #6b6f80 !important">Description : </label>
                                <textarea name="description" id="dis"  rows="3" class="adddiscription" placeholder="Add Description" style="width: 100%">{{$data->description}}</textarea>

                                <label for="taginput" class="text-white text-left  d-block mt-1" style="color: #6b6f80 !important"> Tags : </label>
                                <select class="js-example-basic-multiple" name="tags[]" multiple="multiple" style="color: #fff">
                                  @foreach($alltags as $tag)
                                    <option value="{{$tag->tag}}" @if(strpos($data->tags,$tag->tag) !== false) selected @endif >{{ucfirst($tag->tag)}}</option>
                                  @endforeach
                                </select>


                              </form>
                            </div>
                            <div class="col-lg-6">
                              <ul>
                                <li>Uploaders Details : <br>{{$data->uploaders_name}} <br> {{$data->uploaders_email}}</li>  
                                <li>Owners Details : <br>{{$data->owners_name}} <br> {{$data->owners_email}}</li>  
                                <li>Claimed : {{$data->claimed}}</li>
                                <li>Favourite Count : {{$data->favourite_count}}</li>
                                <li>Comment Count : {{$data->comment_count}}</li>
                                <li>
                                  Status: 
                                  <select class="statusselect" data-id="{{$data->id}}">
                                    <option value="approved" @if($data->status == 'approved') selected @endif>Approved</option>
                                    <option value="pending" @if($data->status == 'pending') selected @endif>Pending</option>
                                    <option value="rejected" @if($data->status == 'rejected') selected @endif>Rejected</option>
                                  </select>
                                </li>
                                <li>
                                  Delete: 
                                  <select class="deleteselect" data-id="{{$data->id}}">
                                    <option value="yes" @if($data->deleted == 'yes') selected @endif>Yes</option>
                                    <option value="no" @if($data->deleted == 'no') selected @endif>No</option>
                                  </select>
                                </li>
                              </ul>  
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-6">
                            <button form="updateform" type="submit" class="btn btn-primary w-100" style="">Update</button>
                          </div>
                          <div class="col-lg-6 row">
                            <a   class="btn btn-danger w-100 delbtn" onclick="delpic()" style="color:#fff;">Delete Permanently</a>
                            <div class="delconfirm row w-100">
                              <div class="col-lg-6 delconfirm">
                                <button class="btn btn-primary w-100" onclick="candelpic()" style="color:#fff;">Dont delete</button>
                              </div>
                              <div class="col-lg-6 delconfirm">
                                <a  href="/admin/deleteper/{{$data->id}}" class="btn btn-danger w-100" style="color:#fff;">Confirm Delete</a>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->
    @include('layouts.js.jquery')
    @include('layouts.js.select2')


    @include('admin.layouts.scripts')
    <script type="text/javascript">
        $(".delconfirm").addClass('d-none');
        
        function delpic() {
          $(".delconfirm").removeClass('d-none');
          $(".delbtn").addClass('d-none');
        }

        function candelpic() {
          $(".delconfirm").addClass('d-none');
          $(".delbtn").removeClass('d-none');
        }


        $('.statusselect').on('change', function() {
          var status = this.value;
          var id = $(this).attr('data-id');
          var link = '/admin/changestatus/'+id+'/'+status;
          console.log(link);
          $.ajax({
              url: link,
              type: "post",
          });
        });


        $('.deleteselect').on('change', function() {
          var deletestatus = this.value;
          var id = $(this).attr('data-id');
          var link = '/admin/changedeletestatus/'+id+'/'+deletestatus;
          console.log(link);
          $.ajax({
                  url: link,
                  type: "post",
              });
        });


        $('.deleteconfirm').removeClass('d-flex');
        $('.deleteconfirm').addClass('d-none');
    
        $(".delfirst").click(function(){
          $(this).siblings('.deleteconfirm').removeClass('d-none');
          $(this).siblings('.deleteconfirm').addClass('d-flex');
        });
        
        
        $(".canceldel").click(function(){
          $(this).parent().addClass('d-none');
          $(this).parent().removeClass('d-flex');
        });
    
    
    
      $('.statusselect').on('change', function() {
        var status = this.value;
        var id = $(this).attr('data-id');
        var link = '/admin/changeuserstatus/'+id+'/'+status;
        console.log(link);
        $.ajax({
                url: link,
                type: "post",
            });
      });
    </script>

  </body>
  <!-- END: Body-->
</html>