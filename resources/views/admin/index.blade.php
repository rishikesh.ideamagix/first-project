<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-3">
              <div class="card pull-up bg-gradient-directional-danger">
                <div class="card-header bg-hexagons-danger">
                  <h4 class="card-title white">Images</h4>
                  <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                  </a>
                </div>
                <div class="card-content collapse show bg-hexagons-danger">
                  <div class="card-body">
                    <div class="media d-flex">
                      <!-- <div class="align-self-center width-100">
                        <div id="Analytics-donut-chart" class="height-100 donutShadow"></div>
                      </div> -->
                      <div class="media-body text-right mt-1">
                        <h3 class="font-large-2 white">{{$imagecount}}</h3>
                        <h6 class="mt-1"><span class="text-muted white">Total Images</span></h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card pull-up bg-gradient-directional-danger">
                <div class="card-header bg-hexagons-danger">
                  <h4 class="card-title white">Claim Requests</h4>
                  <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                  </a>
                </div>
                <div class="card-content collapse show bg-hexagons-danger">
                  <div class="card-body">
                    <div class="media d-flex">
                      <!-- <div class="align-self-center width-100">
                        <div id="Analytics-donut-chart" class="height-100 donutShadow"></div>
                      </div> -->
                      <div class="media-body text-right mt-1">
                        <h3 class="font-large-2 white">{{$pendingimagedatacount}}</h3>
                        <h6 class="mt-1"><span class="text-muted white">Total Pending Images</span></h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card pull-up bg-gradient-directional-danger">
                <div class="card-header bg-hexagons-danger">
                  <h4 class="card-title white">Edit Requests</h4>
                  <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                  </a>
                </div>
                <div class="card-content collapse show bg-hexagons-danger">
                  <div class="card-body">
                    <div class="media d-flex">
                      <!-- <div class="align-self-center width-100">
                        <div id="Analytics-donut-chart" class="height-100 donutShadow"></div>
                      </div> -->
                      <div class="media-body text-right mt-1">
                        <h3 class="font-large-2 white">{{$editcount}}</h3>
                        <h6 class="mt-1"><span class="text-muted white">Total Edit Requests</span></h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-3">
              <div class="card pull-up bg-gradient-directional-danger">
                <div class="card-header bg-hexagons-danger">
                  <h4 class="card-title white">Claim Requests</h4>
                  <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                  </a>
                </div>
                <div class="card-content collapse show bg-hexagons-danger">
                  <div class="card-body">
                    <div class="media d-flex">
                      <!-- <div class="align-self-center width-100">
                        <div id="Analytics-donut-chart" class="height-100 donutShadow"></div>
                      </div> -->
                      <div class="media-body text-right mt-1">
                        <h3 class="font-large-2 white">{{$claimcount}}</h3>
                        <h6 class="mt-1"><span class="text-muted white">Total Claim Requests</span></h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
          <hr style="border-color: #5CD9D3">
          <div class="mb-2">
            <h3>Search Bar background banner</h3>
            <form action="/admin/updatesearchbarbanner" method="POST" enctype="multipart/form-data">
              @csrf
              <label style="color: white">Image</label>
              <br>
              <input type="file" name="image" accept='image/*' style="width: 100%">
              <label style="color: white">Old Image : </label>
              <img src="/assets/images/{{$ads->search_banner_image}}" style="max-height:200px"><br>
              <button class="btn btn-success mt-2" style="background-color: #5CD9D3">Update</button>
            </form>
          </div>
          
          
          <hr style="border-color: #5CD9D3">
          <div class="mb-2">
            <h3>Profile banner</h3>
            <form action="/admin/updateprofilebanner" method="POST" enctype="multipart/form-data">
              @csrf
              <label style="color: white">Image</label>
              <br>
              <input type="file" name="image" accept='image/*' style="width: 100%">
              <label style="color: white">Old Image : </label>
              <img src="/assets/images/{{$ads->profile_banner_image}}" style="max-height:200px"><br>
              <button class="btn btn-success mt-2" style="background-color: #5CD9D3">Update</button>
            </form>
          </div>

          <hr style="border-color: #5CD9D3">
          <div class="mb-2">
            <h3>Ad Banner</h3>
            <form action="/admin/updatead" method="POST" enctype="multipart/form-data">
              @csrf
              <label style="color: white">Type</label>
              <br>
              <label class="radio-inline mr-1" style="color: #fff">
                <input type="radio" name="adtype"  value="link" @if($ads->type == 'link') checked @endif> Ad Link
              </label>
              <label class="radio-inline" style="color: #fff">
                <input type="radio" name="adtype" value="banner" @if($ads->type == 'banner') checked @endif> Banner
              </label>
              <br>

              <label style="color: white">Google Ad's Link</label>
              <input type="text" name="add" value="{{$ads->link}}" style="width: 100%">

              <label style="color: white" class="mt-2">Banner Image</label>
              <input type="file" name="image" accept='image/*' style="width: 100%">
              <label style="color: white">Old Image : </label>
              <img src="/assets/images/{{$ads->image}}" style="max-width: 100%"><br>
              <button class="btn btn-success mt-2" style="background-color: #5CD9D3">Update</button>
            </form>
          </div>
          <hr style="border-color: #5CD9D3">
          </div>
          <div>
            <h3>Send mail to</h3>
            <form action="/admin/allusermail" method="post">
              @csrf
              <select class="form-control mb-2" name="mailto">
                <option value="all">All users</option>
                <option value="senior">Seniors only</option>
                <option value="junior">Juniors only</option>
              </select>
              <textarea class="form-control" placeholder="Enter Content" name="content" style="width: 100%" rows="8" required=""></textarea>
              <button class="btn btn-success mt-2" style="background-color: #5CD9D3">Send Mail</button>
            </form>
          </div>
          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12">
                <h5 class="card-title text-bold-700 my-2">Recently Joined Users</h5>
                <div class="card">            
                    <div class="card-content">
                        <div id="recent-projects" class="media-list position-relative">
                            <div class="table-responsive">
                                <table class="table table-padded table-xl mb-0" id="recent-project-table">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0">Name</th>
                                            <th class="border-top-0">Email</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($users as $userdata)
                                        <tr>
                                          <td>{{$userdata->name}}</td>
                                          <td>{{$userdata->email}}</td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-12">
                <h5 class="card-title text-bold-700 my-2">Recently Uploaded Images</h5>
                <div class="card">            
                    <div class="card-content">
                        <div id="recent-projects" class="media-list position-relative">
                            <div class="table-responsive">
                                <table class="table table-padded table-xl mb-0" id="recent-project-table">
                                    <thead>
                                        <tr>
                                            <th class="border-top-0">Image</th>
                                            <th class="border-top-0">Artist's Name</th>
                                            <th class="border-top-0">Uploader's details</th>
                                            <th class="border-top-0">CLaimed</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($images as $imagedata)
                                        <tr>
                                          <td>
                                            <a href="/image/{{$imagedata->id}}">
                                              <img src="/images/{{$imagedata->image}}" style="width:120px;">
                                            </a>
                                          </td>
                                          <td>
                                            @if($imagedata->artist_name == '')
                                              Unknown
                                            @else
                                              {{ucfirst($imagedata->artist_name)}}
                                            @endif
                                          </td>
                                          <td>{{$imagedata->uploaders_name}} <br> {{$imagedata->uploaders_email}}</td>
                                          <td>{{$imagedata->claimed}}</td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->


    @include('admin.layouts.scripts')

  </body>
  <!-- END: Body-->
</html>