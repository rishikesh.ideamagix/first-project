<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" style="color: #6b6f80">Tag's</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="px-3">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <ul style="margin-bottom:0;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form action="/admin/add/tag" method="post">
                          @csrf
                          <label >Add new tag</label>
                          <input type="text" name="tag" value="" placeholder="Enter Tag Name" style="width: 100%" required>
                          <button class="btn btn-success mt-2" style="background-color: #5CD9D3">Add</button>
                        </form>
                    </div>
                    <hr>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <!-- <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p> -->
                            <div class="table-responsive">
                                <!-- <table class="custom-configuration table display nowrap table-striped table-bordered"> -->
                                <table class="table table-striped table-bordered dom-jQuery-events">
                                    <thead>
                                      <tr>
                                          <th>Id</th>
                                          <th>Tag</th>
                                          <th>Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($data as $key=>$datanew)
                                      @php $key = $key+1; @endphp
                                        <tr>
                                          <td>{{$key}}</td>
                                          <td>{{$datanew->tag}}</td>
                                          <td>
                                            <button  class="btn btn-danger delfirst">Delete Tag</button>
                                            <div class="mt-1 d-flex deleteconfirm">
                                                <button class="btn btn-primary mr-1 canceldel">Cancel</button>
                                                <a href="/admin/deletetag/{{$datanew->id}}" class="btn btn-danger">Confirm and Delete</a>
                                            </div>
                                          </td>
                                        </tr>
                                      @endforeach
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->


    @include('admin.layouts.scripts')
    
    <script type="text/javascript">
        $('.deleteconfirm').removeClass('d-flex');
        $('.deleteconfirm').addClass('d-none');
    
        $(".delfirst").click(function(){
          $(this).siblings('.deleteconfirm').removeClass('d-none');
          $(this).siblings('.deleteconfirm').addClass('d-flex');
        });
        
        
        $(".canceldel").click(function(){
          $(this).parent().addClass('d-none');
          $(this).parent().removeClass('d-flex');
        });
    
    
    
      $('.statusselect').on('change', function() {
        var status = this.value;
        var id = $(this).attr('data-id');
        var link = '/admin/changeuserstatus/'+id+'/'+status;
        console.log(link);
        $.ajax({
                url: link,
                type: "post",
            });
      });
    </script>

  </body>
  <!-- END: Body-->
</html>