<footer class="footer footer-static footer-light navbar-border navbar-shadow">
  <div class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
    <span class="float-md-left d-block d-md-inline-block">Designed and Developed by
      <a class="text-bold-800 grey darken-2" href="https://www.ideamagix.com/" target="_blank">Ideamagix</a>
    </span>
  </div>
</footer>