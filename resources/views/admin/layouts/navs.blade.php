<div class="page-loader">
    <img src="/atmo.gif" alt="ATMO">
</div>

<nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="collapse navbar-collapse show" id="navbar-mobile">
                <ul class="nav navbar-nav mr-auto float-left">
                    <li class="nav-item mobile-menu d-md-none mr-auto">
                      <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                        <i class="ft-menu font-large-1"></i>
                      </a>
                    </li>
                    <!-- <li class="nav-item d-none d-md-block">
                      <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                        <i class="ft-menu"></i>
                      </a>
                    </li> -->
                    <!-- <li class="nav-item d-none d-md-block">
                      <a class="nav-link nav-link-expand" href="#">
                        <i class="ficon ft-maximize"></i>
                      </a>
                    </li> -->
                </ul>
                <ul class="nav navbar-nav float-right">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!-- END: Header-->

<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true" data-img="/adminassets/app-assets/images/backgrounds/02.jpg">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto">
        <a class="navbar-brand" href="/admin/dashboard">
          <!-- <img class="brand-logo" alt="Chameleon admin logo" src="/adminassets/app-assets/images/logo/logo.png"/> -->
          <h3 class="brand-text">ATMO Admin</h3>
        </a>
      </li>
      <li class="nav-item d-md-none">
        <a class="nav-link close-navbar">
          <i class="ft-x"></i>
        </a>
      </li>
    </ul>
  </div>
  <div class="navigation-background"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item">
        <a href="/admin/dashboard">
          <span class="menu-title" data-i18n="">Dashboard</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/tags">
          <span class="menu-title" data-i18n="">Tags</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/users">
          <span class="menu-title" data-i18n="">Users</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/images">
          <span class="menu-title" data-i18n="">Images for tags</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/imagestwo">
          <span class="menu-title" data-i18n="">Images</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/deletedimages">
          <span class="menu-title" data-i18n="">Deleted Images</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/editrequests">
          <span class="menu-title" data-i18n="">Edit Requests</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/claimrequests">
          <span class="menu-title" data-i18n="">Claim Requests</span>
        </a>
      </li>
      <li class=" nav-item">
        <a href="/admin/surveydetails">
          <span class="menu-title" data-i18n="">Survey Details</span>
        </a>
      </li>
      <!-- <li class=" nav-item">
        <a href="/admin/products">
          <span class="menu-title" data-i18n="">Products</span>
        </a>
        <ul class="menu-content">
          <li>
            <a class="menu-item" href="/admin/products">All Products</a>
          </li>
          <li class="">
            <a class="menu-item" href="/admin/addproduct">Add Products</a>
          </li>
          <li class="">
            <a class="menu-item" href="/admin/addbulkproducts">Upload Bulk Data</a>
          </li>
        </ul>
      </li> -->
    </ul>
  </div>
</div>
<!-- END: Main Menu