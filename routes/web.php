<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });



// <!-- @foreach($data as $datanew)
//   <div class="box" data-aos="fade-up">
//     <a href="/image/{{$datanew->id}}" title="" class="d-block">
//       <img src="/images/{{$datanew->image}}" alt="">
//       <div class="home-page-profile-container">
//         <div class="user-detail d-flex align-items-center justify-content-between">
//           <div class="d-flex align-items-center" style="flex-basis: 74%;">
//             <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
//             @if($datanew->artist_name != '')
//               <p class="mb-0">{{$datanew->artist_name}}</p>
//             @elseif($datanew->movie_name != '')
//               <p class="mb-0">{{$datanew->movie_name}}</p>
//             @else
//               <p class="mb-0">Unknown</p>
//             @endif
//           </div>
//           <div class="" style="flex-basis: 70px;">
//             <div class="likes-wrap d-flex justify-content-between">
//               <div class="counts-container d-flex align-items-center mr-2">
//                 <i class="fas fa-share"></i>
//               </div>
//               <div class="counts-container d-flex align-items-center">
//                 <i class="far fa-comment"></i>
//                 <span class="count">999</span>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     </a>
//   </div>
// @endforeach -->


// Auth::routes();
Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/home', 'ImageController@index')->name('images');
// Route::get('/', 'ImageController@index')->name('images');

Route::get('/home', function () {
    return view('comingsoonpage');
});

Route::get('/', function () {
    return view('comingsoonpage');
});


Route::get('/beta', 'ImageController@index')->name('images');
Route::get('/signupform', function () {
    return view('userpanel.signupform');
});
Route::get('/loginform', function () {
    return view('userpanel.loginform');
});

Route::get('/images/{imagecategory}', 'ImageController@page');
Route::get('/images/community/{imagecategory}', 'ImageController@communitypage');
Route::any('/search', 'ImageController@search');
Route::any('/jmsearch', 'ImageController@jmsearch');


Route::resource('image','ImageController');
Route::resource('claim','ClaimController');

Route::get('/uploadimage', 'ImageController@uploadpage')->name('imageupload')->middleware('verified');
Route::get('/myuploads', 'ImageController@useruploads')->name('useruploads')->middleware('verified');
Route::get('/mywishlist', 'ImageController@userwishlist')->name('userwishlist')->middleware('verified');
Route::get('/pendingapproval', 'ImageController@pendingapproval')->name('pendingapproval')->middleware('verified');
Route::get('/mydetail', 'ImageController@userdetail')->name('userdetail')->middleware('verified');
// Route::get('/changemypassword/{token}', 'ImageController@changeuserpassword')->name('changeuserpassword')->middleware('verified');
Route::get('/changemypassword', 'ImageController@changeuserpassword')->name('changeuserpassword')->middleware('verified');
Route::any('/user/credentials', 'ImageController@updatepassword');
Route::get('/myedits', 'ImageController@useredits')->name('useredits')->middleware('verified');
Route::get('/myclaims', 'ImageController@userclaims')->name('userclaims')->middleware('verified');
Route::get('/reference-library', 'ImageController@referencegallery')->name('referencegallery')->middleware('verified');
Route::any('/addtorefgal/{imgid}/{galid}', 'ImageController@addtorefgal')->name('addtorefgal')->middleware('verified');

Route::any('/detailedgallery/{galid}', 'ImageController@detailedgallery')->name('detailedgallery')->middleware('verified');

Route::any('/addnewrefgall', 'ImageController@addnewrefgall')->name('addnewrefgall')->middleware('verified');
Route::any('/editnewrefgall', 'ImageController@editnewrefgall')->name('editnewrefgall')->middleware('verified');
Route::any('/deletegal', 'ImageController@deletegal')->name('deletegal')->middleware('verified');


Route::get('/deleteimage/{id}', 'ImageController@deleteimage')->name('deleteimage')->middleware('verified');
Route::get('/deletedimages', 'ImageController@deletedimages')->name('deletedimages')->middleware('verified');


Route::any('/getalldata','ImageController@getalldata');
Route::any('/getimagedata','ImageController@getimagedata');


Route::any('/updateuser','HomeController@updateuser');


Route::resource('comment','CommentController');



Route::post('like/{fromlike}/{tolike}', 'FavouriteController@like');
Route::post('unlike/{fromlike}/{tolike}', 'FavouriteController@unlike');


Route::get('/markupcomment/{id}', 'ImageController@markupcomment');
Route::post('/savemarkup', 'ImageController@savemarkup');
Route::post('/savemarkupscreenshot', 'ImageController@savemarkupscreenshot');


Route::get('/comingsoon', function () {
    return view('comingsoon');
});

// Route::get('/survey', function () {
//     return view('surveyform');
// });


Route::any('/survey', 'ImageController@surveys');
Route::any('/add/survey', 'ImageController@addsurvey');


Route::get('/allmarkups/{id}', 'ImageController@allmarkups');

Route::get('/images/tag/{tag}', 'ImageController@tagimages');
Route::get('/images/title/{title}', 'ImageController@titleimages');
Route::get('/images/artist/{artist}', 'ImageController@artistimages');

Route::any('/senior/image/approval/{image}/{artist}', 'ImageController@seniorimageapproval')->middleware('verified');

Route::post('/autocomplete/fetch', 'ImageController@fetch')->name('autocomplete.fetch');
Route::post('/autocomplete/jmfetch', 'ImageController@jmfetch')->name('jmautocomplete.fetch');


Route::get('/community', 'ImageController@community')->name('community');



Route::any('{catchall}', 'ImageController@notfound')->where('catchall', '.*');
