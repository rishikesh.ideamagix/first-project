<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Edit;
use App\Claim;
use App\Favourite;
use Validator;
use Hash;
use Redirect;
use App\User;
use App\Survey;
use App\Comment;
use Imagei;
use Storage;
use File;
use App\tag;
use App\ReferenceGallery;
use App\Advertise;
use Mail;


class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }

        // $data = Image::inRandomOrder()->where([['status','approved'],['deleted','no'],['show_on_home_page','1']])->orderby('id','desc')->paginate(100);
        $data = Image::inRandomOrder()->where([['status','approved'],['deleted','no']])->orderby('id','desc')->paginate(100);
        $link = Advertise::find(1);
        $datatype = 'all';
        $users = User::all();
        $favourites = Favourite::where('user_id',$signedinid);
        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();
        return view('index',compact('data','datatype','link','users','favourites','refgalleries'));
    }
    
    
    
    

    public function page($imagecategory)
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }

        // $data = Image::inRandomOrder()->where('category','like','%'.$imagecategory.'%')->where([['status','approved'],['deleted','no'],['show_on_home_page','1']])->inRandomOrder()->paginate(100);
        $data = Image::inRandomOrder()->where('category','like','%'.$imagecategory.'%')->where([['status','approved'],['deleted','no']])->inRandomOrder()->paginate(100);
        $datatype = $imagecategory;
        $link = Advertise::find(1);
        $users = User::all();
        $favourites = Favourite::where('user_id',$signedinid);
        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();
        return view('index',compact('data','datatype','link','users','favourites','refgalleries'));
    }
    
    public function communitypage($imagecategory)
    {
        // return 1;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $artists = User::where('artist_type','!=','senior')->get();
        $artistids = $artists->pluck('id')->toArray();
        // return $artistids;
        $query = DB::table('images');
        foreach($artistids as $idnew){
            $query->orWhere([['status','approved'],['deleted','no'],['user_id',$idnew],['category','like','%'.$imagecategory.'%']]);
            
        }
        
        // $data = $query->orderby('id','desc')->paginate(100);
        $data = $query->inRandomOrder()->paginate(100);
        
        
        // $data = Image::inRandomOrder()->where('category','like','%'.$imagecategory.'%')->where([['status','approved'],['deleted','no'],['show_on_home_page','1']])->inRandomOrder()->paginate(100);
        // $data = Image::inRandomOrder()->where('category','like','%'.$imagecategory.'%')->where([['status','approved'],['deleted','no'],])->inRandomOrder()->paginate(100);
        $datatype = $imagecategory;
        $link = Advertise::find(1);
        $users = User::all();
        $favourites = Favourite::where('user_id',$signedinid);
        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();
        return view('community',compact('data','datatype','link','users','favourites','refgalleries'));
    }


    public function community(){
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }

        // $data = Image::inRandomOrder()->where([['status','approved'],['deleted','no'],['show_on_home_page','1']])->orderby('id','desc')->paginate(100);
        
        $artists = User::where('artist_type','!=','senior')->get();
        $artistids = $artists->pluck('id')->toArray();
        
        $query = DB::table('images');
        foreach($artistids as $idnew){
            $query->orWhere([['status','approved'],['deleted','no'],['user_id',$idnew]]);
            
        }
        
        $data = $query->orderby('id','desc')->paginate(100);
        $link = Advertise::find(1);
        $datatype = 'all';
        $users = User::all();
        $favourites = Favourite::where('user_id',$signedinid);
        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();
        return view('community',compact('data','datatype','link','users','favourites','refgalleries'));    
    }


    public function surveys(){
        $surveys = Survey::paginate('20');
        return view('surveyform',compact('surveys'));
    }
    
    
    
    public function addsurvey(Request $request)
    {
        // return $request;
        $data = New Survey();
        $data->artist_type = $request->artisttype;
        $data->artist_level = $request->artistlevel;
        $data->artist_salary = $request->salary.'/'.$request->salper;
        $data->save();
        return redirect('/survey');
        return back();
    }

    public function useruploads()
    {   
        // return 1;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }

        $data = Image::where([['user_id',$signedinid],['deleted','no']])->orderby('id','desc')->get();
        // return $data;
        $datatype = 'useruploads';
        $link = Advertise::find(1);
        return view('userpanel.myuploads',compact('data','datatype','link'));
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;


        if(Auth::check())
        {
            $signedinid = Auth::user()->id;
            $signedinname = Auth::user()->name;
            $signedinemail = Auth::user()->email;
            $signedinimage = Auth::user()->image;
            
            $signedinartisttype = Auth::user()->artist_type;
            $signedinartisttypeverified = Auth::user()->artist_type_verified;
        }else{
            $signedinname = "notsi";
            $signedinemail = "notsi";
            $signedinimage = "notsi";
            $signedinid = 0;
            $signedinartisttype = "notsi";
            $signedinartisttypeverified = "notsi";
        }

        if ($request->has('admin')) {
            $image = new Image();
            $image->artist_name = $request->artist_name;
            $image->uploaders_name = "admin";
            $image->uploaders_email = "admin";
            $image->owners_email = "admin";

            if($request->movie_name != null){
                $image->movie_name = $request->movie_name;
            }
            if($request->category != null){
                $catstring = implode(',', $request->category);
                // return $catstring;
                $image->category = $catstring;
            }
            if($request->description != null){
                $image->description = $request->description;
            }
            if($request->tags != null){
                $stringtag = implode(',', $request->tags);
                $image->tags = $stringtag;
            }

            if($request->hasfile('image'))
            {
               $file = $request->file('image');
               $filesize = round($file->getSize()/1024);
               $data = getimagesize($file);
               $width = $data[0]/3;
               $height = $data[1]/3;

               $extension = $file->getClientOriginalExtension(); // getting image extension
               $filename ='image.'.time().'.'.$extension;
               $image_resize = Imagei::make($file->getRealPath());
               $file->move('images', $filename);
               $image_resize->resize(500, null, function ($constraint) {
                   $constraint->aspectRatio();
               });
               $image_resize->save('images/compressed/'.$filename,100);
               $image->image =$filename;
            }
            $image->owners_name = "admin";

            $image->status = "approved";
            $image->user_id = 0;
            $image->save();
            return back();
        }
        // return $request;


        $image = new Image();
        
        $image->uploaders_name = $signedinname;
        $image->uploaders_email = $signedinemail;
        $image->uploaders_id =$signedinid;
        
        $self = 'no';
        if($request->artistnamedropdown == $signedinid && $signedinartisttype !='senior'){
            $self = 'yes';
        }else{
            $self = 'no';
        }
        
        if($request->artistnamedropdown == 'unknown'){
            $image->artist_name = 'Unknown';
            $image->claimed = 'no';
            $image->owners_name = $signedinname;
            $image->owners_email = $signedinemail;
            $image->user_id =$signedinid;
        }elseif($request->artistnamedropdown == 'other'){
            $image->artist_name = $request->artist_name;
            $image->claimed = 'no';
            $image->owners_name = $signedinname;
            $image->owners_email = $signedinemail;
            $image->user_id =$signedinid;
        }else{
            $dropdownuserdata = User::find($request->artistnamedropdown);
            $image->artist_name = $dropdownuserdata->name;
            $image->claimed = 'yes';
            $image->claimed_by = $dropdownuserdata->name;
            $image->owners_name = $dropdownuserdata->name;
            $image->owners_email = $dropdownuserdata->email;
            $image->profile_image = $dropdownuserdata->image;
            $image->user_id =$request->artistnamedropdown;
        }



        // if ($request->has('i_am_artist')) {
        //     $image->claimed = 'yes';
        //     $image->claimed_by = $request->artist_name;
        //     $image->owners_name = $request->artist_name;
        //     $image->profile_image = $signedinimage;
        //     $image->user_id =$signedinid;
        // }else{
        //     $image->claimed = 'no';
        //     $image->owners_name = $signedinname;
        //     $image->user_id =$signedinid;
        // }

        if($request->movie_name != null){
            $image->movie_name = $request->movie_name;
        }
        if($request->category != null){
            $catstring = implode(',', $request->category);
            // return $catstring;
            $image->category = $catstring;
        }
        if($request->description != null){
            $image->description = $request->description;
        }
        if($request->tags != null){
            $stringtag = implode(',', $request->tags);
            $image->tags = $stringtag;
        }
       
        if($request->hasfile('image'))
        {
            
            $file = $request->file('image');
            $filesize = round($file->getSize()/1024);
            // return $filesize;
            // return 1;
            $data = getimagesize($file);
            $width = $data[0]/3;
            $height = $data[1]/3;
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename ='image.'.time().'.'.$extension;
            $image_resize = Imagei::make($file->getRealPath());
            $file->move('images', $filename);
            // $image_resize = Imagei::make($request->file('image')->getRealPath());
            // $image_resize = Imagei::make($file->getRealPath());
            // $image_resize->resize($width,$height);
            $image_resize->resize(500, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image_resize->save('images/compressed/'.$filename,100);
            // $image_resize->save('images/compressed/'.$filename);
            $image->image =$filename;


            // return "saved";
        }

        if ($signedinemail == 'moderator@atmo.com') {
            $image->status = "approved";
        }elseif($signedinartisttype == 'senior' && $signedinartisttypeverified == 'yes'){
            $image->status = "approved";
        }elseif($self == 'yes'){
            $image->status = "approved";
        }
        // return $image;
        $image->save();
        
        
        
        $emails = ['akil.ideamagix@gmail.com','support@atmovfx.com','contact@atmovfx.com'];
        $data1 = array('content'=>"abc");
        Mail::send('mails.newupload',$data1, function($message) use($emails){
        $message->to($emails, 'ATMO-VFX')->subject
        ('New Image | ATMO-VFX');
        $message->from('contact@atmovfx.com','ATMO');
        });


        $emailstwo = User::where('artist_type','senior')->pluck('email')->toArray();
        
        
        // return back();
        return redirect('/myuploads');
        
    }

    

    public function search(Request $request)
    {
        $search = $request->search_field;
        $users = User::all();

        $terms = explode(",",$search);
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        
        $favourites = Favourite::where('user_id',$signedinid);
        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();

        $search_fields = ['artist_name', 'description', 'tags', 'movie_name'];
        $search_terms = explode(',',$search);
        $query = Image::query();
        foreach ($search_terms as $term) {
            if($term != ''){
                $query->orWhere(function ($query) use ($search_fields, $term) {
                    foreach ($search_fields as $field) {
                        $query->orWhere($field, 'LIKE', '%' . $term . '%');
                    }
                });
            }
        }
        $data = $query->inRandomOrder()->paginate(100);
        $search_terms = implode(",",$search_terms);
        $data->appends(['search_field' => $search_terms]);
        $link = Advertise::find(1);
        $datatype = 'search';
        return view('index',compact('data','datatype','search','link','users','refgalleries','favourites'));
    }
    
    public function jmsearch(Request $request)
    {
        $search = $request->search_field;
        $users = User::all();

        $terms = explode(",",$search);
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        
        $favourites = Favourite::where('user_id',$signedinid);
        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();

        $search_fields = ['artist_name', 'description', 'tags', 'movie_name'];
        $search_terms = explode(',',$search);
        $query = Image::query();
        foreach ($search_terms as $term) {
            if($term != ''){
                $query->orWhere(function ($query) use ($search_fields, $term) {
                    foreach ($search_fields as $field) {
                        $query->orWhere($field, 'LIKE', '%' . $term . '%');
                    }
                });
            }
        }
        $data = $query->inRandomOrder()->paginate(100);
        
        $artists = User::where('artist_type','!=','senior')->get();
        $artistids = $artists->pluck('id')->toArray();
        
        // return $artistids;
        // return $data;
        
        foreach($data as $key=>$datanew){
            $matched = '0';
            foreach($artistids as $idnew){
                if($datanew->user_id == $idnew){
                    $matched = '1';
                    // return "mtched";
                }
            }
            if($matched == '0'){
                unset($data[$key]);
                // return "nahi match hua re baba";
            }else{
                // return "match hua re baba";
            }
        }
        
        // return 1;
        $search_terms = implode(",",$search_terms);
        $data->appends(['search_field' => $search_terms]);
        $link = Advertise::find(1);
        $datatype = 'search';
        return view('community',compact('data','datatype','search','link','users','refgalleries','favourites'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        // return 1;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;
            $claimdata = Claim::where('claimers_id',$signedinid)->get();
            $likeddata = Favourite::where('user_id',$signedinid)->get();
            $signedinartisttype = Auth::user()->artist_type;
            $signedinartisttypeverified = Auth::user()->artist_type_verified;
            // return $claimdata;
        }else{
            $claimdata = '0';
            $likeddata = '0';
            $signedinartisttype = '0';
            $signedinartisttypeverified = '0';
            $signedinid = '0';
        }

        $refgalleries = ReferenceGallery::where('user_id',$signedinid)->orderby('name','asc')->get();
        if($image->status != 'approved' || $image->deleted == 'yes'){
            if($signedinartisttype == 'senior' && $signedinartisttypeverified == 'yes'){
                $comments = Comment::where('image_id',$image->id)->orderby('id','desc')->get();
            
                $userdetail = User::where('id',$image->user_id)->first();
        
                $datatype = "favourites";
                $alltags = tag::orderby('tag','asc')->get();
                // return $image;
                $users = User::all();
                return view('imagedetail',compact('refgalleries','image','claimdata','comments','likeddata','userdetail','datatype','alltags','users'));
            }else{
                return redirect('/');
            }
        }else{
            $comments = Comment::where('image_id',$image->id)->orderby('id','desc')->get();
            
            $userdetail = User::where('id',$image->user_id)->first();
    
            $datatype = "favourites";
            $alltags = tag::orderby('tag','asc')->get();
            // return $image;
            $users = User::all();
            return view('imagedetail',compact('refgalleries','image','claimdata','comments','likeddata','userdetail','datatype','alltags','users'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        if(Auth::check())
        {   
            $signedinname = Auth::user()->name;
            $signedinemail = Auth::user()->email;
            $signedinid = Auth::user()->id;
            
            $signedinartisttype = Auth::user()->artist_type;
            $signedinartisttypeverified = Auth::user()->artist_type_verified;
        }else{
            $signedinname = "notsi";
            $signedinemail = "notsi";
            $signedinid = "notsi";
        }

        $edit = new Edit();
        $edit->editors_id = $signedinid;
        $edit->editors_name = $signedinname;
        $edit->editors_email = $signedinemail;
        $edit->uploaders_name = $image->owners_name;
        $edit->uploaders_email = $image->owners_email;
        $edit->claimed = $image->claimed;
        $edit->image_id = $image->id;
        $edit->image = $image->image;
        $edit->old_artist_name = $image->artist_name;
        $edit->new_artist_name = $request->artist_name;
        $edit->old_movie_name = $image->movie_name;
        $edit->new_movie_name = $request->movie_name;
        $edit->old_category = $image->category;
        if($request->category != ''){
            $newcatstring = implode(',', $request->category);
        }else{
            $newcatstring = '';
        }
        $edit->new_category = $newcatstring;
        $edit->old_tags = $image->tags;
        if($request->tags){
            $stringtag = implode(',', $request->tags);
            $edit->new_tags = $stringtag;
        }else{
            $edit->new_tags = $request->tags;
        }

        // $edit->new_tags = $request->tags;
        $edit->old_description = $image->description;
        $edit->new_description = $request->description;
        $edit->status = 'pending';
        // return $edit;
        
        
        
        if($signedinartisttype == 'senior' && $signedinartisttypeverified == 'yes'){
            $edit->status = 'approved';
            
            $imageid = $edit->image_id;
            $imagedata = Image::find($imageid);
            // return $imagedata;
            $imagedata->artist_name = $edit->new_artist_name;
            $imagedata->movie_name = $edit->new_movie_name;
            $imagedata->category = $edit->new_category;
            $imagedata->tags = $edit->new_tags;
            $imagedata->description = $edit->new_description;
            $imagedata->save();
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        $edit->save();
        
        
        
        
        
        
        

        $emails = ['akil.ideamagix@gmail.com','support@atmovfx.com'];
        $data1 = array('content'=>"There's a new image edit request on atmovfx.com, please visit atmovfx.com/admin to check complete details");
        Mail::send('mails.editreq',$data1, function($message) use($emails){
        $message->to($emails, 'ATMO-VFX')->subject
        ('Edit Request');
        $message->from('support@atmovfx.com','ATMO');
        });



        return back();





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        //
    }

    public function uploadpage()
    {
        $tags = tag::orderby('tag','asc')->get();
        $users = User::where('id', '!=', auth()->id())->get();
        // return $tags;
        
        $link = Advertise::find(1);
        return view('userpanel.uploadimage',compact('tags','users','link'));
    }

    public function userwishlist()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Favourite::where('user_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'favourites';
        
        $link = Advertise::find(1);
        return view('userpanel.mywishlist',compact('data','datatype','link'));
    }

    public function pendingapproval()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Image::where('status','pending')->orderby('id','desc')->get();
        // return $data;
        $datatype = 'pendingapproval';
        
        $link = Advertise::find(1);
        
        return view('userpanel.pendingapproval',compact('data','datatype','link'));
    }


    public function seniorimageapproval($image, $artist)
    {
        $data = Image::find($image);
        $data->status = 'approved';
        $userid = $data->user_id;
        $data->save();

        $user = User::find($userid);
        $emails = ['akil.ideamagix@gmail.com','support@atmovfx.com','contact@atmovfx.com'];

        $data1 = array('content'=>"abc");
        Mail::send('mails.imageapproved',$data1, function($message) use($emails, $user){
        $message->to($user->email, 'ATMO-VFX')->subject
        ('Image Approved | ATMO-VFX');
        $message->from('contact@atmovfx.com','ATMO');
        });



        // return $data;
    }

    public function userdetail()
    {
        if(Auth::check())
        {
            $data = Auth::user();  
        }else{
            $data = 0;
        }
        $datatype = 'userdetails';
        
        $link = Advertise::find(1);
        
        return view('userpanel.userdetail',compact('data','datatype','link'));
    }

    public function changeuserpassword()
    {   
        // return view('auth.passwords.reset',compact('token'));
        $datatype = 'changepassword';
        
        $link = Advertise::find(1);
        return view('userpanel.changepassword',compact('datatype','link'));
    }

    public function admin_credential_rules(array $data)
    {
      $messages = [
        'current-password.required' => 'Please enter current password',
        'password.required' => 'Please enter password',
      ];

      $validator = Validator::make($data, [
        'current-password' => 'required',
        'password' => 'required|same:password',
        'password_confirmation' => 'required|same:password',     
      ], $messages);

      return $validator;
    }


    public function updatepassword(Request $request)
    {
        // return 112;
      if(Auth::Check())
      {
        $request_data = $request->All();
        $validator = $this->admin_credential_rules($request_data);
        if($validator->fails())
        {
          $error = response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
          return Redirect::back()->withErrors(['New Passwords Dont Match','New Passwords Dont Match']);
        }
        else
        {  
          $current_password = Auth::User()->password;           
          if(Hash::check($request_data['current-password'], $current_password))
          {           
            $user_id = Auth::User()->id;                       
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($request_data['password']);;
            $obj_user->save(); 
            return Redirect::back()->withErrors(['Password Changed', 'Password Changed']);
          }
          else
          {           
            return Redirect::back()->withErrors(['Please enter correct current password', 'Please enter correct current password']);
          }
        }        
      }
      else
      {
        return redirect()->to('/');
      }    
    }

    public function useredits()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Edit::where('editors_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'useredits';
        
        $link = Advertise::find(1);
        
        return view('userpanel.myedits',compact('data','datatype','link'));
    }

    public function userclaims()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = Claim::where('claimers_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'userclaims';
        
        $link = Advertise::find(1);
        return view('userpanel.myclaims',compact('data','datatype','link'));   
    }


    public function referencegallery()
    {
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        // return $signedinid;
        // return 1;
        $data = ReferenceGallery::where('user_id',$signedinid)->orderby('id','desc')->get();
        // return $data;
        $datatype = 'reference-gallery';

        $finalimglist= '';
        foreach ($data as $key => $value) {
            $finalimglist = $finalimglist.','.$value->images;
        };
        $finalimglistarr = explode(',', $finalimglist);
        $finalimglistarr = array_filter($finalimglistarr);
        $finalimglistarr = array_unique($finalimglistarr);

        $query = DB::table('images');
        foreach($finalimglistarr as $idnew){
           $query->orWhere('id', $idnew);
        }

        $images = $query->get();
        // return $images;


        // return $finalimglistarr;
        // return $key;
        
        $link = Advertise::find(1);
        return view('userpanel.referencegallery',compact('data','datatype','images','link'));   
    }

    public function addtorefgal($imgid, $galid)
    {
        // return 1;
        
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }

        
        // return $galid;




        // $datan = ReferenceGallery::where('user_id',$signedinid)->orderby('id','desc')->get();
        // $newgalid = '';
        // foreach ($datan as $key => $valuen) {
        //     $imgarr = explode(",",$valuen->images);
        //     if (in_array($imgid, $imgarr)) {
        //         $newgalid = $valuen->id;
        //     }
        // }

        // $datam = ReferenceGallery::find($newgalid);
        // $imgarrm = explode(",",$datam->images);

        // if (($key = array_search($imgid, $imgarrm)) !== false) {
        //     unset($imgarrm[$key]);
        // }
        // $imglist = implode(",",$imgarrm);
        // $datam->images = $imglist;
        // $datam->save();



        if ($galid == 'none') {
            // return 1;            

            $datan = ReferenceGallery::where('user_id',$signedinid)->orderby('id','desc')->get();
            $newgalid = '';
            foreach ($datan as $key => $valuen) {
                $imgarr = explode(",",$valuen->images);
                if (in_array($imgid, $imgarr)) {
                    $newgalid = $valuen->id;
                }
            }
            
            if ($newgalid != '') {
                $datam = ReferenceGallery::find($newgalid);
                $imgarrm = explode(",",$datam->images);

                if (($key = array_search($imgid, $imgarrm)) !== false) {
                    unset($imgarrm[$key]);
                }
                $imglist = implode(",",$imgarrm);
                $datam->images = $imglist;
                $datam->save();
            }
            


            
        }else{

            $datan = ReferenceGallery::where('user_id',$signedinid)->orderby('id','desc')->get();
            $newgalid = '';
            foreach ($datan as $key => $valuen) {
                $imgarr = explode(",",$valuen->images);
                if (in_array($imgid, $imgarr)) {
                    $newgalid = $valuen->id;
                }
            }
            
            if ($newgalid != '') {
                $datam = ReferenceGallery::find($newgalid);
                $imgarrm = explode(",",$datam->images);

                if (($key = array_search($imgid, $imgarrm)) !== false) {
                    unset($imgarrm[$key]);
                }
                $imglist = implode(",",$imgarrm);
                $datam->images = $imglist;
                $datam->save();
            }








            $data = ReferenceGallery::find($galid);
            if ($data->images == null) {
                $data->images = $imgid;
                $data->save();
            }else{
                $newimages = $data->images.','.$imgid;
                $data->images = $newimages;
                $data->save();
            }
        }
    }


    public function addnewrefgall(Request $request)
    {
        // return 1;
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = new ReferenceGallery();
        $data->name = ucfirst($request->name);
        $data->images = $request->imgid;
        $data->user_id = $signedinid;
        $data->save();
        return back();
    }



    public function editnewrefgall(Request $request)
    {
        
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = ReferenceGallery::find($request->id);
        $data->name = ucfirst($request->name);
        $data->save();
        return back();
    }

    public function deletegal(Request $request)
    {
        
        if(Auth::check())
        {
            $signedinid = Auth::user()->id;  
        }else{
            $signedinid = 0;
        }
        $data = ReferenceGallery::find($request->id);
        // return $request;
        $data->delete();
        return redirect('/reference-library');
        return back();
    }


    public function detailedgallery($galid)
    {
        // return $galid;
        $data = ReferenceGallery::find($galid);
        $imageslist = $data->images;

        $imagesarr = explode(",",$imageslist);

        $query = DB::table('images');
        foreach($imagesarr as $idnew){
           $query->orWhere('id', $idnew);
        }

        $images = $query->get();
        // return $images;
        $datatype = 'reference-gallery';
        $link = Advertise::find(1);
        return view('userpanel.detailedgallery',compact('datatype','data','images','link'));

    }



    public function deleteimage($id)
    {
        // return $id;
        $image = Image::find($id);
        $image->deleted = 'yes';
        $image->save();
        return back();
    }


    public function deletedimages()
    {
        $data = Image::where('deleted','yes')->orderby('id','desc')->get();
        $datatype = 'deletedimages';
        $link = Advertise::find(1);
        return view('userpanel.deletedimages',compact('data','datatype','link'));
    }


    public function markupcomment($id)
    {
        $image = Image::find($id);
        $link = Advertise::find(1);
        return view('userpanel.commentmarkup',compact('image','link'));
        return $data;
    }

    public function getalldata(Request $request)
    {
        // return $request->offset;
        $alldata = DB::table('images')->select('*')->where([['status', 'approved'],['deleted','no']])->take($request->limit)->skip($request->offset)->orderby('id','desc')->get();
        $res = json_decode($alldata, true);
        // return $res;

        


        foreach ($alldata as $alldatanew) {

            $id = $alldatanew->id;
            $image = $alldatanew->image;
            $artist_name = $alldatanew->artist_name;
            $movie_name = $alldatanew->movie_name;
            $dataone='

                <div class="box" data-aos="fade-up">
                  <a href="/image/'.$id.'" title="" class="d-block">
                    <img src="/images/'.$image.'" alt="">
                    <div class="home-page-profile-container">
                      <div class="user-detail d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center" style="flex-basis: 74%;">
                          <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                    ';
                          if($alldatanew->artist_name != ''){
                $datatwo = '

                    <p class="mb-0">'.$artist_name.'</p>

                ';
                }
                            
                          elseif($alldatanew->movie_name != ''){
                $datatwo = ' 

                    <p class="mb-0">'.$movie_name.'</p>

                ';

                }
                          else{

                $datatwo='
                            <p class="mb-0">Unknown</p>

                ';
                
                }           

                $datathree = '
                        </div>
                        <div class="" style="flex-basis: 70px;">
                          <div class="likes-wrap d-flex justify-content-between">
                            <div class="counts-container d-flex align-items-center mr-2">
                              <i class="fas fa-share"></i>
                            </div>
                            <div class="counts-container d-flex align-items-center">
                              <i class="far fa-comment"></i>
                              <span class="count">999</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>

            ';


            $finaldata = $dataone.$datatwo.$datathree;
            echo $finaldata;
        }
    }


    public function getimagedata(Request $request)
    {
        // return $request->offset;

        if ($request->type == 'all') {
            $alldata = DB::table('images')->select('*')->where([['status', 'approved'],['deleted','no']])->take($request->limit)->skip($request->offset)->orderby('id','desc')->get();
            $res = json_decode($alldata, true);    
        }else{
            $alldata = DB::table('images')->select('*')->where([['category','like','%'.$request->type.'%'],['status', 'approved'],['deleted','no']])->take($request->limit)->skip($request->offset)->orderby('id','desc')->get();
            $res = json_decode($alldata, true);   
        }
        
        
        // return $res;

        // if(Auth::check())
        // {
        //     $user = Auth::user(); 
        //     $userimg = Auth::user()->image; 
        // }else{
        //     $user = 0;
        // }

        foreach ($alldata as $alldatanew) {

            $id = $alldatanew->id;
            $image = $alldatanew->image;
            $artist_name = $alldatanew->artist_name;
            $movie_name = $alldatanew->movie_name;
            $favourite_count = $alldatanew->favourite_count;
            if ($alldatanew->claimed == 'yes') {
                $userid = $alldatanew->user_id;
                $userdata = User::find($userid);
                $user_image = $userdata->image;
                // $user_image = $userimg;
            }else{
                $user_image ='1.jpg';
            }
            // return $user_image;
            $dataone='

                <div class="box" data-aos="fade-up">
                  <a href="/image/'.$id.'" title="" class="d-block">
                    <img src="/images/compressed/'.$image.'" alt="">
                    <div class="home-page-profile-container">
                      <div class="user-detail d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center" style="flex-basis: 74%;">
                          <img src="/assets/images/'.$user_image.'" alt="" class="rounded-circle">
                    ';
                          if($alldatanew->artist_name != ''){
                $datatwo = '

                    <p class="mb-0">'.$artist_name.'</p>

                ';
                }
                            
                          elseif($alldatanew->movie_name != ''){
                $datatwo = ' 

                    <p class="mb-0">'.$movie_name.'</p>

                ';

                }
                          else{

                $datatwo='
                            <p class="mb-0">Unknown</p>

                ';
                
                }           

                $datathree = '
                        </div>
                        <div class="" style="flex-basis: 70px;">
                          <div class="likes-wrap d-flex justify-content-between">
                            <div class="counts-container d-flex align-items-center mr-2">
                              <i class="fas fa-share"></i>
                            </div>
                            <div class="counts-container d-flex align-items-center">
                              <i class="far fa-comment"></i>
                              <span class="count">'.$favourite_count.'</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>

            ';


            $finaldata = $dataone.$datatwo.$datathree;
            echo $finaldata;
        }
    }




    public function savemarkup(Request $request)
    {
        $img = $request['imgBase64'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $filename ='/images/markup.'.time().'.png';
        // $success = file_put_contents($filename, $data);
        // File::put($filename,$data);
        Storage::disk('markup_uploads')->put($filename,$data);
        return "success";
        
    }


    public function notfound()
    {
        // return 1;
        return view('errors.404');
    }
    
    
    
    
    
    
    public function savemarkupscreenshot(Request $request)
    {
        // return 1;
        $img = $request['imgBase64'];
        // $img = str_replace('data:image/png;base64,', '', $img);
        // $img = str_replace(' ', '+', $img);
        // $data = base64_decode($img);
        // $filename ='image.'.time().'.jpeg';
        // return $data;
        $width = "500"; 
        $height = "500"; 
        $name = time().'.'.explode('/',explode(':',substr($img,0,strpos($img,';')))[1])[1];
        // Imagei::make($img)->resize($width,$height)->save(public_path('images/test/').$name);
        $newimg = Imagei::make($img);
        // $newimg->resize($width,$height);
        $newimg->save(public_path('images/test/').$name,70);
        // $image_resize = Imagei::make($data);
        // $image_resize->resize($width,$height);
        // return $data;
        // $image_resize->save('/images/compressed/'.$filename,100);

        // $success = file_put_contents($filename, $data);
        // File::put($filename,$data);
        // Storage::disk('markup_uploads')->put($filename,$data);
        return $name;



        // $file = $request->file('image');
        // $filesize = round($file->getSize()/1024);
        // // return $filesize;
        // // return 1;
        // $data = getimagesize($file);
        // $width = $data[0]/3;
        // $height = $data[1]/3;
        // $extension = $file->getClientOriginalExtension(); // getting image extension
        // $filename ='image.'.time().'.'.$extension;
        // $image_resize = Imagei::make($file->getRealPath());
        // $file->move('images', $filename);
        // // $image_resize = Imagei::make($request->file('image')->getRealPath());
        // // $image_resize = Imagei::make($file->getRealPath());
        // // $image_resize->resize($width,$height);
        // $image_resize->resize(500, null, function ($constraint) {
        //     $constraint->aspectRatio();
        // });
        // $image_resize->save('images/compressed/'.$filename,100);
        // // $image_resize->save('images/compressed/'.$filename);
        // $image->image =$filename;
        
    }


    public function allmarkups($id)
    {
        // return $id;
        // $data = Comment::where('id',$id)->get();
        $data = Comment::where([['image_id',$id],['markup','!=','null']])->paginate(20);
        // return $data;
        $users = User::all();
        return view('allmarkups',compact('data','users'));

        
    }
    

    public function tagimages($tag)
    {
        $tag = str_replace('-', ' ', $tag);
        $data = Image::where('tags', 'like', '%'.$tag.'%')->paginate(50);
        $type = 'Tag';
        $value = $tag;
        $users = User::all();
        return view('otherimages',compact('data','type','value','users'));
        // return $data;
    }

    public function titleimages($title)
    {
        $title = str_replace('-', ' ', $title);
        $data = Image::where('movie_name', 'like', '%'.$title.'%')->paginate(50);
        $type = 'Title';
        $value = $title;
        $users = User::all();
        return view('otherimages',compact('data','type','value','users'));
        // return $data;
    }

    public function artistimages($artist)
    {
        // return $artist;
        if ($artist == 'unknown') {
            $artist = ''; 
            $data = Image::where('artist_name','')->paginate(50);   
        }else{
            $artist = str_replace('-', ' ', $artist);    
            $data = Image::where('artist_name', 'like', '%'.$artist.'%')->paginate(50);
        }
        
        $type = 'Artist';
        $value = $artist;
        if ($artist == '') {
            $value = 'Unknown';
        };
        $users = User::all();
        return view('otherimages',compact('data','type','value','users'));
        return $data;
    }


    function fetch(Request $request)
    {
        // $search_fields = ['artist_name', 'description', 'tags', 'movie_name'];
        // return 1;
     if($request->get('query'))
     {
      $query = $request->get('query');
      
      $data = DB::table('images')
        ->select('artist_name', DB::raw('count(*) as total'))
        ->groupBy('artist_name')
        ->where([['artist_name', 'LIKE', "%{$query}%"],['deleted','!=','yes'],['status','approved']])
        ->get();

        $datatwo = DB::table('images')
        ->select('movie_name', DB::raw('count(*) as total'))
        ->groupBy('movie_name')
        ->where([['movie_name', 'LIKE', "%{$query}%"],['deleted','!=','yes'],['status','approved']])
        ->get();

        $datathree = DB::table('tags')
        ->select('tag', DB::raw('count(*) as total'))
        ->groupBy('tag')
        ->where('tag', 'LIKE', "%{$query}%")
        ->get();


      $output = '<ul class="dropdown-menu searchul" style="display:block; position:relative ;max-height:100px;width:auto;overflow:auto;">';
      foreach($data as $row)
      {
       $output .= '
       <li style="cursor:pointer;margin-left:10px;" class="searchlistitem" onclick="searchitemfunc(this)" data-value="'.$row->artist_name.'">'.$row->artist_name.'</li>
       ';
      }
      foreach($datatwo as $row)
      {
       $output .= '
       <li style="cursor:pointer;margin-left:10px;" class="searchlistitem" onclick="searchitemfunc(this)" data-value="'.$row->movie_name.'">'.$row->movie_name.'</li>
       ';
      }

       foreach($datathree as $row)
       {
        $output .= '
        <li style="cursor:pointer;margin-left:10px;" class="searchlistitem" onclick="searchitemfunc(this)" data-value="'.$row->tag.'">'.$row->tag.'</li>
        ';
       }
      $output .= '</ul>';
      echo $output;
     }
    }
    
    
    function jmfetch(Request $request)
    {
        // $search_fields = ['artist_name', 'description', 'tags', 'movie_name'];
        
        
    $artists = User::where('artist_type','!=','senior')->get();
    $artistids = $artists->pluck('id')->toArray();
    
    
     if($request->get('query'))
     {
      $queryd = $request->get('query');
      
    
        
        $query = DB::table('images')->select('artist_name', DB::raw('count(*) as total'))->groupBy('artist_name');
        foreach($artistids as $idnew){
            $query->orWhere([['artist_name', 'LIKE', "%{$queryd}%"],['deleted','!=','yes'],['status','approved'],['user_id',$idnew]]);
        }
        $data = $query->get();
        
        $query = DB::table('images')->select('movie_name', DB::raw('count(*) as total'))->groupBy('movie_name');
        foreach($artistids as $idnew){
            $query->orWhere([['movie_name', 'LIKE', "%{$queryd}%"],['deleted','!=','yes'],['status','approved'],['user_id',$idnew]]);
        }
        $datatwo = $query->get();
        
        $datathree = DB::table('tags')
        ->select('tag', DB::raw('count(*) as total'))
        ->groupBy('tag')
        ->where('tag', 'LIKE', "%{$queryd}%")
        ->get();
        

      $output = '<ul class="dropdown-menu searchul" style="display:block; position:relative ;max-height:100px;width:auto;overflow:auto;">';
      foreach($data as $row)
      {
       $output .= '
       <li style="cursor:pointer;margin-left:10px;" class="searchlistitem" onclick="searchitemfunc(this)" data-value="'.$row->artist_name.'">'.$row->artist_name.'</li>
       ';
      }
      foreach($datatwo as $row)
      {
      $output .= '
      <li style="cursor:pointer;margin-left:10px;" class="searchlistitem" onclick="searchitemfunc(this)" data-value="'.$row->movie_name.'">'.$row->movie_name.'</li>
      ';
      }

      foreach($datathree as $row)
      {
        $output .= '
        <li style="cursor:pointer;margin-left:10px;" class="searchlistitem" onclick="searchitemfunc(this)" data-value="'.$row->tag.'">'.$row->tag.'</li>
        ';
      }
      $output .= '</ul>';
      echo $output;
     }
    }
    
    
    
    
    
    
    
}