<?php

namespace App\Http\Controllers;

use App\ReferenceGallery;
use Illuminate\Http\Request;

class ReferenceGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReferenceGallery  $referenceGallery
     * @return \Illuminate\Http\Response
     */
    public function show(ReferenceGallery $referenceGallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReferenceGallery  $referenceGallery
     * @return \Illuminate\Http\Response
     */
    public function edit(ReferenceGallery $referenceGallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReferenceGallery  $referenceGallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReferenceGallery $referenceGallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReferenceGallery  $referenceGallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReferenceGallery $referenceGallery)
    {
        //
    }
}
