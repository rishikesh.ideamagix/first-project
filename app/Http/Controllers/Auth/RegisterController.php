<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            // 'number' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $emails = ['support@atmovfx.com','akil.ideamagix@gmail.com'];
        $data1 = array('content'=>"new user registered withe email address : ".$data['email']." and name : " .$data['name']);
        Mail::send('mails.editreq',$data1, function($message) use($emails){
        $message->to($emails, 'ATMO-VFX')->subject
        ('New User Registration');
        $message->from('contact@atmovfx.com','ATMO');
        });
        
        
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'artist_type' => $data['artisttype'],
            'linkedinprofile' => $data['linkedinprofile'],
            'industryexperience' => $data['industryexperience'],
            'artist_url' => $data['artisturl'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
