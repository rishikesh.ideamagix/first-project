<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function updateuser(Request $request)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $oldname = $user->name;
            $oldartisttype = $user->artist_type;
            $oldemail = $user->email;
            $user->name = $request->name;
            $user->number = $request->number;
            $user->email = $request->email;
            
            $user->artist_type = $request->artisttype;
            if ($request->artisttype == 'senior' && $oldartisttype != 'senior') {
                $user->artist_type_verified = 'no';
            }

            
            $user->artist_url = $request->artisturl;
            $user->linkedinprofile = $request->linkedinprofile;
            $user->industryexperience = $request->industryexperience;
            
            if($request->hasfile('image'))
            {
               $file = $request->file('image');
               $extension = $file->getClientOriginalExtension(); // getting image extension
               $filename ='profile.'.time().'.'.$extension;
               $file->move('assets/images', $filename);

               $oldimage = $user->image;
               if ($oldimage != '1.jpg') {
                    unlink('assets/images/'.$oldimage);
               }
               $user->image =$filename;
            }
            
            
            $imagedata = Image::where('uploaders_name',$oldname)->get();
            foreach ($imagedata as $key => $data) {
                $data->uploaders_name = $user->name;
                $data->save();
            }
            $imagedata = Image::where('uploaders_email',$oldemail)->get();
            foreach ($imagedata as $key => $data) {
                $data->uploaders_email = $user->email;
                $data->save();
            }
            $user->save();
            // $imagedata->save();
            return back();
        }else{
            return redirect('/');
        }


    }
}
