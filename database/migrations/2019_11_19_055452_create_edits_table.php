<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('editors_name');
            $table->string('editors_email');
            $table->string('uploaders_name');
            $table->string('uploaders_email');
            $table->string('claimed');
            $table->string('image_id');
            $table->string('image');
            $table->string('old_artist_name');
            $table->string('new_artist_name');
            $table->string('old_movie_name');
            $table->string('new_movie_name');
            $table->string('old_category');
            $table->string('new_category');
            $table->string('old_tags');
            $table->string('new_tags');
            $table->string('old_description');
            $table->string('new_description');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edits');
    }
}
