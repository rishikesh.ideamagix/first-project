

function canvasdraw() {
		const canvas = document.querySelector("#canvas");
		const ctx = canvas.getContext("2d");

		// canvas.height = window.innerHeight;
		// canvas.width = window.innerWidth;
		var color;
		var mode = 'pen';
		var settings1,settings2,settings3,settings4,settings5,settings6,settings7,settings8;
		
		var settings1 = document.getElementById('colorpanel1');
		settings1.addEventListener('click',function () {
			color = settings1.getAttribute('data-color');
			mode = 'pen';
		});

		var settings2 = document.getElementById('colorpanel2');
		settings2.addEventListener('click',function () {
			color = settings2.getAttribute('data-color');
			mode = 'pen';
		});

		var settings3 = document.getElementById('colorpanel3');
		settings3.addEventListener('click',function () {
			color = settings3.getAttribute('data-color');
			mode = 'pen';
		});

		var settings4 = document.getElementById('colorpanel4');
		settings4.addEventListener('click',function () {
			color = settings4.getAttribute('data-color');
			mode = 'pen';
		});

		var settings5 = document.getElementById('colorpanel5');
		settings5.addEventListener('click',function () {
			color = settings5.getAttribute('data-color');
			mode = 'pen';
		});

		var settings6 = document.getElementById('colorpanel6');
		settings6.addEventListener('click',function () {
			color = settings6.getAttribute('data-color');
			mode = 'pen';
		});

		var settings7 = document.getElementById('colorpanel7');
		settings7.addEventListener('click',function () {
			console.log("eraser");
			mode = 'eraser';
		});


		var settings8 = document.getElementById('colorpanel8');
		settings8.addEventListener('click',function () {
			console.log("undo");
		});

		var canva = document.getElementById('canvasimage');
		var imgheight = canva.height;
		var imgwidth = canva.width;
		console.log(imgheight);

		



		canvas.height = imgheight;
		canvas.width = imgwidth;

		// var background = new Image();
		// background.src = "/assets/images/3.jpg";

		let painting = false;

		function startPosition(e) {
			painting = true;
			draw(e);
		}

		function finishedPosition() {
			painting = false;
			ctx.beginPath();
		}

		function draw(e) {
			if (!painting) {
				return;
			}

			if (mode == 'pen') {
				ctx.globalCompositeOperation="source-over";
				ctx.lineWidth = 4;
				ctx.lineCap = 'round';
				ctx.strokeStyle = color;

				ctx.lineTo(e.offsetX , e.offsetY);
				ctx.stroke();
				ctx.beginPath();
				ctx.moveTo(e.offsetX , e.offsetY);	
			}else{
				var x = e.offsetX;
				var y = e.offsetY;

				ctx.globalCompositeOperation="destination-out";
		      	ctx.beginPath();
	      	    ctx.arc(x, y, 10, 0, 2 * Math.PI);
	      	    ctx.fill();
			}
			
		}

		canvas.addEventListener('mousedown', startPosition)
		canvas.addEventListener('mouseup', finishedPosition)

		canvas.addEventListener('touchstart', startPosition)
		// canvas.addEventListener('touchstart',function () {
		// 	alert("xasxsax");
			
		// });
		canvas.addEventListener('touchend', finishedPosition)
		
		canvas.addEventListener('mousemove', draw)
		
		canvas.addEventListener('touchmove', draw)
}


window.addEventListener("load",function() {
	canvasdraw();
});


window.addEventListener("resize",function() {
	canvasdraw();
});


var savebutton1 = document.getElementById('savebutton');
savebutton1.addEventListener('click',function () {
	var dataURL = canvas.toDataURL();
	$.ajax({
	  type: "POST",
	  url: "/savemarkup",
	  data: { 
	     imgBase64: dataURL
	  }
	}).done(function(o) {
	  console.log(o); 
	});
});


	




