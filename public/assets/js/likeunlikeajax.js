function like(data) {
    var link = $(data).attr('data-link');
    // console.log(link);
    $.ajax({
        url: link,
        type: 'POST',
        success: function(response) {
            // console.log(response);
            if (response == 1) {
                $(data).siblings('.unlikebutton').removeClass("d-none");
                $(data).addClass("d-none");
                // console.log('hua like');
            }
        }
    });
}
function unlike(data) {
    var link = $(data).attr('data-link');
    $.ajax({
        url: link,
        type: 'POST',
        success: function(response) {
            if (response == 1) {
                $(data).siblings('.likebutton').removeClass("d-none");
                $(data).addClass("d-none");
            }
            // console.log('hua unlike');
        }
    });
}