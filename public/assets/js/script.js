(function() {
    var btn = $('#topbutton');

    $(window).scroll(function() {
      if ($(window).scrollTop() > 300) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on('click', function(e) {
      e.preventDefault();
      $('html, body').animate({scrollTop:0}, '300');
    });


    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });


    $(window).on('load', function(){
      $('.page-loader').fadeOut();
    });
    // home page category button open close functionality start
    $('.category-button button').on('click', function() {
        $(this).children('span').toggleClass('fa-chevron-down');
        $('.categories-tab').toggleClass('d-none');
    });
    // home page category button open close functionality end

    // home page filter button active-deactive functionality start
    $('.categories-tab button').on('click', function() {
        $('.categories-tab button').removeClass('active');
        $(this).addClass('active');
    });
    // home page filter button active-deactive functionality end

    // sidenav start
    // sidenav submenu
    var bottomHeaderMenu = $('header nav > ul > li');
    $('header nav > ul > li:has(ul)>a').append('<span class="fas fa-chevron-down"></span>');
    var bottomHeaderMenuCloned = $('header nav > ul > li').clone(bottomHeaderMenu);
    var topHeaderMenu = $('header .signup-container a');
    var topHeaderMenuCloned = $('header .signup-container a').clone(topHeaderMenu);
    $('.sidenav .sidenav-main-menu').append(bottomHeaderMenuCloned);
    $('.sidenav .signup-container').append(topHeaderMenuCloned);


    $('.sidenav-main-menu a > span').on('click', function() {
        console.log(1);
        $(this).parent().siblings('.submenu').slideToggle();
        $(this).toggleClass('fa-chevron-up');
    });

    function sidenavCloseAction() {
        $('.sidenav').removeClass('sidenav-open');
        $('.wrapper').removeClass('overlay');
        $('header .fa-bars').removeClass('fa-times');
        $('.submenu').slideUp();
        $('.sidenav-main-menu >li> a').children('.fa-chevron-down').removeClass('fa-chevron-up');
    }

    $('.sidenav-close').on('click', function() {
        sidenavCloseAction();
    });

    $('header .fa-bars').on('click', function() {
        $('.sidenav').toggleClass('sidenav-open');
        $(this).toggleClass('fa-times');
        $('.wrapper').toggleClass('overlay');
    });
    // sidenav end

    // to close sidemenu & filter section start
    $('.wrapper').on('click', function(e) {
        if ($(e.target).hasClass('overlay')) {
            sidenavCloseAction();
        }
    });

    $(window).on('resize', function() {
        if ($(this).width() > 991) {
            sidenavCloseAction();
        }
    });
    // to close sidemenu & filter section end

    // give top offset start
    // $(window).scroll(function () {
    //   // console.log($('.home-page-search-form').offset().top);
    //   var el = $('.home-page-search-form'); //this would just be your selector
    //   var bottom = el.position().top + el.outerHeight(true);
    //   console.log(bottom);
    //   if ($(window).scrollTop() > bottom) {
    //     console.log(1);
    //   }
    // });
    // give top offset end
})();






// animate on scroll start
// $(window).on('load', function() {
//     AOS.init({
//         // Global settings:
//         disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
//         startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
//         initClassName: 'aos-init', // class applied after initialization
//         animatedClassName: 'aos-animate', // class applied on animation
//         useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
//         disableMutationObserver: false, // disables automatic mutations' detections (advanced)
//         debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
//         throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)


//         // Settings that can be overridden on per-element basis, by `data-aos-*` attributes:
//         offset: 0, // offset (in px) from the original trigger point
//         delay: 750, // values from 0 to 3000, with step 50ms
//         duration: 1000, // values from 0 to 3000, with step 50ms
//         easing: 'ease', // default easing for AOS animations
//         once: true, // whether animation should happen only once - while scrolling down
//         mirror: false, // whether elements should animate out while scrolling past them
//         anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation

//     });
// })
// animate on scroll end





// if (window.location.href.indexOf("images/matte-painting-set-extension") > -1) {
//     var pagename = 'matte-painting-set-extension';
// } else if (window.location.href.indexOf("images/classical-painting") > -1) {
//     var pagename = 'classical-painting';
// } else if (window.location.href.indexOf("images/photography-composition") > -1) {
//     var pagename = 'photography-composition';
// } else if (window.location.href.indexOf("images/movies") > -1) {
//     var pagename = 'movies';
// } else if (window.location.href.indexOf("") > -1) {
//     var pagename = 'all';
// }


    
