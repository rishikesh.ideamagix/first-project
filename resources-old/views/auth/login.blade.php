<!DOCTYPE html>
 <html lang="en">

 <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <meta property="og:url" content="">
   <meta property="og:title" content="">
   <meta property="og:image" content="">
   <meta property="og:site_name" content="">
   <meta property="og:description" content="">
   <meta name="author" content="">
   <meta name="description" content="">
   <meta name="keywords" content="">
   <link rel="icon" href="" sizes="32x32" type="image/png">
   <title>ATMO | Login</title>

   <!-- css files start -->
   <!-- bootstrap 4.0.0 -->
   <link rel="stylesheet" href="/assets/vendor/bootstrap-4.0.0/dist/css/bootstrap.min.css">
   <!-- font awesome 5.11.2 -->
   <link rel="stylesheet" href="/assets/vendor/fontawesome-free-5.11.2-web/css/all.min.css">
   <!-- owl carousel 2.3.4 -->
   <link rel="stylesheet" href="/assets/vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
   <!-- animate.css 3.7.2 -->
   <link rel="stylesheet" href="/assets/vendor/animate.css.3.7.2/animate.3.7.2.css">
   <!-- animate on scroll -->
   <link rel="stylesheet" href="/assets/vendor/aos-master/dist/aos.css">
   <!-- nice select 1.1.0 -->
   <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
   <!-- custom -->
   <link rel="stylesheet" href="/assets/css/style.css">
   <!-- css files end -->
 </head>

 <body>
   <!-- wrapper start -->
   <div class="wrapper">
     <!-- header start -->
     @include('layouts.header')
     <!-- header end -->
     <!-- main start -->
     <main>
       <!-- login form start -->
       <div class="login-form">
         <div class="container">
           <div class="row justify-content-center">
             <div class="col-lg-6">
               <div class="form-wrap p-3">
                 <h4 class="mb-3" style="color: white">Login</h4>
                 <form method="POST" action="{{ route('login') }}" class="w-100">
                    @csrf
                   <div class="input-wrap mb-4">
                     <!-- <input type="text" class="w-100 p-2" placeholder="Enter your email id"> -->
                     <input id="email" type="email" class="form-control @error('email') is-invalid @enderror w-100 p-2" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter Email">

                     @error('email')
                         <span class="invalid-feedback" role="alert">
                             <strong>{{ $message }}</strong>
                         </span>
                     @enderror
                   </div>
                   <div class="input-wrap mb-4">
                     <!-- <input type="password" class="w-100 p-2" placeholder="Enter password"> -->
                     <input id="password" type="password" class="form-control @error('password') is-invalid @enderror w-100 p-2" name="password" required autocomplete="current-password" placeholder="Enter Password">

                     @error('password')
                         <span class="invalid-feedback" role="alert">
                             <strong>{{ $message }}</strong>
                         </span>
                     @enderror
                   </div>
                   <div class="input-wrap">
                     <button class="w-100 p-2">
                       Submit
                     </button>
                   </div>
                 </form>
                 <p class="my-1" style="color: white">Don't have an account? <a href="/register" title="Signup">Signup</a></p>
                 <p class="mb-0"><a href="/password/reset" title="Forgot password?">Forgot password?</a></p>
               </div>
             </div>
           </div>
         </div>
       </div>
       <!-- login form start -->
     </main>
     <!-- main end -->
     <!-- footer start -->
     @include('layouts.footer')
     <!-- footer end -->
     <!-- sidenav start -->
     @include('layouts.sidenav')
     <!-- sidenav end -->
   </div>
   <!-- wrapper end -->
   @include('layouts.js.jquery')
   @include('layouts.js.script')
 </body>

 </html>