<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO | Change Password</title>

  <!-- css files start -->
  <!-- bootstrap 4.0.0 -->
  <link rel="stylesheet" href="/assets/vendor/bootstrap-4.0.0/dist/css/bootstrap.min.css">
  <!-- font awesome 5.11.2 -->
  <link rel="stylesheet" href="/assets/vendor/fontawesome-free-5.11.2-web/css/all.min.css">
  <!-- owl carousel 2.3.4 -->
  <link rel="stylesheet" href="/assets/vendor/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css">
  <!-- animate.css 3.7.2 -->
  <link rel="stylesheet" href="/assets/vendor/animate.css.3.7.2/animate.3.7.2.css">
  <!-- animate on scroll -->
  <link rel="stylesheet" href="/assets/vendor/aos-master/dist/aos.css">
  <!-- nice select 1.1.0 -->
  <link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
  <!-- custom -->
  <link rel="stylesheet" href="/assets/css/style.css">
  <!-- css files end -->
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <!-- user profile avatar start -->
      @include('layouts.userheader')
      <!-- user profile avatar end -->
      <!-- user panel page selector start -->
      <div class="user-panel-page-selector">
          <div class="container-fluid px-2">
              @include('layouts.userdropdown')
          </div>
        </div>
        <!-- user panel page selector end -->
      <!-- change password start -->
      <div class="change-password py-5">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <div class="form-wrap p-3">
                <h4 class="mb-3">Reset your password</h4>
                <!-- <form action="{{ url('/user/credentials') }}" id="form-change-password" role="form" method="POST" class="w-100" novalidate>
                  <div class="input-wrap mb-4">
                    <input type="text" class="w-100 p-2" placeholder="Enter old password">
                  </div>
                  <div class="input-wrap mb-4">
                    <input type="text" class="w-100 p-2" placeholder="Enter new password">
                  </div>
                  <div class="input-wrap mb-4">
                    <input type="text" class="w-100 p-2" placeholder="Confirm new password">
                  </div>
                  <div class="input-wrap">
                    <button class="w-100 p-2">
                      Submit
                    </button>
                  </div>
                </form> -->
                <div>
                    @if($errors->any())
                        <div class="alert alert-primary">
                            <h6 style="margin-bottom: 0">{{$errors->first()}}</h6>    
                        </div>
                    @endif
                </div>
                <form id="form-change-password" role="form" method="POST" action="{{ url('/user/credentials') }}" novalidate class="form-horizontal">
                  <div class=""> 
                    <!-- <label for="current-password" class="col-sm-4 control-label">Current Password</label> -->
                    <div class="">
                      <div class="form-group">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Current Password" required="" style="border-radius: 0;">
                      </div>
                    </div>
                    <!-- <label for="password" class="col-sm-4 control-label">New Password</label> -->
                    <div class="">
                      <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="New Password" required="" style="border-radius: 0;">
                      </div>
                    </div>
                    <!-- <label for="password_confirmation" class="col-sm-4 control-label">Re-enter Password</label> -->
                    <div class="">
                      <div class="form-group">
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter New Password" required="" style="border-radius: 0;">
                      </div>
                    </div>
                  </div>
                  <div class="form-group buttons">
                    <div class="right ml-auto">
                        <button type="submit" class=" btn btn-theme-padding btn-theme-yellow ">Update</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- change password start -->
    </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  
  @include('layouts.js.jquery')
  @include('layouts.js.niceselect')
  @include('layouts.js.script')
  <script type="text/javascript">
    $('#userpages').on('change', function() {
      var link = this.value;
      window.location.href = link;
    });
  </script>
</body>

</html>