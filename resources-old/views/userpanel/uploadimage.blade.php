<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title> Upload | ATMO </title>

  <!-- css files start -->
  
  @include('layouts.styles')
  <!-- css files end -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="/assets/vendor/select2/select2.min.css">
  

<style type="text/css">
  .nice-select{
    line-height: 24px;
  }
  .nice-select .option:hover, .nice-select .option.focus, .nice-select .option.selected.focus{
    background: #181F27;
  }

  .select2-container--default .select2-selection--multiple{
    background: transparent;
    border: 2px solid #5cd9d3;
    border-radius: 0;
    min-height: 40px;
  }
.select2-container--default.select2-container--focus .select2-selection--multiple{
    border: 2px solid #5cd9d3;
  }
  .select2-container--default .select2-selection--multiple .select2-selection__choice{
    background: #5cd9d3;
    border: 0;
  }


    .select2-container--default .select2-selection--single{
      background: transparent;
      border: 2px solid #5cd9d3;
      border-radius: 0;
      min-height: 40px;
    }
  .select2-container--default.select2-container--focus .select2-selection--single{
      border: 2px solid #5cd9d3;
    }
    .select2-container--default .select2-selection--single .select2-selection__choice{
      background: #5cd9d3;
      border: 0;
    }

    .select2-selection__rendered{
      margin-top: 3px;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow{
      margin-top: 5px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered{
      color: #5CD9D3;
    }

  .select2-container{
    width: 100% !important;
  }

  .select2-container--default .select2-search--inline .select2-search__field{
    color: #fff;
  }
  /*.select2-container--default .select2-selection--multiple .select2-selection__rendered li{
    background: #5cd9d3;
  }*/
</style>
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
     <main>

        <!-- home page search form start -->
        <div class="home-page-search-form">
            <div class="container">
              <div class="row justify-content-center">
                    <nav aria-label="breadcrumb " class="pb-2">
                        <h2 class="pt-5  text-white">Upload Image</h2>
                        <ol class="breadcrumb justify-content-center">
                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Upload</li>
                        </ol>
                    </nav>
              </div>
            </div>
        </div>
        <!-- home page search form end -->
        <div class="container">
                <div class="row">
                    <div class="col-md-10 mx-auto my-5 coverform p-5">
                        <div class=" coverall ">
                            <form method="post" id="dropFileForm" action="/image" onsubmit="return validation()" enctype="multipart/form-data" >
                                {{ csrf_field() }}
                                <div class="row">
                                  <div class="col-lg-6 mt-3">
                                      <label for="image"  class="text-white text-left  d-block"> Image : </label>
                                      <div class="cover-upload-image py-4  py-sm-5 px-sm-5" style="padding-bottom: 4.3em !important">
                                          <div class="check">
                                              <input type="file" name="image" accept="image/*" id="fileInput"
                                                  onchange="addFiles(event)" >
                                          </div>
                                          <label for="fileInput" id="fileLabel"
                                              ondragover="overrideDefault(event);fileHover();"
                                              ondragenter="overrideDefault(event);fileHover();"
                                              ondragleave="overrideDefault(event);fileHoverEnd();" ondrop="overrideDefault(event);fileHoverEnd(); addFiles(event);">
                                              <i class="fa fa-download fa-5x"></i>
                                              <br>
                                              <span id="fileLabelText" class="py-2">
                                                  Choose a file or drag it here
                                              </span>
                                              <br>
                                              <span id="uploadStatus"></span>
                                          </label>
                                      </div>
                                      <span id="fileerror" class="brandcolor font-weight-bold py-1 d-block text-left"></span>
                                      <span id="common" class="brandcolor font-weight-bold py-1 d-block text-left"></span>
                                      
                                      <!-- <label for="tags"  class="text-white text-left  d-block"> Tags : </label>
                                      <input type="text" value="" name="tags" id="taginput" class="mytags" data-role="tagsinput" placeholder="Add Tags" />
                                      <span id="tagerror" class="brandcolor font-weight-bold pb-2 d-block text-left"></span>
                                      <span id="common" class="brandcolor font-weight-bold pb-2 d-block text-left"></span> -->
                                      <label for=""  class="text-white text-left  d-block"> Tag(s) : </label>
                                      <select class="js-example-basic-multiple" name="tags[]" multiple="multiple" style="color: #fff">
                                        @foreach($tags as $tag)
                                          <option value="{{$tag->tag}}">{{ucfirst($tag->tag)}}</option>
                                        @endforeach
                                      </select>


                                  </div>
                                  <div class="col-lg-6 my-3">
                                    <div class="form-group  text-lg-left mb-1" style="color: white">
                                        <label for="artist"  class="text-white text-left  d-block"> Artist : </label>
                                        
                                        <select class="js-example-basic-single" placeholder="Select Artist"  name="artistnamedropdown"   style="width: 100%;background: transparent;" required="">
                                            <option value="{{Auth::user()->id}}" >{{ Auth::user()->name }}</option>
                                            <option value="other" >Other (Please Enter Name)</option>
                                            <option value="unknown" >Unknown</option>
                                          @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}} </option>
                                          @endforeach
                                        </select>
                                        
                                        <input type="text" name="artist_name" id="artist" placeholder="Enter Artist Name" class="artist_name d-none mt-3">
                                        <!-- <span class="brandcolor font-weight-bold pb-2 d-block text-left">
                                          <input type="checkbox" id="artist-checkbox" name="i_am_artist"> <small>I Am The Artist</small>
                                        </span> -->
                                    </div>
                                    <div class="form-group  text-lg-left mb-1">
                                        <label for="title"  class="text-white text-left  d-block"> Movie/TV Title : </label>
                                        <input type="text" id="artist" name="movie_name" placeholder="Enter Movie Name" class="mytextfield">
                                    </div>
                                    <div class="form-group  text-lg-left mb-1" style="color: white">
                                        <label for="myselect"  class="text-white text-left  d-block"> Category : </label>
                                        <select class="js-example-basic-multiple" placeholder="Select Category"  name="category[]" multiple="multiple" style="width: 100%;background: transparent;" required="">
                                          <option value="matte-painting-set-extension">Matte Painting Set Extension</option>
                                          <option value="classical-painting">Classical Painting</option>
                                          <option value="photography-composition">Photography Composition</option>
                                          <option value="movies">Movies</option>
                                          <option value="games">Games</option>
                                        </select>
                                    </div>

                                    <!-- <div class="form-group  text-lg-left mb-1" style="color: white">
                                        <label for="myselect"  class="text-white text-left  d-block"> Category : </label>
                                        <select class="js-example-basic-multiple" placeholder="Select Category"  name="category" style="width: 100%;background: transparent;">
                                          <option value="matte-painting-set-extension">Matte Painting Set Extension</option>
                                          <option value="classical-painting">Classical Painting</option>
                                          <option value="photography-composition">Photography Composition</option>
                                          <option value="movies">Movies</option>
                                          <option value="games">Games</option>
                                        </select>
                                    </div> -->
                                    <label for="description"  class="text-white text-left  d-block"> Description : </label>
                                    <textarea id="dis" rows="5" class="adddiscription mt-2" placeholder="Add Description" name="description"></textarea>
                                    
                                  </div>
                                </div>
                                <div class="d-flex justify-content-sm-end col-lg-4  ml-auto my-2 px-0">
                                  <input type="submit" value="SUBMIT" class="uploadButton mr-1 submitbtn">
                                  <!-- <a href="#" class="uploadButton ml-1 cancelbtn">CANCEL</a> -->
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
    </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->
    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  @include('layouts.js.jquery')
  @include('layouts.js.select2')
  @include('layouts.js.script')
  @include('layouts.js.bootstraptagsinput')
  @include('layouts.js.upload')
  <script type="text/javascript">
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2({
          placeholder: "Select",
          allowClear: true
        });

        $('.js-example-basic-multiple-one').select2({
          placeholder: "Select",
          allowClear: true,
          maximumSelectionLength: 1,
          language: {
                  // You can find all of the options in the language files provided in the
                  // build. They all must be functions that return the string that should be
                  // displayed.
                  maximumSelected: function (e) {
                      var t = "You can only select " + e.maximum + " artist";
                      e.maximum != 1 && (t += "s");
                      return t ;
                  }
              }
        });

        $('.js-example-basic-single').select2({
          placeholder: "Select",
          allowClear: true,
          multiple: false
        });

        $('.js-example-basic-single').on('change', function() {
          var data = $(".js-example-basic-single option:selected").val();
          console.log(data);

          if (data == 'other') {
            $('#artist').removeClass('d-none');
            $('#artist').attr('required','true');
          }else{
            $('#artist').addClass('d-none');
            $('#artist').removeAttr('required');
          }
          



          // $("#test").val(data);

        })
    });
    $('input[type="checkbox"]').click(function(){
      if($(this).prop("checked") == true){
          $('.artist_name').attr('required','required');
      }
      else if($(this).prop("checked") == false){
          $('.artist_name').removeAttr('required','required');
      }
    });


  </script>
</body>

</html>