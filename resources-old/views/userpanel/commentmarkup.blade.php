<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:image" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta name="author" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="" sizes="32x32" type="image/png">
    <title> Profile | ATMO </title>

    <!-- css files start -->

    @include('layouts.styles')
    <!-- bootstrap.tagsinput -->
    <style type="text/css">
        .nice-select {
            line-height: 24px;
        }
        
        .nice-select .option:hover,
        .nice-select .option.focus,
        .nice-select .option.selected.focus {
            background: #181F27;
        }
        
        .select2-container--default .select2-selection--multiple {
            background: transparent;
            border: 2px solid #5cd9d3;
            border-radius: 0;
            min-height: 40px;
        }
        
        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 2px solid #5cd9d3;
        }
        
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background: #5cd9d3;
            border: 0;
        }


        .bootstrap-tagsinput{
            background: transparent;
            border:2px solid #5cd9d3;
            min-height: 40px;
            border-radius: 0;
            width: 100%;
        }

        .pencilicon { 
            cursor: url('data:image/x-icon;base64,AAACAAEAICAQAAAAAADoAgAAFgAAACgAAAAgAAAAQAAAAAEABAAAAAAAAAIAAAAAAAAAAAAAEAAAAAAAAAAAAAAAhYWFAPqv6ADgm4sASkpKAJ/l7QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACIAAAAAAAAAAAAAAAAAAAEiIAAAAAAAAAAAAAAAAAAxEiIAAAAAAAAAAAAAAAADMxEgAAAAAAAAAAAAAAAAMzMxAAAAAAAAAAAAAAAAAzMzMAAAAAAAAAAAAAAAADMzMwAAAAAAAAAAAAAAAAMzMzAAAAAAAAAAAAAAAAAzMzMAAAAAAAAAAAAAAAADMzMwAAAAAAAAAAAAAAAABTMzAAAAAAAAAAAAAAAAAFVTMAAAAAAAAAAAAAAAAABFVQAAAAAAAAAAAAAAAAAARAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///////////////////////////////////////////////////////////////////////////////////////P////h////wP///4H///8D///+B////A////gf///wP///4H///+D////B////w////8///////////////w=='), auto; 
        }

        .erasericon { 
            /*cursor: url('data:image/x-icon;base64,AAACAAEAICACAAAAAAAwAQAAFgAAACgAAAAgAAAAQAAAAAEAAQAAAAAAgAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAA66TnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgAAAC4AAABuAAAA4AAAAdwAAAO4AAAHcAAABuAAAAXAAAADgAAAAAAAAA///////////////////////////////////////////////////////////////////////////////////////////////////////////+D////A////gP///wD///4A///8Af//+AP///AH///wD///8B////A////wf///8='), auto; */
            cursor: url('/eraser.cur'), auto; 
        }


    </style>
</head>

<body>
    @php
      if(Auth::check())
      {
          $user = Auth::user();
          $signedinid = Auth::user()->id;
          $signedinname = Auth::user()->name;
          $signedinemail = Auth::user()->email;
          $signedinimage = Auth::user()->image;
      }else{
          $signedinname = "notsi";
          $signedinemail = "notsi";
          $signedinid = 0;
          $signedinimage = 0;
      }
    @endphp
    <!-- wrapper start -->
    <div class="wrapper">
        <!-- header start -->
        @include('layouts.header')
        <!-- header end -->
        <!-- main start -->
        <main>

            <!-- home page search form start -->
            <div class="home-page-search-form">
                <div class="container">
                    <div class="row justify-content-center">
                        <nav aria-label="breadcrumb " class="pb-2">
                            <h2 class="pt-5 text-center text-white">Profile</h2>
                            <ol class="breadcrumb justify-content-center">
                                <li class="breadcrumb-item"><a href="/">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Profile</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- home page search form end -->
            <div class="container-fluid">
                <div class="my-5 coverform px-0">
                    <div class=" coverall ">
                        <div class="profile-uploaded-image flex-column">
                            <figure id="figdiv" class="mb-0" style="position: relative;">
                                <img src="/images/{{$image->image}}" class="w-100 profileimage" id="canvasimage" alt="">
                                <canvas id="canvas" style="position: absolute;top:0; left: 0;" class="pencilicon"></canvas>
                            </figure>
                            <div style="margin-top: 5px;" class="settingpanel d-flex flex-wrap">
                                <div id="" style="margin:0 5px;color:white;" data-color="#f00;">Choose Pencil Color : </div>
                                <div class="mt-1" id="colorpanel1" style="background: #f00; height: 15px; width: 15px; border: 1px solid gray; margin:0 5px;" data-color="#f00"></div>
                                <div class="mt-1" id="colorpanel2" style="background: #0f0; height: 15px; width: 15px; border: 1px solid gray; margin-right:5px;" data-color="#0f0"></div>
                                <div class="mt-1" id="colorpanel3" style="background: #00f; height: 15px; width: 15px; border: 1px solid gray; margin-right:5px;" data-color="#00f"></div>
                                <div class="mt-1" id="colorpanel4" style="background: #ff0; height: 15px; width: 15px; border: 1px solid gray; margin-right:5px;" data-color="#ff0"></div>
                                <div class="mt-1" id="colorpanel5" style="background: #000; height: 15px; width: 15px; border: 1px solid gray; margin-right:5px;" data-color="#000"></div>
                                <div class="mt-1" id="colorpanel6" style="background: #fff; height: 15px; width: 15px; border: 1px solid gray; margin-right:5px;" data-color="#fff"></div>
                                
                                <div id="colorpanel7" style=";color:white;margin-right:5px;" class="ml-auto">
                                    <i class="fas fa-eraser"></i>
                                </div>
                                <div id="resetbutton" style="background: transparent; border: 1px solid #5CD9D3; margin:0 5px;color:#5CD9D3;padding:1px;" data-color="#fff">Reset</div>
                                <div id="colorpanel8" style="color: white;margin-right:5px;">
                                    <!-- <i class="fas fa-undo"></i> -->
                                </div>
                                <!-- <div id="ssbutton" style="background: brown;  border: 1px solid gray; margin-right:5px;" onclick="takess();" >ss</div> -->
                                <!-- <div id="savebutton" style="background: purple;  border: 1px solid gray; margin-right:5px;">Save</div> -->
                            </div>
                        </div>
                        <div>
                            <form id="markupform" action="/comment" method="post" class="m-0 mt-4">
                                @csrf
                                <input type="hidden" name="user_id" value="{{$user->id}}">
                                <input type="hidden" name="user_name" value="{{$user->name}}">
                                <input type="hidden" name="user_email_id" value="{{$user->email}}">
                                <input type="hidden" name="image" value="{{$user->image}}">
                                <input type="hidden" name="image_id" value="{{$image->id}}">
                                <input type="hidden" name="image" value="{{$image->image}}">
                                <input type="hidden" name="user_profile_image" value="{{$user->image}}">

                                <input type="hidden" name="markup" id="markupimage" value="">
                                <div class="form-group  text-lg-left m-4">
                                    <textarea id="dis" cols="50" rows="6" class="adddiscription" name="comment" placeholder="Add Comment (Required)" required="" onkeyup="emptycheck()"></textarea>
                                    <div class=" profilesubmit  mt-auto mb-2 ml-auto text-right">
                                        <!-- <input type="submit" value="Add marks" name="submit" class="uploadButton mr-1 submitbtn  text-right"> -->
                                        <!-- <a href="javascript:void(0)" class="uploadButton ml-1 cancelbtn  text-right">Post</a> -->
                                        <!--<input class="btn mb-4" type="button" style="background: #5CD9D3;border-radius: 0" onclick="takess(this);" value="Post Comment" id="submitbutton" disabled=""></input>-->
                                        <button class="btn mb-4" type="button" style="background:transparent;border-radius: 0;color:#5CD9D3;border-color:#5CD9D3;" onclick="takess(this);"  id="submitbutton" disabled="">Post Comment <img src="/atmo2.gif" id="loadgif" class="d-none" style="width:25px;"></button>
                                        <!-- <a href="/markupcomment/{{$image->id}}" class="btn btn-success" style="background: #5CD9D3;border-radius: 0">Comment With Markup</a> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- main end -->
        <!-- footer start -->
        @include('layouts.footer')
        <!-- footer end -->
        <!-- sidenav start -->
        @include('layouts.sidenav')
        <!-- sidenav end -->
    </div>

    <!-- wrapper end -->
    <!-- javascript files start -->

    <!-- jquery 3.4.1 -->
    @include('layouts.js.jquery')
    @include('layouts.js.script')
    @include('layouts.js.canvaswithscreenshot')
    <script type="text/javascript">
        function emptycheck(xyz) {
            var disval = $("#dis").val();
            disval = disval.replace(/\s/g,'');
            if (disval == '') {
                $('#submitbutton').attr('disabled','true');
            }else{
                $('#submitbutton').removeAttr('disabled');
            }
        }
    </script>

</body>

</html>