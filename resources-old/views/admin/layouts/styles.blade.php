<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="Admin | ATMO">
<meta name="keywords" content="">
<meta name="author" content="">
<title>Admin | ATMO</title>
<!-- <link rel="apple-touch-icon" href="/adminassets/app-assets/images/ico/apple-icon-120.png"> -->
<!-- <link rel="shortcut icon" type="image/x-icon" href="https://themeselection.com/demo/chameleon-admin-template/app-assets/images/ico/favicon.ico"> -->
<link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">

<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/vendors/css/vendors.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/vendors/css/forms/toggle/switchery.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/plugins/forms/switch.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/core/colors/palette-switch.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/vendors/css/charts/chartist.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/vendors/css/charts/chartist-plugin-tooltip.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/vendors/css/tables/datatable/datatables.min.css">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/bootstrap-extended.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/colors.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/components.min.css">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/core/colors/palette-gradient.min.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/pages/chat-application.css">
<link rel="stylesheet" type="text/css" href="/adminassets/app-assets/css/pages/dashboard-analytics.min.css">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="/adminassets/assets/css/style.css">
<!-- END: Custom CSS-->

<script src="https://unpkg.com/feather-icons"></script>
<script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>

<style>
    .page-loader {
      position: fixed;
      height: 100vh;
      width: 100vw;
      background: #030303;
      /*background:#f5f5f5;*/
      top: 0;
      left: 0;
      z-index: 10000;
    }
    
    .page-loader img {
      width: 500px;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%,-50%);
      -webkit-transform: translate(-50%,-50%);
      -moz-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
      -o-transform: translate(-50%,-50%);
    }
</style>

<meta name="csrf-token" content="{{ csrf_token() }}" />





