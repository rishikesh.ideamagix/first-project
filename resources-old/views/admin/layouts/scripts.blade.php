

<!-- BEGIN: Vendor JS-->
<script src="/adminassets/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/forms/toggle/switchery.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/js/scripts/forms/switch.min.js" type="text/javascript"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!--<script src="/adminassets/app-assets/vendors/js/charts/chartist.min.js" type="text/javascript"></script>-->
<!--<script src="/adminassets/app-assets/vendors/js/charts/chartist-plugin-tooltip.min.js" type="text/javascript"></script>-->


<!-- END: Page Vendor JS-->



<!-- BEGIN: Theme JS-->
<script src="/adminassets/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/js/core/app.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/jquery.sharrre.js" type="text/javascript"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="/adminassets/app-assets/js/scripts/pages/dashboard-analytics.min.js" type="text/javascript"></script>
<!-- END: Page JS-->

<script src="/adminassets/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/buttons.flash.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/jszip.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/pdfmake.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/vfs_fonts.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/buttons.html5.min.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/vendors/js/tables/buttons.print.min.js" type="text/javascript"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>


<script src="/adminassets/app-assets/js/scripts/tables/datatables/datatable-advanced.min.js" type="text/javascript"></script>


<script src="/adminassets/app-assets/vendors/js/extensions/sweetalert2.all.js" type="text/javascript"></script>
<script src="/adminassets/app-assets/js/scripts/extensions/sweet-alerts.min.js" type="text/javascript"></script>


<!-- <script src="/adminassets/app-assets/vendors/js/editors/ckeditor/ckeditor.js" type="text/javascript"></script> -->
<!-- <script src="/adminassets/app-assets/js/scripts/editors/editor-ckeditor.min.js" type="text/javascript"></script> -->




<script type="text/javascript">
	$(window).on('load', function(){
      $('.page-loader').fadeOut();
    });
	
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
</script>