<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                      <h4 class="card-title" style="color: #6b6f80">Image Detail</h4>
                      <img src="/images/{{$data->image}}" style="width: 100%;margin-top: 2%;margin-bottom: 2%">
                      <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                      <h4>Image Claimed : {{ucfirst($data->claimed)}}</h4>
                      <hr>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="row">
                              <div class="col-6">
                                <h3>Old Details</h3>
                                <ul>
                                  <li><b>Uploaders Name</b> : {{$data->uploaders_name}}</li>
                                  <li><b>Uploaders Email</b> : {{$data->uploaders_email}} <a class="btn btn-sm btn-primary" href="mailto:{{$data->uploaders_email}}">Mail</a></li>
                                  <hr>
                                  <li><b>Artist Name</b> : {{$data->old_artist_name}}</li>
                                  <li><b>Movie Name</b> : {{$data->old_movie_name}}</li>
                                  @php
                                    $catdata = str_replace("-", " ", $data->old_category);
                                    $catdata = str_replace(",", ", ", $catdata);
                                  @endphp
                                  <li><b>Category</b> : {{ucwords($catdata)}}.</li>
                                  <li><b>Tags</b> : {{$data->old_tags}}</li>
                                  <li><b>Description</b> : {{$data->old_description}}</li>
                                </ul>
                              </div>
                              <div class="col-6">
                                <h3>New Details</h3>
                                <ul>
                                  <li><b>Editors Name</b> : {{$data->editors_name}}</li>
                                  <li><b>Editors Email</b> : {{$data->editors_email}} <a class="btn btn-sm btn-primary" href="mailto:{{$data->editors_email}}">Mail</a></li>
                                  <hr>
                                  <li><b>Artist Name</b> : {{$data->new_artist_name}}</li>
                                  <li><b>Movie Name</b> : {{$data->new_movie_name}}</li>
                                  @php
                                    $catdata = str_replace("-", " ", $data->new_category);
                                    $catdata = str_replace(",", ", ", $catdata);
                                  @endphp
                                  <li><b>Category</b> : {{ucwords($catdata)}}.</li>
                                  <li><b>Tags</b> : {{$data->new_tags}}</li>
                                  <li><b>Description</b> : {{$data->new_description}}</li>
                                </ul>
                              </div>
                              @if($data->status == 'pending')
                                <div class="col-6">
                                  <a href="/admin/rejectedit/{{$data->id}}" class="btn btn-danger" style="width: 100%;color: white">Reject</a>
                                </div>
                                <div class="col-6">
                                  <a href="/admin/approveedit/{{$data->id}}" class="btn btn-success" style="width: 100%;color: white">Approve</a>
                                </div>
                              @else
                                <!-- <div class="col-12"> -->
                                  <button class="btn btn-primary" style="width:100%">
                                    @if($data->status == 'approved')
                                      Already Approved
                                    @else
                                      Already Rejected
                                    @endif
                                  </button>
                                <!-- </div> -->
                              @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->


    @include('admin.layouts.scripts')
    
    

  </body>
  <!-- END: Body-->
</html>