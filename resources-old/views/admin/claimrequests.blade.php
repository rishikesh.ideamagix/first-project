<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" style="color: #6b6f80">Claim Requests</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <!-- <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p> -->
                            <div class="table-responsive">
                                <!-- <table class="custom-configuration table display nowrap table-striped table-bordered"> -->
                                <table class="table table-striped table-bordered dom-jQuery-events">
                                    <thead>
                                      <tr>
                                          <th>Image</th>
                                          <th>Claimers Name</th>
                                          <th>Owners Name</th>
                                          <th>Claimed</th>
                                          <th>Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($claimrequests as $data)
                                        <tr>
                                          <td>
                                            <a href="/admin/claimdetail/{{$data->id}}"><img src="/images/{{$data->image}}" style="width: 200px"></a>
                                          </td>
                                          <td>{{$data->claimers_name}}</td>
                                          <td>{{$data->owners_name}}</td>
                                          <td>{{ucfirst($data->claimed)}}</td>
                                          <td>{{ucfirst($data->status)}}</td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->


    @include('admin.layouts.scripts')
    
    

  </body>
  <!-- END: Body-->
</html>