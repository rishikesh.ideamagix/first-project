<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Image Detail</h4>
                      <img src="/images/{{$data->image}}" style="width: 100%;margin-top: 2%;margin-bottom: 2%">
                      <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                      <h4>Image Claimed : {{ucfirst($data->claimed)}}</h4>
                      <span>Claim Message : {{$data->message}}</span>
                      @if($data->supporting_document != '')
                        <a class="btn btn-sm btn-primary" href="/supportingdocs/{{$data->supporting_document}}" download>Download Supporting Document</a>
                      @else
                        <span style="display: block;">No Supporting Document Attached.</span>
                      @endif
                      <hr>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <div class="row">
                              <div class="col-6">
                                <h3>Old Details</h3>
                                <ul>
                                  <li><b>Owners Name</b> : {{$data->owners_name}}</li>
                                  <li><b>Owners Email</b> : {{$data->owners_email}} <a class="btn btn-sm btn-primary" href="mailto:{{$data->owners_email}}">Mail</a></li>
                                </ul>
                              </div>
                              <div class="col-6">
                                <h3>New Details</h3>
                                <ul>
                                  <li><b>Claimers Name</b> : {{$data->claimers_name}}</li>
                                  <li><b>Claimers Email</b> : {{$data->claimers_email}} <a class="btn btn-sm btn-primary" href="mailto:{{$data->uploaders_email}}">Mail</a></li>
                                </ul>
                              </div>
                              @if($data->status == 'pending')
                                <div class="col-6">
                                  <a href="/admin/rejectclaim/{{$data->id}}" class="btn btn-danger" style="width: 100%;color: white">Reject</a>
                                </div>
                                <div class="col-6">
                                  <a href="/admin/approveclaim/{{$data->id}}" class="btn btn-success" style="width: 100%;color: white">Approve</a>
                                </div>
                              @else
                                <!-- <div class="col-12"> -->
                                  <span>Status : {{ucfirst($data->status)}}</span>
                                  <a href="/admin/deleteclaim/{{$data->id}}" class="btn btn-primary" style="width:100%;color: white">
                                    Delete Claim
                                  </a>
                                <!-- </div> -->
                              @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->


    @include('admin.layouts.scripts')
    
    

  </body>
  <!-- END: Body-->
</html>