<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <!-- BEGIN: Head-->
  <head>
    @include('admin.layouts.styles')  
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu 2-columns   fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    @include('admin.layouts.navs')

    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row"></div>
        <div class="content-body"><!-- Revenue, Hit Rate & Deals -->
          <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" style="color: #6b6f80">User's</h4>
                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body card-dashboard">
                            <!-- <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p> -->
                            <div class="table-responsive">
                                <!-- <table class="custom-configuration table display nowrap table-striped table-bordered"> -->
                                <table class="table table-striped table-bordered dom-jQuery-events">
                                    <thead>
                                      <tr>
                                          <th style="display: none;">Id</th>
                                          <th>User Name</th>
                                          <th>User Email</th>
                                          <th>Artist Type</th>
                                          <th>Art Link</th>
                                          <th>Artist Type Approved</th>
                                          <th>User Status</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($data as $datanew)
                                        <tr>
                                          <td style="display: none;">{{$datanew->id}}</td>
                                          <td>{{$datanew->name}}</td>
                                          <td>{{$datanew->email}}</td>
                                          <td>{{$datanew->artist_type}}</td>
                                          <td>
                                            <a href="{{$datanew->artist_url}}">{{$datanew->artist_url}}</a>
                                          </td>
                                          <td>
                                            @if($datanew->artist_type == 'junior')
                                              No approval required
                                            @else
                                              <select class="approvalselect" data-id="{{$datanew->id}}">
                                                <option value="no" @if($datanew->artist_type_verified == 'no') selected @endif>No</option>
                                                <option value="yes" @if($datanew->artist_type_verified == 'yes') selected @endif>Yes</option>
                                              </select>  
                                            @endif
                                          </td>
                                          <td>
                                            <select class="statusselect" data-id="{{$datanew->id}}">
                                              <option value="enabled" @if($datanew->status == 'enabled') selected @endif>Enable</option>
                                              <option value="disabled" @if($datanew->status == 'disabled') selected @endif>Disable</option>
                                            </select>
                                          </td>
                                        </tr>
                                      @endforeach
                                      
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    @include('admin.layouts.footer')
    <!-- END: Footer-->


    @include('admin.layouts.scripts')
    
    <script type="text/javascript">
      $('.statusselect').on('change', function() {
        var status = this.value;
        var id = $(this).attr('data-id');
        var link = '/admin/changeuserstatus/'+id+'/'+status;
        console.log(link);
        $.ajax({
                url: link,
                type: "post",
            });
      });

      $('.approvalselect').on('change', function() {
        var status = this.value;
        var id = $(this).attr('data-id');
        var link = '/admin/changeapprovalstatus/'+id+'/'+status;
        console.log(link);
        $.ajax({
                url: link,
                type: "post",
            });
      });
    </script>

  </body>
  <!-- END: Body-->
</html>