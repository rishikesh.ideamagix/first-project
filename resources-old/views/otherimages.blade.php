<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO</title>

  @include('layouts.styles')
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <!-- result grid start -->
      <section class="result-grid py-3 px-2 px-lg-0" style="min-height: 80vh">
        <h3 style="text-align: center;color: #5CD9D3">{{$type}} : {{ucfirst($value)}}</h3>
        <div class="container-fluid px-2 py-1">
          <div id="mecy" class="image-container">
            <!-- Automatic data comes here -->
            @foreach($data as $datanew)
              <div class="box" data-aos="fade-up">
                <a href="/image/{{$datanew->id}}" title="" class="d-block">
                  <img src="/images/compressed/{{$datanew->image}}" alt="" class="lazy">
                </a>
                @php 
                  $artistlink = preg_replace('/\s+/', '-', $datanew->artist_name);
                  $titlelink = preg_replace('/\s+/', '-', $datanew->movie_name);
                  $blanklink = 'unknown';
                  $newlink = '';

                  if($datanew->artist_name != ''){
                    $newlink = 'artist/'.$artistlink;
                  }else if($datanew->movie_name != ''){
                    $newlink = 'title/'.$titlelink;
                  }else{
                    $newlink = 'artist/unknown';
                  }
                @endphp

                <div class="home-page-profile-container">
                  <div class="user-detail d-flex align-items-center justify-content-between">
                    <a href="/images/{{$newlink}}" class="d-flex align-items-center fl70p" style="">
                      @if($datanew->claimed == 'yes')
                        <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle">
                      @else  
                        <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                      @endif
                      @if($datanew->artist_name != '')
                        <p class="mb-0">
                            {{$datanew->artist_name}}
                            @if($datanew->claimed == 'yes')
                            @foreach($users as $user)
                              @if($datanew->user_id == $user->id)
                                @if($user->artist_type == 'senior'  && $user->artist_type_verified == 'yes' )
                                  <img src="/verify.png" style="width: 20px !important">
                                @endif
                              @endif
                            @endforeach
                          @endif    
                        </p>
                      @elseif($datanew->movie_name != '')
                        <p class="mb-0">{{$datanew->movie_name}}</p>
                      @else
                        <p class="mb-0">Unknown</p>
                      @endif
                    </a>
                    <div class="fl74p" style="">
                      <a href="/image/{{$datanew->id}}" class="likes-wrap d-flex justify-content-end">
                        <!--<div class="counts-container d-flex align-items-center mr-2">-->
                        <!--  <i class="fas fa-share"></i>-->
                        <!--</div>-->
                        <div class="counts-container d-flex align-items-center">
                          <i class="far fa-comment"></i>
                          <span class="count">
                            {{$datanew->comment_count}}
                          </span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="mt-5">
            {{$data->links()}}
          </div>
          <div id="scrolldiv" class="text-center pt-4 d-none">
            <span class="d-block mb-1" style="color: #5CD9D3">Scroll to load more</span>
            <img src="/scroll3.gif" style="height: 30px">
          </div>
        </div>
      </section>
      <!-- result grid end -->
    </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->

    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  <!-- javascript files start -->
  @include('layouts.js.jquery')
  @include('layouts.js.macy')
  @include('layouts.js.script')

  <!-- javascript files end -->

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
  <script type="text/javascript">
    $(".category-button").click(function(){
      var link = $(this).attr('data-link');
      window.location.href = link;
    });
    $(function() {
        $('.lazy').lazy();
    });
  </script>
</body>

</html>