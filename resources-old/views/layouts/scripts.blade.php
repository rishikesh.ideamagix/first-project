<!-- jquery 3.4.1 -->
<script src="/assets/vendor/jquery-3.4.1/jquery.3.4.1.min.js"></script>
<!-- macy for masonry grid view for cards -->
<script src="/assets/vendor/macy.js-master/dist/macy.js"></script>
<!-- nice select 1.1.0 -->
<script src="/assets/vendor/jquery-nice-select-1.1.0/js/jquery.nice-select.min.js"></script>
<!-- custom script -->
<script src="/assets/js/script.js" async defer></script>