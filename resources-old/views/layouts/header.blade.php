<div class="page-loader">
    <img src="/atmo.gif" alt="ATMO">
</div>

<header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <div class="container-fluid">
    <div class="row align-items-center justify-content-between px-2">
      <a href="/" title="ATMO" class="logo">
        <img src="/atmo-logo.png" style="width: 80px">
      </a>
      <nav class="d-none d-lg-block">
        <ul class="p-0 m-0 d-flex">
          <li><a href="/uploadimage" title="Upload Images" class="active">Upload Images</a></li>
          <li><a href="/comingsoon" title="Products">Products</a></li>
          <li><a href="/comingsoon" title="Academy">Academy</a></li>
          <li><a href="/comingsoon" title="Reference Gallery">Reference Gallery</a></li>
          <li><a href="/comingsoon" title="About">About</a></li>
        </ul>
      </nav>
      <div class="signup-container d-none d-lg-block">
        @if (\Auth::check())
          <a href="/myuploads" title="My Profile">My Profile</a>
          <a href="" title="Logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>  
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
        @else
          <a href="/login" title="Login">Login</a>
          <a href="/register" title="Signup">Signup</a>
        @endif

        
      </div>
      <span class="fas fa-bars d-lg-none"></span>
    </div>
  </div>
</header>