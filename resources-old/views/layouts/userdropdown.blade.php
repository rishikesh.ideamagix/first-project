@php
  if(Auth::check())
  {
      $user = Auth::user();
      $signedinemail = Auth::user()->email;
  }else{
      $user = 0;
  }
@endphp

<div class="d-flex justify-content-center py-4">
  <div class="col-lg-4">
    <select name="" id="userpages" class="nice-select w-100">
      <option value="/myuploads" @if($datatype == 'useruploads') selected @endif>My Uploads</option>
      <option value="/mywishlist" @if($datatype == 'favourites') selected @endif>Favourites</option>
      <option value="/mydetail" @if($datatype == 'userdetails') selected @endif>User Details</option>
      <option value="/changemypassword" @if($datatype == 'changepassword') selected @endif>Change Password</option>
      <!-- <option value="/changemypassword/{{$user->password}}">Change Password</option> -->
      <option value="/myedits" @if($datatype == 'useredits') selected @endif>My Edits</option>
      <option value="/myclaims" @if($datatype == 'userclaims') selected @endif>My Claims</option>
      @if($signedinemail == 'moderator@atmo.com')
        <option value="/deletedimages" @if($datatype == 'deletedimages') selected @endif>Deleted Images</option>
      @endif
    </select>
  </div>
</div>