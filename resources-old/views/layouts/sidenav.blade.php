<div class="sidenav">
  <!-- <div class="sidenav-close d-flex justify-content-center align-items-center">
    <span class="fas fa-angle-left"></span>
  </div> -->
  <div class="sidenav-wrap">
    <a href="/" title="ATMO" class="d-block sidenav-logo py-4">
      <img src="/atmo-logo.png" alt="ATMO" class="d-block mx-auto">
    </a>
    <ul class="sidenav-main-menu p-0 m-0">
      <!-- Links will be generated automatically using the main nav bar -->
    </ul>
    <div class="signup-container py-4 d-flex justify-content-around mb-5">
      <!-- Right side Links will be generated automatically using the main right  nav bar -->
    </div>
  </div>
</div>