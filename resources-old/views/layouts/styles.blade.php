<!-- css files start -->
<!-- bootstrap 4.0.0 -->
<link rel="stylesheet" href="/assets/vendor/bootstrap-4.0.0/dist/css/bootstrap.min.css">
<!-- font awesome 5.11.2 -->
<link rel="stylesheet" href="/assets/vendor/fontawesome-free-5.11.2-web/css/all.min.css">
<!-- nice select 1.1.0 -->
<link rel="stylesheet" href="/assets/vendor/jquery-nice-select-1.1.0/css/nice-select.css">
<!-- custom -->
<link rel="stylesheet" href="/assets/css/style.css">
<!-- css files end -->
<link rel="stylesheet" href="/assets/vendor/select2/select2.min.css">
<link rel="stylesheet" href="/assets/vendor/fancybox/jquery.fancybox.min.css">

<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.8.0/bootstrap-tagsinput.css">


<script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5e29762e82ddf0001251a4f6&product=inline-share-buttons' async='async'></script>