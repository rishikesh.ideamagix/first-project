<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta property="og:url" content="">
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
  <meta name="author" content="">
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link rel="icon" href="" sizes="32x32" type="image/png">
  <title>ATMO VFX</title>
  
  <style>
      .hoverchnage:hover{
          color:#5FD9D3 !important;
      }
  </style>
  @include('layouts.styles')
</head>

<body>
  <!-- wrapper start -->
  <div class="wrapper">
    <!-- header start -->
    @include('layouts.header')
    <!-- header end -->
    <!-- main start -->
    <main>
      <!-- home page search form start -->
      <div class="home-page-search-form">
        <div class="container">
          <div class="row justify-content-center">
            <!-- <h1 class="col-12 text-center mb-1">ATMO</h1> -->
            <img src="/atmologotwo.png" class="mt-5 mb-2">
            <p class="col-12 text-center">The Educational Gallery Reference for Environment Artists</p>
            <form action="/search" method="post" class="col-md-7">
              @csrf
              <div class="input-wrap w-100">
                @if($datatype == 'search')
                  <input type="text" placeholder="Search by artist, description, movie name or tags" class="py-2 py-lg-3 pl-2 pl-sm-3 w-100 border-0" name="search_field" value="{{$search}}">
                @else
                  <input type="text" placeholder="Search by artist, description, movie name or tags" class="py-2 py-lg-3 pl-2 pl-sm-3 w-100 border-0" name="search_field">
                @endif
                <button class="border-0 py-0 h-100 px-4">
                  <span class="fas fa-search"></span>
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- home page search form end -->
      <!-- showcase section start -->
      <div class="showcase px-2 px-lg-0">
        <div class="container-fluid">
          <div class="row justify-content-center">
            <div class="showcase-banner text-center d-flex align-items-center w-100 justify-content-center">
              {{$link->link}}
            </div>
          </div>
        </div>
      </div>
      <!-- showcase section end -->
      <!-- categories filter start -->
      <div class="categories-filter">
        <div class="container-fluid">
          <div class="row justify-content-between px-2">
            <div class="categories-tab col-12 p-0 mb-4 flex-wrap justify-content-between animated fadeIn">
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'all') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/">All Results</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'matte-painting-set-extension') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/matte-painting-set-extension">Matte Painting Set Extension</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'classical-painting') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/classical-painting">Classical Painting</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'photography-composition') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/photography-composition">Photography Composition</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu" >
                <button @if($datatype == 'movies') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/movies">Movies/TV</button>
              </div>
              <div class="col-lg-2 col-sm-6 p-0 mb-3 mb-lg-0 main-menu">
                <button @if($datatype == 'games') class="category-button w-100 h-100 p-2 active" @else class="category-button w-100 h-100 p-2" @endif data-link="/images/games">Games</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- categories filter end -->
      <!-- result grid start -->
      <section class="result-grid pb-5 px-2 px-lg-0">
        <div class="container-fluid px-2">
          @if(count($data) < 1)
            <div class="mt-5 text-center" style="color: #5CD9D3">
              <h3>No Image Found</h3>
            </div>
          @endif
          <div id="mecy" class="image-container">
            <!-- Automatic data comes here -->
            @foreach($data as $key=>$datanew)
              <div class="box" data-aos="fade-up">
                <a href="/image/{{$datanew->id}}" title="" class="d-block">
                  <!--@if($key <= 10)-->
                  <!--  <img src="/images/compressed/{{$datanew->image}}"  alt="" >-->
                  <!--@else-->
                  <!--  <img data-src="/images/compressed/{{$datanew->image}}" data-srcset="/images/compressed/{{$datanew->image}}"  alt="" class="lazy" src="/image-loading.gif" >-->
                  <!--@endif-->
                  <!--<img data-src="/images/compressed/{{$datanew->image}}" data-srcset="/images/compressed/{{$datanew->image}}"  alt="" class="lazy" src="/verify.png" >-->
                  <img src="/images/compressed/{{$datanew->image}}"  alt="" >
                </a>
                @php 
                  $artistlink = preg_replace('/\s+/', '-', $datanew->artist_name);
                  $titlelink = preg_replace('/\s+/', '-', $datanew->movie_name);
                  $blanklink = 'unknown';
                  $newlink = '';

                  if($datanew->artist_name != ''){
                    $newlink = 'artist/'.$artistlink;
                  }else if($datanew->movie_name != ''){
                    $newlink = 'title/'.$titlelink;
                  }else{
                    $newlink = 'artist/unknown';
                  }
                @endphp

                <div class="home-page-profile-container">
                  <div class="user-detail d-flex align-items-center justify-content-between">
                    <a href="/images/{{$newlink}}" class="d-flex align-items-center fl70p hoverchnage ">
                      @if($datanew->claimed == 'yes')
                        <img src="/assets/images/{{$datanew->profile_image}}" alt="" class="rounded-circle">
                      @else  
                        <img src="/assets/images/1.jpg" alt="" class="rounded-circle">
                      @endif
                      @if($datanew->artist_name != '')
                        <p class="mb-0">
                          {{$datanew->artist_name}}
                          

                          @if($datanew->claimed == 'yes')
                            @foreach($users as $user)
                              @if($datanew->user_id == $user->id)
                                @if($user->artist_type == 'senior'  && $user->artist_type_verified == 'yes' )
                                  <img src="/verify.png" style="width: 20px !important">
                                @endif
                              @endif
                            @endforeach
                          @endif
                        
                        </p>
                      @elseif($datanew->movie_name != '')
                        <p class="mb-0">{{$datanew->movie_name}}</p>
                      @else
                        <p class="mb-0">Unknown</p>
                      @endif
                    </a>
                    <div class="fl74p" style="">
                      <a href="/image/{{$datanew->id}}" class="likes-wrap d-flex justify-content-end">
                        <!--<div class="counts-container d-flex align-items-center mr-2">-->
                        <!--  <i class="fas fa-share"></i>-->
                        <!--</div>-->
                        <div class="counts-container d-flex align-items-center">
                          <i class="far fa-comment"></i>
                          <span class="count">
                            {{$datanew->comment_count}}
                          </span>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </div>
          <div class="mt-5">
            {{$data->links()}}
          </div>
          <div id="scrolldiv" class="text-center pt-4 d-none">
            <span class="d-block mb-1" style="color: #5CD9D3">Scroll to load more</span>
            <img src="/scroll3.gif" style="height: 30px">
          </div>
        </div>
      </section>
      <!-- result grid end -->
    </main>
    <!-- main end -->
    <!-- footer start -->
    @include('layouts.footer')
    <!-- footer end -->
    <!-- sidenav start -->

    @include('layouts.sidenav')
    <!-- sidenav end -->
  </div>
  <!-- wrapper end -->
  <!-- javascript files start -->
  @include('layouts.js.jquery')
  @include('layouts.js.macy')
  @include('layouts.js.script')

  <!-- javascript files end -->

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.plugins.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
        $("html, body").animate({ scrollTop: 0 }, "fast");
        return false;
    });
  
  
  
    $(".category-button").click(function(){
      var link = $(this).attr('data-link');
      window.location.href = link;
    });
    // $(function() {
    //     $('.lazy').lazy();
    // });
    
    
    document.addEventListener("DOMContentLoaded", function() {
      let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
      let active = false;
    
      const lazyLoad = function() {
        if (active === false) {
          active = true;
    
          setTimeout(function() {
            lazyImages.forEach(function(lazyImage) {
              if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
                lazyImage.src = lazyImage.dataset.src;
                lazyImage.srcset = lazyImage.dataset.srcset;
                lazyImage.classList.remove("lazy");
    
                lazyImages = lazyImages.filter(function(image) {
                  return image !== lazyImage;
                });
    
                if (lazyImages.length === 0) {
                  document.removeEventListener("scroll", lazyLoad);
                  window.removeEventListener("resize", lazyLoad);
                  window.removeEventListener("orientationchange", lazyLoad);
                }
              }
            });
    
            active = false;
          }, 200);
        }
      };
      
      
      
      document.addEventListener("scroll", lazyLoad);
      window.addEventListener("resize", lazyLoad);
      window.addEventListener("orientationchange", lazyLoad);
    });
  </script>
    
    
    
  </script>
</body>

</html>